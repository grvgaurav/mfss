import { AppcenteradminPage } from './app.po';

describe('appcenteradmin App', function() {
  let page: AppcenteradminPage;

  beforeEach(() => {
    page = new AppcenteradminPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
