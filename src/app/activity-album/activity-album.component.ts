import { Component, OnInit, NgModule,ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import { ActivatedRoute, Router} from '@angular/router';
import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'app-activity-album',
  templateUrl: './activity-album.component.html',
  styleUrls: ['./activity-album.component.css'],
  providers:[datacalls]
})
export class ActivityAlbumComponent implements OnInit {




dtOptions: DataTables.Settings = {};

dtTrigger: Subject<any> = new Subject();


  posts:Post[];
  id:any;
   sid;
  a: any;
  constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef,private activatedRoute:ActivatedRoute) {
  }

  ngOnInit() {
    window.scrollTo(0,0);
    this.activatedRoute.params.subscribe(
      (param: any) => {
        this.id=param['id'];
        
        this.sid = param['sid'];
       // this.type = param['type_id'];

        //console.log("Type: "+ this.type);
        if (this.id != undefined) {
       if (this.sid!= undefined){
     this._DatacallsService.getActivity().subscribe(posts => {
       
       for (var i=0; i<posts.Data.length;i++){
       for(var j=0; j<posts.Data[i].subCategory.length;j++){
         
        console.log("activity_category_id"+posts.Data[i].subCategory[j].activity_category_id+" "+this.id+" "+this.sid);
        // if (this.id==posts.Data[i].activity_category_id){
           //console.log(this.id+" "+posts.Data[i].activity_category_id);
         if(this.sid==posts.Data[i].subCategory[j].activity_category_id){ 
         console.log(this.sid+" "+posts.Data[i].subCategory[j].activity_category_id);
    this.posts=posts.Data[i].subCategory[j].album;
    this.dtTrigger.next();
     console.log(posts.Data[i].subCategory[j].album);
      window.setTimeout(() => {
          var s = document.createElement("script");
          s.text = "TableDatatablesManaged.init();";
          this._elementRef.nativeElement.appendChild(s);
           
        }, 100);
    }
       }}
        
   // console.log(this.posts);
       // }
      } );
       }
        
      }});

 

//  this._DatacallsService.getAllCategory().subscribe(
//       data => {
//         if (data.Data.length > 0) {
//           this.categorylist = data.Data[0].data;
//           console.log("categories:" , this.categorylist);
//         }
//       }
//     );
  }
  
}
interface Post{
Message:string;
Status:number;
Success:string;
}