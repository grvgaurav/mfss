import { Component, OnInit, NgModule,ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import { ActivatedRoute, Router} from '@angular/router';
import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'app-activity-category',
  templateUrl: './activity-category.component.html',
  styleUrls: ['./activity-category.component.css'],
  providers:[datacalls]
})
export class ActivityCategoryComponent implements OnInit {

dtOptions: DataTables.Settings = {};
dtTrigger: Subject<any> = new Subject();
posts:Post[];
  a: any;
  constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef) {
  }

  ngOnInit() {
     this._DatacallsService.getActivity().subscribe(posts => {
    this.posts=posts.Data;
    this.dtTrigger.next();
      console.log(this.posts=posts.Data);
         window.setTimeout(() => {
          var s = document.createElement("script");
          s.text = "TableDatatablesManaged.init();";
          this._elementRef.nativeElement.appendChild(s);
           
        }, 100);
   // console.log(this.posts);
  });
 

//  this._DatacallsService.getAllCategory().subscribe(
//       data => {
//         if (data.Data.length > 0) {
//           this.categorylist = data.Data[0].data;
//           console.log("categories:" , this.categorylist);
//         }
//       }
//     );
  }

}
interface Post{
Message:string;
Status:number;
Success:string;
}