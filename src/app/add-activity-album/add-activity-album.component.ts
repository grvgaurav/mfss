import { Component, OnInit, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-activity-album',
  templateUrl: './add-activity-album.component.html',
  styleUrls: ['./add-activity-album.component.css'],
  providers: [datacalls]
})
export class AddActivityAlbumComponent implements OnInit {

  public activityalbumForm: FormGroup;
  public submitted: boolean;
  public events: any[] = [];
  //activitycategoryList:any;
  imagepath: string = "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";

  a: any;
  filesToPost: any;
  file_keys: string[];
  id: any;
  sid: any;
  aid: any;
  alertClass: string;
  errorMsg: string;
  resized_images_list: { filename: string, url: string, dimension: string }[];
  isFileLoaded: boolean = false;
  isValidImage: boolean = true;

  constructor(private _fb: FormBuilder, private activatedRoute: ActivatedRoute, private _http: Http, private _datacalls: datacalls, private Router: Router) {
    this.activityalbumForm = this._fb.group({
      activity_images_id: [0],
      app_id: [12],
      image_name: ['', Validators.required],
      //description:['',Validators.required],
      path: ['']
      //file: ['']
    });
  }
  ngOnInit() {

    //       this._datacalls.getAllCategory().subscribe(
    //             data => {
    //               if (data.Data.length > 0) {
    //                 this.aboutusList = data.Data[0].data;
    //                 console.log(this.aboutusList);
    //               }
    //             }
    //           );

    this.activatedRoute.params.subscribe(
      (param: any) => {

        this.id = param['id'],
          this.sid = param['sid'],
          this.aid = param['aid'];
        console.log(this.id + " " + this.sid + " " + this.aid)
        if (this.id != undefined) {
          if (this.sid != undefined) {
            if (this.aid != undefined) {

              this._datacalls.getActivity().subscribe(

                data => {
                  for (var i = 0; i < data.Data.length; i++) {

                    for (var j = 0; j < data.Data[i].subCategory.length; j++) {

                      for (var k = 0; k < data.Data[i].subCategory[j].album.length; k++) {


                        console.log(this.aid + " " + data.Data[i].subCategory[j].album[k].activity_images_id)
                        if (this.id == data.Data[i].activity_category_id) {
                          if (this.sid == data.Data[i].subCategory[j].activity_category_id) {
                            if (this.aid == data.Data[i].subCategory[j].album[k].activity_images_id) {

                              this.imagepath = data.Data[i].subCategory[j].album[k].path;
                              //console.log("ALBUM"+data.Data[i].subCategory[j].album.length)
                              if (data.Data[i].subCategory[j].album.length > 0) {
                                //console.log("naME"+data.Data[i].subCategory[j].album[k].image_name);
                                this.activityalbumForm = this._fb.group({


                                  'activity_images_id': data.Data[i].subCategory[j].album[k].activity_images_id,

                                  'app_id': data.Data[i].subCategory[j].album[k].app_id,
                                  'image_name': data.Data[i].subCategory[j].album[k].image_name,
                                  //'description':data.Data.subCategory[i].description,

                                  'path': data.Data[i].subCategory[j].album[k].path
                                  //'file':data.Data[i].subCategory[j].album[k].file
                                });
                                this.filesToUpload = new Array<File>();
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              );
            }
          }
        }
      });


  }

  file: File;
  filesToUpload: Array<File>;

  _url: string = "http://35.154.141.107:4000/api/insertactivity_images";
  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
    this.isFileLoaded = true;
    let size = fileInput.target.files[0].size;
    console.log("Size: " + size);
    if (size >= 500000) {
      this.isValidImage = false;
      console.log("isValidImage: " + this.isValidImage);
    } else {
      this.isValidImage = true;
      console.log("isValidImage: " + this.isValidImage);
    }
  }
  onSubmit() {

    // this.a = [{name :"priya"}];
    var formData: any = new FormData();
    formData.append("activity_images_id", this.activityalbumForm.value.activity_images_id);
    formData.append("app_id", 12);
    formData.append("image_name", this.activityalbumForm.value.image_name);
    //formData.append("description",this.activityalbumForm.value.description); 
    formData.append("file", this.activityalbumForm.value.path);
    formData.append("category", this.sid);
    //formData.append("path", this.activityalbumForm.value.path);



    this.makeFileRequest(this._url, formData, this.filesToUpload).then((result) => {
      console.log(result);
      if (result['Success'] == true) {
        this.alertClass = "alert alert-success text-center  alert-dismissible";
        this.errorMsg = 'Activity Album Added Successfully!';
        window.setTimeout(() => {
          this.Router.navigate(['activity-album' + '/' + this.id + '/' + this.sid]);
        }, 3000);
      }
      else {
        this.alertClass = "alert alert-danger text-center  alert-dismissible";
        this.errorMsg = result['Message'];
      }

    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }

      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

}