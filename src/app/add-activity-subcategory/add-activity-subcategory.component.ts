import { Component, OnInit,ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-activity-subcategory',
  templateUrl: './add-activity-subcategory.component.html',
  styleUrls: ['./add-activity-subcategory.component.css'],
  providers:[datacalls]
})
export class AddActivitySubcategoryComponent implements OnInit {

  public activitysubcategoryForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    //activitycategoryList:any;
    imagepath:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";

    a:any;
    filesToPost: any;
    file_keys: string[];
    id;
    sid;
    alertClass: string;
    errorMsg: string;
    resized_images_list: { filename: string, url: string, dimension: string }[];
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
    this.activitysubcategoryForm = this._fb.group({
            activity_category_id:[0],
            app_id:[12],
            category_name:['',Validators.required],
            description:['',Validators.required],
            image:[''],
            file:['']
        });
    }
    ngOnInit() {

//       this._datacalls.getAllCategory().subscribe(
//             data => {
//               if (data.Data.length > 0) {
//                 this.aboutusList = data.Data[0].data;
//                 console.log(this.aboutusList);
//               }
//             }
//           );
            
 this.activatedRoute.params.subscribe(
      (param: any) => {

        this.id = param['id'],
        this.sid = param['sid'];
        console.log(this)
        if (this.id != undefined) {if (this.sid != undefined) {

          this._datacalls.getActivity().subscribe(
            
            data => {for(var i=0;i<data.Data.length;i++){
              for(var j=0;j<data.Data[i].subCategory.length;j++){

                              console.log( this.sid +""+data.Data[i].subCategory[j].activity_category_id)
if(this.id==data.Data[i].activity_category_id){
                              if(this.sid==data.Data[i].subCategory[j].activity_category_id){

              this.imagepath=data.Data[i].subCategory[j].image;
              console.log(data.Data[i].subCategory[j].image)
              if (data.Data[i].subCategory.length > 0) {
                 
                this.activitysubcategoryForm = this._fb.group({
                  'activity_category_id':data.Data[i].subCategory[j].activity_category_id,
                  'app_id':data.Data[i].subCategory[j].app_id,
                  'category_name':data.Data[i].subCategory[j].category_name,
                  'description':data.Data[i].subCategory[j].description,
                  'image':data.Data[i].subCategory[j].image,
                  'file':data.Data[i].subCategory[j].file
                });
                this.filesToUpload=new Array<File>();
              }
            }
              } } }} );
        }}
      });
        

    }
    
    file: File;
    filesToUpload: Array<File>;
 
_url:string ="http://35.154.141.107:4000/api/activity_category";
  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
    this.isFileLoaded = true;
    let size = fileInput.target.files[0].size;
    console.log("Size: "+ size);
    if(size>=500000) {
      this.isValidImage = false;
      console.log("isValidImage: "+ this.isValidImage);
    }else {
    this.isValidImage = true;
    console.log("isValidImage: "+ this.isValidImage);
  }
  }
  onSubmit() {
     
       // this.a = [{name :"priya"}];
    var formData: any = new FormData();
     formData.append("activity_category_id",this.activitysubcategoryForm.value.activity_category_id);
     formData.append("app_id",12);
      formData.append("category_name",this.activitysubcategoryForm.value.category_name);
      formData.append("parent_id",this.id);
     formData.append("description",this.activitysubcategoryForm.value.description); 
     formData.append("file",this.activitysubcategoryForm.value.image); 
    // formData.append("image",this.activitysubcategoryForm.value.image); 
     
     
     
    this.makeFileRequest(this._url,formData, this.filesToUpload).then((result) => {
      console.log(result);
        if(result['Success']==true){
             this.alertClass = "alert alert-success text-center  alert-dismissible";
          this.errorMsg = 'Activity Subcategory Added Successfully!';
  window.setTimeout(() => {
            this.Router.navigate(['activity-subcategory'+'/'+this.id]);
          }, 3000);
        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = result['Message'];
        }
    
    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

}