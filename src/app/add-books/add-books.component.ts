import { Component, OnInit, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-books',
  templateUrl: './add-books.component.html',
  styleUrls: ['./add-books.component.css'],
  providers:[datacalls]
})
export class AddBooksComponent implements OnInit {
public bookForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    imagepath:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";

    a:any;
    filesToPost: any;
    file_keys: string[];
    id;
    alertClass: string;
    errorMsg: string;
    resized_images_list: { filename: string, url: string, dimension: string }[];
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
    this.bookForm = this._fb.group({
            books_id:[0],
            app_id:[7],
            price:['',Validators.required],
            book_name:['',Validators.required],
            description:['',Validators.required],
            file:[''],
            image_path:['']
        });
    }
    ngOnInit() {

//       this._datacalls.getAllCategory().subscribe(
//             data => {
//               if (data.Data.length > 0) {
//                 this.aboutusList = data.Data[0].data;
//                 console.log(this.aboutusList);
//               }
//             }
//           );
            
 this.activatedRoute.params.subscribe(
      (param: any) => {

        this.id = param['id'];
        console.log(this)
        if (this.id != undefined) {

          this._datacalls.getAllBooks(this.id).subscribe(
            
            data => {
              this.imagepath=data.Data[0].path;
              if (data.Data.length > 0) {
                 
                this.bookForm = this._fb.group({
                  'books_id': data.Data[0].awards_id,
                  'book_name': data.Data[0].name,
                  'description': data.Data[0].description,
                  'price': data.Data[0].rank,
                  'app_id': data.Data[0].app_id,
                  'file': data.Data[0].path,
                  'image_path': data.Data[0].path
                });
                this.filesToUpload=new Array<File>();
              }
            }
          );
        }
      });
        

    }
    
    file: File;
    filesToUpload: Array<File>;
 
_url:string ="http://35.154.141.107:6002/api/insertawards";
  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
    let size = fileInput.target.files[0].size;
    console.log("Size: "+ size);
    if(size>=500000) {
      this.isValidImage = false;
      console.log("isValidImage: "+ this.isValidImage);
    }else {
    this.isValidImage = true;
    console.log("isValidImage: "+ this.isValidImage);
  }
    this.isFileLoaded = true;
  }
  onSubmit() {
     
       // this.a = [{name :"priya"}];
     var formData: any = new FormData();
     formData.append("awards_id",this.bookForm.value.books_id);
     formData.append("app_id",this.bookForm.value.app_id);
     formData.append("name",this.bookForm.value.book_name); 
     formData.append("description",this.bookForm.value.description);
     formData.append("rank",this.bookForm.value.price);
     formData.append("image_path",this.bookForm.value.image_path);
     this.makeFileRequest(this._url,formData, this.filesToUpload).then((result) => {
      console.log(result);
        if(result['Success']==true){
             this.alertClass = "alert alert-success text-center  alert-dismissible";
          this.errorMsg = 'Book Added SuccessFully';
  window.setTimeout(() => {
            this.Router.navigate(['books']);
          }, 3000);
     
        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = result['Message'];
        }
    
    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }


}
