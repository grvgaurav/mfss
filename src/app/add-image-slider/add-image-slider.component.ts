import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-image-slider',
  templateUrl: './add-image-slider.component.html',
  styleUrls: ['./add-image-slider.component.css'],
    providers: [datacalls]
})
export class AddImageSliderComponent implements OnInit {

   public imagesliderForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
   a:any;
    filesToPost: any;
       alertClass: string;
   errorMsg: string;
    file_keys: string[];
    id; 
    imagepath:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;
    
    resized_images_list: { filename: string, url: string, dimension: string }[];

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls, private changeDetectorRef: ChangeDetectorRef,private fb:FormBuilder,private Router:Router ) {

        this.imagesliderForm = this._fb.group({
            image_id:[0],
            app_id:[17],
            image_name: ['', [<any>Validators.required]],
            file:[''],
            image_path:['']

        });
     }


    ngOnInit() {
      window.scrollTo(0,0);

  
      this.activatedRoute.params.subscribe(
      (param: any) => {

        this.id = param['id'];

        console.log(this)
        if (this.id != undefined) {

          this._datacalls.getImageSliders(this.id).subscribe(            
            data => {
             console.log(data.Data)
            for(var i=0;i<data.Data.ImageSlider.length;i++){
debugger
                              console.log( this.id+" "+data.Data.ImageSlider[i].image_path)

                              if(this.id==data.Data.ImageSlider[i].image_slider_id){


              this.imagepath=data.Data.ImageSlider[i].image_path;
            
                 
                this.imagesliderForm.patchValue({
                  'image_id': data.Data.ImageSlider[i].image_slider_id,
                  'app_id': 17,
                  'image_name':data.Data.ImageSlider[i].image_name,
                  'image_path': data.Data.ImageSlider[i].image_path,
                });

                this.filesToUpload=new Array<File>();
              
            }
          }
          },
            
            error => { },
            () => {

            }
          );
        }
      });
        


    }

 

  

    

    file: File;
  filesToUpload: Array<File>;
 
 _url:string ="http://myappcenter.co.in/serverfileuploadapi/api/insertimageslider";
  onChange(fileInput: any) {
     this.isFileLoaded=true;
    this.filesToUpload = <Array<File>>fileInput.target.files;
    let size = fileInput.target.files[0].size;
    console.log("Size: "+ size);
    if(size>=500000) {
      this.isValidImage = false;
      console.log("isValidImage: "+ this.isValidImage);
    }else {
    this.isValidImage = true;
    console.log("isValidImage: "+ this.isValidImage);
  }

}



  onSubmit() {
     
       // this.a = [{name :"priya"}];
    var formData: any = new FormData();
    if(this.imagesliderForm.value.image_id==""){
      this.imagesliderForm.value.image_id=0;
    }
    formData.append("image_slider_id",this.imagesliderForm.value.image_id);
    formData.append("app_id",17);
    formData.append("image_name",this.imagesliderForm.value.image_name);
    formData.append("image_path",this.imagesliderForm.value.image_path);
    formData.append("image_description"," ");
    formData.append("active_status",1);
    formData.append("created_by",this.imagesliderForm.value.app_id);
    console.log(this.filesToUpload)
    this.makeFileRequest(this._url,formData, this.filesToUpload).then((result) => {
      console.log(result);
       if(result['Success']==true){
             this.alertClass = "alert alert-success text-center";
          if (this.imagesliderForm.value.image_id==0) {
            this.errorMsg = 'ImageSlider Added SuccessFully';
          } else {
            this.errorMsg = 'ImageSlider Edited SuccessFully';
          }
  window.setTimeout(() => {
            this.Router.navigate(['imageSlider']);
          }, 2500);
   // this.Router.navigate(['productlist']);
     
        }
        else {
          this.alertClass = "alert alert-danger text-center";
          this.errorMsg = "result['Message']";
        }
    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }





}