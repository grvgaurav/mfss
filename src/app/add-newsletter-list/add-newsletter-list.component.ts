import { Component, OnInit,ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-newsletter-list',
  templateUrl: './add-newsletter-list.component.html',
  styleUrls: ['./add-newsletter-list.component.css'],
  providers:[datacalls]

})
export class AddNewsletterListComponent implements OnInit {

    public newsletterlistForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    newsletterlistList:any;
    imagepath:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
    teacher_id;
    a:any;
    filesToPost: any;
    file_keys: string[];
    subject_id;
    id;
    alertClass: string;
    errorMsg: string;
    resized_images_list: { filename: string, url: string, dimension: string }[];
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
    this.newsletterlistForm = this._fb.group({
            subject_id:[0],
            my_notes_id:[0],
            app_id:[13],
            title:['',Validators.required],
            description:['',Validators.required],
            file:[''],
            image_path:['']
        });
    }
    ngOnInit() {

//       this._datacalls.getAllCategory().subscribe(
//             data => {
//               if (data.Data.length > 0) {
//                 this.newsletterlistList = data.Data[0].data;
//                 console.log(this.newsletterlistList);
//               }
//             }
//           );
            
 this.activatedRoute.params.subscribe(
      (param: any) => {

        this.id = param['my_notes_id'];
           this.subject_id = param['subject_id'];
              this.teacher_id = param['teacher_id'];
        console.log(this)
        if (this.id != undefined) {

          this._datacalls.getViewSubNewsletter(this.subject_id,this.teacher_id).subscribe(
            
            data => {
              console.log('data:',data.Data);
                            for(var i=0;i<data.Data.length;i++){

                        //      console.log( this.id +""+data.Data[i].id)

                              if(this.id ==data.Data[i].my_notes_id){

                               this.imagepath=data.Data[i].path;
              console.log(data.Data[i].path)
                 
                this.newsletterlistForm = this._fb.group({
                  'subject_id': data.Data[i].subject_id,
                  'my_notes_id': data.Data[i].my_notes_id,
                  'app_id': data.Data[i].app_id,
                  'title': data.Data[i].title,
                  'description': data.Data[i].description,
                  'file': data.Data[i].path,
                  'image_path': data.Data[i].path
                });
                this.filesToUpload=new Array<File>();
                              }
              }
            }
          );
        }
      });
        

    }
    
    file: File;
    filesToUpload: Array<File>;
 
_url:string ="http://35.154.141.107:6002/api/insertmynotes";
  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
    this.isFileLoaded = true;
    let size = fileInput.target.files[0].size;
    console.log("Size: "+ size);
    if(size>=1000000) {
      this.isValidImage = false;
      console.log("isValidImage: "+ this.isValidImage);
    }else {
    this.isValidImage = true;
    console.log("isValidImage: "+ this.isValidImage);
  }
  }
  onSubmit() {
     this.teacher_id=this.activatedRoute.snapshot.params['teacher_id'];
     this.subject_id=this.activatedRoute.snapshot.params['subject_id'];
     console.log(this.teacher_id);
       // this.a = [{name :"priya"}];
    var formData: any = new FormData();
     formData.append("subject_id",this.subject_id);
     formData.append("my_notes_id",this.newsletterlistForm.value.my_notes_id);
     formData.append("app_id",13);
     formData.append("user_std",'5th');
     formData.append("type",'');
     formData.append("title",this.newsletterlistForm.value.title);
     formData.append("teacher_id",this.teacher_id);
     formData.append("description",this.newsletterlistForm.value.description);
     formData.append("file",this.newsletterlistForm.value.path) 
     formData.append("image_path",this.newsletterlistForm.value.path); 
     formData.append("division",'all');
     //formData.append("sub_category_id",47);
    this.makeFileRequest(this._url,formData, this.filesToUpload).then((result) => {
      console.log(result);
        if(result['Success']==true){
             this.alertClass = "alert alert-success text-center  alert-dismissible";
          this.errorMsg = 'Newsletter Added Successfully!';
  window.setTimeout(() => {
            this.Router.navigate(['newsletterfile/'+this.subject_id+'/'+this.teacher_id]);
          }, 3000);
        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = result['Message'];
        }
    
    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

}
