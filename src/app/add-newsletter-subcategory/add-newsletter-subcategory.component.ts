import { Component, OnInit, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-newsletter-subcategory',
  templateUrl: './add-newsletter-subcategory.component.html',
  styleUrls: ['./add-newsletter-subcategory.component.css'],
  providers:[datacalls]
})
export class AddNewsletterSubcategoryComponent implements OnInit {

    public newslettersubcategoryForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    addnewsletterList:any;
    imagepath:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";

    a:any;
    filesToPost: any;
    file_keys: string[];
    teacher_id;
    subject_master_id;
    alertClass: string;
    errorMsg: string;
    resized_images_list: { filename: string, url: string, dimension: string }[];
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
    this.newslettersubcategoryForm = this._fb.group({
            subject_master_id:[0],
            teacher_id:[],
            app_id:[13],
            name:['',Validators.required],
            file:[''],
            image_path:['']
        });
    }
    ngOnInit() {
//console.log('teacher_id:'+this.activatedRoute.snapshot.params['teacher_id']);
//       this._datacalls.getAllCategory().subscribe(
//             data => {
//               if (data.Data.length > 0) {
//                 this.addnewsletterList = data.Data[0].data;
//                 console.log(this.addnewsletterList);
//               }
//             }
//           );
            
 this.activatedRoute.params.subscribe(
      (param: any) => {

        this.subject_master_id = param['subject_id'];
        this.teacher_id = param['teacher_id'];
        console.log('sub and teach:'+this.subject_master_id+' and '+this.teacher_id);
        if (this.subject_master_id != undefined) {

          this._datacalls.getNewsletterList().subscribe(
          
            data => {
                 for(var i=0;i<data.Data.length;i++){

                              console.log( this.subject_master_id +""+data.Data[i].subject_master_id)

                              if(this.subject_master_id ==data.Data[i].subject_master_id){

                               //this.imagepath=data.Data[i].image_path;
              console.log(data.Data[i].image_path)
                this.newslettersubcategoryForm.patchValue({
                  'subject_master_id': data.Data[i].subject_master_id,
                  'app_id': data.Data[i].app_id,
                  'name': data.Data[i].name,
                  
                });
                this.filesToUpload=new Array<File>();
                              }
                 }
            }
              
            
          );
        }
      });
        

    }
    
    file: File;
    filesToUpload: Array<File>;
 
_url:string ="http://35.154.141.107:6001/api/insertsubjectmaster";
  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
    this.isFileLoaded = true;
    let size = fileInput.target.files[0].size;
    console.log("Size: "+ size);
    if(size>=500000) {
      this.isValidImage = false;
      console.log("isValidImage: "+ this.isValidImage);
    }else {
    this.isValidImage = true;
    console.log("isValidImage: "+ this.isValidImage);
  }
  }
  onSubmit() {
     //console.log('teacher_id:'+this.activatedRoute.snapshot.params['teacher_id']);
    // this.teacher_id=this.activatedRoute.snapshot.params['teacher_id'];
       // this.a = [{name :"priya"}];

    this.teacher_id=this.activatedRoute.snapshot.params['teacher_id'];

    console.log('abc');
    var formData: any = new FormData();
     formData.append("subject_id",this.newslettersubcategoryForm.value.subject_master_id);
     formData.append("teacher_id",this.teacher_id);
     formData.append("app_id",13);
     formData.append("name",this.newslettersubcategoryForm.value.name);
     //formData.append("teacher_id",this.teacher_id); 
     //formData.append("image_path",this.newslettersubcategoryForm.value.image_path); 
     //formData.append("sub_category_id",47);
    this.makeFileRequest(this._url,formData, this.filesToUpload).then((result) => {
      console.log(result);
        if(result['Success']==true){
             this.alertClass = "alert alert-success text-center  alert-dismissible";
          this.errorMsg = 'Newsletter Subcategory Added Successfully!';
  window.setTimeout(() => {
            this.Router.navigate(['newsletter-subcategory',this.teacher_id]);
          }, 3000);
        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = result['Message'];
        }
    
    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      // for (var i = 0; i < files.length; i++) {
      //   formData.append("file", files[i], files[i].name);
      // }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

}
