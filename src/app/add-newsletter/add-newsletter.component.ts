import { Component, OnInit, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-newsletter',
  templateUrl: './add-newsletter.component.html',
  styleUrls: ['./add-newsletter.component.css'],
  providers:[datacalls]
})
export class AddNewsletterComponent implements OnInit {

  public newsletterForm: FormGroup;
    public submitted: boolean;
    a:any;
    id;
    
    //type;
    //types;
    alertClass: string;
    errorMsg: string;


    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
    this.newsletterForm = this._fb.group({
            id:[0],
            //brand_id:[23],
            app_id:[13],
           name:['',Validators.required]
           
        });
    }
    ngOnInit() {

//this._datacalls.getBrandModel(null).subscribe(
           // data => {
             // if (data.Data.length > 0) {
              //  this.types = data.Data;
              //  console.log("Types: ",data.Data);
             // }
           // }
         // );
            
this.activatedRoute.params.subscribe(
      (param: any) => {
        this.id = param['teacher_id'];
        //this.return_policy_id = param['m,'];
       // this.type = param['type_id'];

        console.log("Type: "+ this.id);
        if (this.id != undefined) {
          this._datacalls.getNewsletter().subscribe(
            
            data => {
              for(var i=0;i<data.Data.Teachers.length;i++){

                              console.log( this.id +""+data.Data.Teachers[i].id) 

                              if(this.id ==data.Data.Teachers[i].id){
              if (data.Data.Teachers.length > 0) {                 
                this.newsletterForm = this._fb.group({
                  id:data.Data.Teachers[i].id,
                  app_id: 13,
                  name:data.Data.Teachers[i].name,
                  
                 
                });
              }
            }
            }
          }
            );
        }
        
      });




}
    
onSubmit() {

/*let formData={
                'return_policy_id': this.newsletterForm.value.return_policy_id,
                'brand_id': this.newsletterForm.value.brand_id,
                'app_id': this.newsletterForm.value.app_id,
                'model_name': this.newsletterForm.value.model_name
                
}*/
 var formData: any = new FormData();
 formData.append("id",this.newsletterForm.value.id);
     //formData.append("brand_id",this.newsletterForm.value.brand_id);
     formData.append("app_id",13);
     formData.append("name",this.newsletterForm.value.name ); 


// this._datacalls.addReturnPolicy(formData).subscribe(
      data => {
        if (data) {
          console.log("Inserted value");

          // this.Router.navigate(['events']);
        } else {
          console.log("not");
        }
        
         if(data['Success']==true){
          this.alertClass = "alert alert-success text-center  alert-dismissible";
          if (this.newsletterForm.value.id==0) {
            this.errorMsg = 'Newsletter Added SuccessFully';
          } else {
            this.errorMsg = 'Newsletter Edited SuccessFully';
          }
          window.setTimeout(() => {
              this.Router.navigate(['newsletter']);
            //this.Router.navigate(['attributeType/'+ this.type + '/attributes']);
          }, 250);     
        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = data['Message'];
        }
      }
      
   // );
    
}

}

