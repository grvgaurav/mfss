import { Component, OnInit,ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-obituary',
  templateUrl: './add-obituary.component.html',
  styleUrls: ['./add-obituary.component.css'],
  providers: [datacalls]
})
export class AddObituaryComponent implements OnInit {

    public obituaryForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    obituaryList:any;
    imagepath:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";

    a:any;
    filesToPost: any;
    file_keys: string[];
    obituary_id;
    alertClass: string;
    errorMsg: string;
    resized_images_list: { filename: string, url: string, dimension: string }[];
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
   let contactRegex='^([0-9]*)$';
    this.obituaryForm = this._fb.group({
            obituary_id:[0],
            app_id:[13],
            obituary_to: ['',[Validators.required, Validators.maxLength(4),Validators.minLength(4),<any>Validators.pattern(contactRegex)]],
            obituary_from:['',[Validators.required,Validators.maxLength(4),Validators.minLength(4),<any>Validators.pattern(contactRegex)]],
            name:['',Validators.required],
            image_path:[''],
            info:[''],
        });
    }
    ngOnInit() {
      window.scrollTo(0,0);

//       this._datacalls.getAllCategory().subscribe(
//             data => {
//               if (data.Data.length > 0) {
//                 this.obituaryList = data.Data[0].data;
//                 console.log(this.obituaryList);
//               }
//             }
//           );
            
 this.activatedRoute.params.subscribe(
      (param: any) => {

        this.obituary_id = param['obituary_id'];
        console.log(this.obituary_id)
        if (this.obituary_id != undefined) {

          this._datacalls.getObituary(this.obituary_id).subscribe(
            
            data => {
                            console.log('data',data)
                            
                            for(var i=0;i<data.Data.length;i++){

                            //  console.log('data',data.Data[i].id)

                              if(this.obituary_id ==data.Data[i].obituary_id){

                               this.imagepath=data.Data[i].image_path;
              console.log(data.Data[i].image_path)
                 
                this.obituaryForm.patchValue({
                  'obituary_id': data.Data[i].obituary_id,
                  'app_id':13,
                  'obituary_to': data.Data[i].obituary_to,
                  'obituary_from': data.Data[i].obituary_from,
                  'name': data.Data[i].name,
                  'image_path': data.Data[i].image_path,
                  'info': data.Data[i].info,
                });
                this.filesToUpload=new Array<File>();
                              }
              }
            }
          );
        }
      });
        

    }
    
    file: File;
    filesToUpload: Array<File>;
 
_url:string ="http://myappcenter.co.in/serverfileuploadapi/api/insertobituary";
  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
    this.isFileLoaded = true;
    let size = fileInput.target.files[0].size;
    console.log("Size: "+ size);
    if(size>=500000) {
      this.isValidImage = false;
      console.log("isValidImage: "+ this.isValidImage);
    }else {
    this.isValidImage = true;
    console.log("isValidImage: "+ this.isValidImage);
  }
  }


  onSubmit() {
     debugger
       // this.a = [{name :"priya"}];
    var formData: any = new FormData();
     formData.append("obituary_id",this.obituaryForm.value.obituary_id);
     formData.append("app_id",13);
      
    //  formData.append("student_name",this.obituaryForm.value.student_name);
     formData.append("obituary_to",this.obituaryForm.value.obituary_to);
    //  formData.append("std",'5th'); 
    //  formData.append("subject",2017);
     formData.append("obituary_from",this.obituaryForm.value.obituary_from);
     formData.append("name",this.obituaryForm.value.name); 
     formData.append("image_path",this.imagepath); 
     formData.append("info",this.obituaryForm.value.info);
    //  formData.append("datetime",this.obituaryForm.value.datetime);
     //formData.append("sub_category_id",47);
    this.makeFileRequest(this._url,formData, this.filesToUpload).then((result) => {
      console.log(result);
        if(result['Success']==true){
             this.alertClass = "alert alert-success text-center  alert-dismissible";
          if (this.obituaryForm.value.obituary_id==0) {
            this.errorMsg = 'Obituary Added SuccessFully';
          } else {
            this.errorMsg = 'Obituary Edited SuccessFully';
          }
  window.setTimeout(() => {
            this.Router.navigate(['obituary']);
          }, 3000);
        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = result['Message'];
        }
    
    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

}
