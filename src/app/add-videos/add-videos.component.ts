import { Component, OnInit, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-videos',
  templateUrl: './add-videos.component.html',
  styleUrls: ['./add-videos.component.css'],
  providers:[datacalls]
})
export class AddVideosComponent implements OnInit {
public videoForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    imagepath:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";

     a:any;
    filesToPost: any;
    file_keys: string[];
    id;
    alertClass: string;
    errorMsg: string;
    resized_images_list: { filename: string, url: string, dimension: string }[];
    isFileLoaded:boolean = false;


    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
    this.videoForm = this._fb.group({
            my_videos_id:[0],
            app_id:[13],
            title:['',Validators.required],
            description:['',Validators.required],
            path:['',Validators.required],
            file:[''],
            sub_category_id:[45],
            type:'video/mp4',
            category_id:[19]
        });
    }
    ngOnInit() {
      window.scrollTo(0,0);

//       this._datacalls.getAllCategory().subscribe(
//             data => {
//               if (data.Data.length > 0) {
//                 this.aboutusList = data.Data[0].data;
//                 console.log(this.aboutusList);
//               }
//             }
//           );
            
 this.activatedRoute.params.subscribe(
      (param: any) => {

        this.id = param['id'];
        console.log(this)
        if (this.id != undefined) {

          this._datacalls.getAllVideos(this.id).subscribe(
            
            data => {
              this.imagepath=data.Data[0].image_path;
              console.log("image path: "+data.Data[0].image_path)
              if (data.Data.length > 0) {
                 
                this.videoForm.patchValue({
                  'my_videos_id': data.Data[0].my_videos_id,
                  'app_id': 13,
                  'title': data.Data[0].title,
                  'description': data.Data[0].description,
                  'image_path':'http://35.154.141.107:3001/my_videos/youtube.png',
                  'path': data.Data[0].path,
                   'sub_category_id':45,
                   'type':'video/mp4',
                   'category_id':19
                });
                this.filesToUpload=new Array<File>();
              }
            }
          );
        
        }
      });
        
    

    }
    
    file: File;
    filesToUpload: Array<File>;
 
_url:string ="http://myappcenter.co.in/serverfileuploadapi/api/insertvideos";
  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
    this.isFileLoaded = true;
  }
  onSubmit() {
     
       // this.a = [{name :"priya"}];
    var formData: any = new FormData();
     formData.append("my_videos_id",this.videoForm.value.my_videos_id);
     formData.append("app_id",13);
     formData.append("user_std",'1st');
     formData.append("title",this.videoForm.value.title); 
     formData.append("description",this.videoForm.value.description);
      formData.append("division",'all');
     formData.append("image_path",'http://35.154.141.107:3001/my_videos/youtube.png');
    // formData.append("path", this.videoForm.value.path)
     formData.append("path",this.videoForm.value.path);
     formData.append("subcat_id",45);
     formData.append("category_id",19);
      formData.append("type",'video/mp4');
     this.makeFileRequest(this._url,formData, this.filesToUpload).then((result) => {
      console.log(result);
        if(result['Success']==true){
             this.alertClass = "alert alert-success text-center  alert-dismissible";
          if (this.videoForm.value.my_videos_id==0) {
            this.errorMsg = 'Videos Added SuccessFully';
          } else {
            this.errorMsg = 'Videos Edited SuccessFully';
          }
  window.setTimeout(() => {
            this.Router.navigate(['videos']);
          }, 3000);     
        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = result['Message'];
        }
    
    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      // for (var i = 0; i < files.length; i++) {
      //   formData.append("file", files[i], files[i].name);
      // }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

}
