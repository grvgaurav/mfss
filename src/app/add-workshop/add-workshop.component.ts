import { Component, OnInit, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';
  
import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-workshop',
  templateUrl: './add-workshop.component.html',
  styleUrls: ['./add-workshop.component.css'],
  providers:[datacalls]
})
export class AddWorkshopComponent implements OnInit {
    public workshopForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    aboutusList:any;
    imagepath:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";

    // public uploader: FileUploader = new FileUploader({ url: '35.154.141.107:4000/api/appcategory' });
     a:any;
    filesToPost: any;
    file_keys: string[];
    id;
    alertClass: string;
    errorMsg: string;
    resized_images_list: { filename: string, url: string, dimension: string }[];
    isFileLoaded:boolean = false;
    

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
    this.workshopForm = this._fb.group({
            workshop_id:[0],
            app_id:[7],
            workshop_name:['',Validators.required],
            description_1:['',Validators.required],
            file:[''],
            image_path:['']
        });
    }
    ngOnInit() {

//       this._datacalls.getAllCategory().subscribe(
//             data => {
//               if (data.Data.length > 0) {
//                 this.aboutusList = data.Data[0].data;
//                 console.log(this.aboutusList);
//               }
//             }
//           );
            
 this.activatedRoute.params.subscribe(
      (param: any) => {

        this.id = param['id'];
        console.log(this)
        if (this.id != undefined) {

          this._datacalls.getAllWorkshops(this.id).subscribe(
            
            data => {
              this.imagepath=data.Data[0].path;
              console.log(data.Data[0].image1)
              if (data.Data.length > 0) {
                 
                this.workshopForm = this._fb.group({
                  'workshop_id': data.Data[0].our_toppers_id,
                  'app_id': data.Data[0].app_id,
                  'workshop_name': data.Data[0].student_name,
                  'description_1': data.Data[0].subject,
                  'image_path': data.Data[0].path
                });
                this.filesToUpload=new Array<File>();
              }
            }
          );
        }
      });
        

    }
    
    file: File;
    filesToUpload: Array<File>;
 
_url:string ="http://35.154.141.107:6002/api/insertourtoppers";
  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
    this.isFileLoaded = true;
  }
  onSubmit() {
     
       // this.a = [{name :"priya"}];
    var formData: any = new FormData();
     formData.append("our_toppers_id",this.workshopForm.value.workshop_id);
     formData.append("app_id",this.workshopForm.value.app_id);
     formData.append("student_name",this.workshopForm.value.workshop_name);
     formData.append("subject",this.workshopForm.value.description_1);
     formData.append("image_path",this.workshopForm.value.image_path); 
    this.makeFileRequest(this._url,formData, this.filesToUpload).then((result) => {
      console.log(result);
        if(result['Success']==true){
             this.alertClass = "alert alert-success text-center  alert-dismissible";
          this.errorMsg = 'Workshop Added SuccessFully';
  window.setTimeout(() => {
            this.Router.navigate(['workshops']);
          }, 3000);

        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = result['Message'];
        }
    
    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }


}
