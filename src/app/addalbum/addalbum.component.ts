import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';
import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-addalbum',
  templateUrl: './addalbum.component.html',
  styleUrls: ['./addalbum.component.css'],
   providers:[datacalls]
})
export class AddalbumComponent implements OnInit {
    albumForm: FormGroup;
    submitted: boolean;
    events: any[] = [];
    imagepath:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";


    a:any;
    filesToPost: any;
    file_keys: string[];
    album_category_id;
    alertClass: string;
    errorMsg: string;
    resized_images_list: { filename: string, url: string, dimension: string }[];
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
    this.albumForm = this._fb.group({
            album_category_id:[0],
            app_id:[17],
            //album_category_id:['',Validators.required],
            catgeory_name:['',Validators.required],
            file:[''],
            image_path:['']
        });
    }

  ngOnInit() {
    window.scrollTo(0,0);

   this.activatedRoute.params.subscribe(
      (param: any) => {

        this.album_category_id = this.activatedRoute.snapshot.params['album_category_id'];
        console.log(this)
        if (this.album_category_id != undefined) {

          this._datacalls.getAlbum().subscribe(
            
            data => {
                            for(var i=0;i<data.Data.length;i++){

                              console.log( this.album_category_id +""+data.Data[i].album_category_id)

                              if(this.album_category_id ==data.Data[i].album_category_id){

                               this.imagepath=data.Data[i].image_path;
              console.log(data.Data[i].image_path)
                 
                this.albumForm.patchValue({
                  
                  'album_category_id':data.Data[i].album_category_id,
                  'app_id':17,
                  'catgeory_name': data.Data[i].catgeory_name, 
                  'file': data.Data[i].image_path,
                  'image1': data.Data[i].image_path
                });
                this.filesToUpload=new Array<File>();
                              }
                            }
                              }
                           
                       
          );
        }
      });
        

    }
    

    file: File;
    filesToUpload: Array<File>;
 
_url:string ="http://myappcenter.co.in/serverapi/api/insertalbum_category";
  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
    this.isFileLoaded = true;
    let size = fileInput.target.files[0].size;
    console.log("Size: "+ size);
    if(size>=500000) {
      this.isValidImage = false;
      console.log("isValidImage: "+ this.isValidImage);
    }else {
    this.isValidImage = true;
    console.log("isValidImage: "+ this.isValidImage);
  }
  }

onSubmit() {
     
      //  this.a = [{name :"priya"}];
     var formData: any = new FormData();
     console.log(formData)

     formData.append("album_category_id",this.albumForm.value.album_category_id);
     formData.append("app_id",17);
     formData.append("catgeory_name",this.albumForm.value.catgeory_name); 
     //formData.append("user_std",'5th');
     //formData.append("type","");
      //formData.append("sub_category_id",60);
    // formData.append("name",this.albumForm.value.book_name); 
      //formData.append("description",this.albumForm.value.description);
    // formData.append("rank",this.albumForm.value.price);
     //formData.append("file",this.albumForm.value.image_path);
     //formData.append("path","");
     //formData.append("category_id",53);
     formData.append("gaon_id",1);
     this.makeFileRequest(this._url,formData, this.filesToUpload).then((result) => {
      console.log(result);
        if(result['Success']==true){
             this.alertClass = "alert alert-success text-center  alert-dismissible";
         if (this.albumForm.value.album_category_id==0) {
            this.errorMsg = ' Photo Album Added SuccessFully';
          } else {
            this.errorMsg = 'Photo Album Edited SuccessFully';
          }
  window.setTimeout(() => {
            this.Router.navigate(['albumlist']);
          }, 3000);
     
        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = result['Message'];
        }
    
    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      // for (var i = 0; i < files.length; i++) {
      //   formData.append("file", files[i], files[i].name);
      // }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }
}