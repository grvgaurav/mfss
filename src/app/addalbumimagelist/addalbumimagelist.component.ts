import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';

@Component({
  selector: 'app-addalbumimagelist',
  templateUrl: './addalbumimagelist.component.html',
  styleUrls: ['./addalbumimagelist.component.css'],
  providers: [datacalls]
})
export class addalbumimagelistComponent implements OnInit {

  albumimageForm: FormGroup;
  submitted: boolean;
  events: any[] = [];
  imagepath: string = "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";


  a: any;
  filesToPost: any;
  file_keys: string[];
  album_category_id;
  album_images_id;
  alertClass: string;
  errorMsg: string;
  resized_images_list: { filename: string, url: string, dimension: string }[];
  isFileLoaded: boolean = false;
  isValidImage: boolean = true;

  constructor(private _fb: FormBuilder, private activatedRoute: ActivatedRoute, private _http: Http, private _datacalls: datacalls, private Router: Router) {
    this.albumimageForm = this._fb.group({
      album_category_id:[this.album_category_id],
      album_images_id: [0],
      app_id: [17],
      image_name: ['', Validators.required],
      // title:['',Validators.required],
      file: [''],
      image1: ['']
    });
  }

  ngOnInit() {
    window.scrollTo(0, 0);

    this.activatedRoute.params.subscribe(
      (param: any) => {
this.album_category_id = this.activatedRoute.snapshot.params['album_category_id'];
        this.album_images_id = this.activatedRoute.snapshot.params['album_images_id'];
        console.log(this.album_category_id+" "+this.album_images_id)
if (this.album_category_id != undefined) {
        if (this.album_images_id != undefined) {

          this._datacalls.getalbumimage(this.album_category_id).subscribe(


            data => {
              for (var i = 0; i < data.Data.length; i++) {

                console.log(this.album_images_id + " " + data.Data[i].album_images_id)

if (this.album_category_id == data.Data[i].category) {

                if (this.album_images_id == data.Data[i].album_images_id) {

                  this.imagepath = data.Data[i].path;
                  console.log(data.Data[i].image_path)

                  //if (data.Data.length > 0) {

                  this.albumimageForm.patchValue({

                    'image_name': data.Data[i].image_name,
                    'album_images_id': data.Data[i].album_images_id,
                    // 'file': data.Data[i].path,
                    'image1': data.Data[i].path
                  });
                  this.filesToUpload = new Array<File>();

                }
              }}


            }

          );
        }
}
      });


  }


  file: File;
  filesToUpload: Array<File>;

  _url: string = "http://myappcenter.co.in/serverfileuploadapi/api/insertalbum_images";
  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
    this.isFileLoaded = true;
    let size = fileInput.target.files[0].size;
    console.log("Size: " + size);
    if (size >= 500000) {
      this.isValidImage = false;
      console.log("isValidImage: " + this.isValidImage);
    } else {
      this.isValidImage = true;
      console.log("isValidImage: " + this.isValidImage);
    }
  }

  onSubmit() {

    // this.a = [{name :"priya"}];


    var formData: any = new FormData();
    formData.append("album_images_id", this.albumimageForm.value.album_images_id);
    // formData.append("address",this.albumimageForm.value.address);
    formData.append("image_name", this.albumimageForm.value.image_name);
    //formData.append("name",this.albumimageForm.value.book_name); 
    //formData.append("description",this.albumimageForm.value.description);
    //formData.append("rank",this.albumimageForm.value.price);
    formData.append("file", this.albumimageForm.value.image1);
    formData.append("app_id", 17);
    
    formData.append("category",this.album_category_id);


    this.makeFileRequest(this._url, formData, this.filesToUpload).then((result) => {
      console.log(result);
      if (result['Success'] == true) {
        this.alertClass = "alert alert-success text-center  alert-dismissible";
        if (this.albumimageForm.value.album_images_id == 0) {
          this.errorMsg = 'Photo Album Image Added SuccessFully';
        } else {
          this.errorMsg = 'Photo Album Image Edited SuccessFully';
        }
        window.setTimeout(() => {
          this.Router.navigate(['viewalbum/'+this.album_category_id]);
        }, 3000);

      }
      else {
        this.alertClass = "alert alert-danger text-center  alert-dismissible";
        this.errorMsg = result['Message'];
      }

    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }

      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }
}

