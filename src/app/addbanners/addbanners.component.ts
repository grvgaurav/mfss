import { Component, OnInit,ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-addbanners',
  templateUrl: './addbanners.component.html',
  styleUrls: ['./addbanners.component.css'],
  providers: [datacalls]
})
export class AddbannersComponent implements OnInit {

  public bannersForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    aboutusList:any;
    imagepath:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";

    a:any;
    filesToPost: any;
    file_keys: string[];
    id;
    sid;
    alertClass: string;
    errorMsg: string;
    resized_images_list: { filename: string, url: string, dimension: string }[];
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
    this.bannersForm = this._fb.group({
            forum_banner_id:[0],
            forum_id:[0],
            app_id:[17],
            url:['',Validators.required],
            file:[''],
            image:[''],
        });
    }
    ngOnInit() {
      window.scrollTo(0,0);

//       this._datacalls.getAllCategory().subscribe(
//             data => {
//               if (data.Data.length > 0) {
//                 this.aboutusList = data.Data[0].data;
//                 console.log(this.aboutusList);
//               }
//             }
//           );
            
 this.activatedRoute.params.subscribe(
      (param: any) => {

        this.id = param['forum_id'];
        this.sid = param['forum_banner_id'];
        console.log('id'+this.id+" "+'sid'+this.sid)
        if (this.id != undefined) {
        if (this.sid != undefined) {

          this._datacalls.getBanners(this.id).subscribe(
            
            data => {
                            for(var i=0;i<data.Data.length;i++){

                        //      console.log( this.id +""+data.Data[i].id)
if(this.id ==data.Data[i].forum_id){
                              if(this.sid ==data.Data[i].forum_banner_id){
                         console.log(data.Data[i].id)
                               this.imagepath=data.Data[i].image;
              console.log(data.Data[i].image)
                 
                this.bannersForm.patchValue({
                  'forum_banner_id':data.Data[i].forum_banner_id,
                  'forum_id': data.Data[i].forum_id,
                  'app_id': 17,
                  'url': data.Data[i].url,
                  'file': data.Data[i].image,
                  'image': data.Data[i].image,
                });
                this.filesToUpload=new Array<File>();
                              }
              }
                             } }
          );
        }
      }});
        

    }
    
    file: File;
    filesToUpload: Array<File>;
 
_url:string ="http://myappcenter.co.in/serverfileuploadapi/api/insertforumbanner";
  onChange(fileInput: any) {
    console.log("this is image: "+ this.bannersForm.value.image);
    this.filesToUpload = <Array<File>>fileInput.target.files;
    this.isFileLoaded = true;
    let size = fileInput.target.files[0].size;
    console.log("Size: "+ size);
    if(size>=500000) {
      this.isValidImage = false;
      console.log("isValidImage: "+ this.isValidImage);
    }else {
    this.isValidImage = true;
    console.log("isValidImage: "+ this.isValidImage);
  }
  }
  onSubmit() {
     
       // this.a = [{name :"priya"}];
       console.log(this.bannersForm.value.image);
    var formData: any = new FormData();
     formData.append("forum_banner_id",this.bannersForm.value.forum_banner_id);
     formData.append("app_id",17);
     formData.append("url",this.bannersForm.value.url);
     formData.append("file",this.bannersForm.value.image); 
     formData.append("image",this.bannersForm.value.image); 
     formData.append("forum_id",this.id);
    this.makeFileRequest(this._url,formData, this.filesToUpload).then((result) => {
      console.log(result);
        if(result['Success']==true){
             this.alertClass = "alert alert-success text-center  alert-dismissible";
          if (this.bannersForm.value.forum_banner_id==0) {
            this.errorMsg = 'Forum Banner Added SuccessFully';
          } else {
            this.errorMsg = 'Forum Banner Edited SuccessFully';
          }
  window.setTimeout(() => {
            this.Router.navigate(['viewbanners/'+this.id]);
          }, 3000);
        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = result['Message'];
        }
    
    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

}
