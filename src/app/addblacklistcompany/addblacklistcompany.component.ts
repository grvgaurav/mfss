import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';

@Component({
  selector: 'app-addblacklistcompany',
  templateUrl: './addblacklistcompany.component.html',
  styleUrls: ['./addblacklistcompany.component.css'],
  providers: [datacalls]
})
export class AddblacklistcompanyComponent implements OnInit {

  blacklistForm: FormGroup;
    submitted: boolean;
    events: any[] = [];
    imagepath:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";


    a:any;
    filesToPost: any;
    file_keys: string[];
    blacklist_id;
    alertClass: string;
    errorMsg: string;
    resized_images_list: { filename: string, url: string, dimension: string }[];
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
    this.blacklistForm = this._fb.group({
            blacklist_id:[0],
            app_id:[13],
            company_name:['',Validators.required],
            location:['',Validators.required],
            file:[''],
            image_path:['']
        });
    }

  ngOnInit() {

   this.activatedRoute.params.subscribe(
      (param: any) => {

        this.blacklist_id = this.activatedRoute.snapshot.params['blacklist_id'];
        console.log(this)
        if (this.blacklist_id != undefined) {

          this._datacalls.getBlacklistcompany().subscribe(
            
            data => {
                            for(var i=0;i<data.Data.length;i++){

                              console.log( this.blacklist_id +""+data.Data[i].blacklist_id)

                              if(this.blacklist_id ==data.Data[i].blacklist_id){

                               this.imagepath=data.Data[i].image_path;
              console.log(data.Data[i].image_path)
                 
                this.blacklistForm = this._fb.group({
                  
                  'blacklist_id':data.Data[i].blacklist_id,
                  'app_id':data.Data[i].app_id,
                  'company_name': data.Data[i].company_name,
                  'location': data.Data[i].location, 
                  'file': data.Data[i].image_path,
                  'image1': data.Data[i].image_path
                });
                this.filesToUpload=new Array<File>();
                              }
                            }
                              }
                           
                       
          );
        }
      });
        

    }
    

    file: File;
    filesToUpload: Array<File>;
 
_url:string ="http://35.154.141.107:6001/api/insertblacklistedcompany/";
  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
    this.isFileLoaded = true;
    let size = fileInput.target.files[0].size;
    console.log("Size: "+ size);
    if(size>=500000) {
      this.isValidImage = false;
      console.log("isValidImage: "+ this.isValidImage);
    }else {
    this.isValidImage = true;
    console.log("isValidImage: "+ this.isValidImage);
  }
  }

onSubmit() {
     
      //  this.a = [{name :"priya"}];
     var formData: any = new FormData();
     console.log(formData)
     
     formData.append("blacklist_id",this.blacklistForm.value.blacklist_id);
     formData.append("app_id",this.blacklistForm.value.app_id);
     formData.append("company_name",this.blacklistForm.value.company_name);
     formData.append("location",this.blacklistForm.value.location); 
     //formData.append("user_std",'5th');
     //formData.append("type","");
      //formData.append("sub_category_id",60);
    // formData.append("name",this.blacklistForm.value.book_name); 
      //formData.append("description",this.blacklistForm.value.description);
    // formData.append("rank",this.blacklistForm.value.price);
     //formData.append("file",this.blacklistForm.value.image_path);
     //formData.append("path","");
     //formData.append("category_id",53);
    // formData.append("sub_category_id",60);
     this.makeFileRequest(this._url,formData, this.filesToUpload).then((result) => {
      console.log(result);
        if(result['Success']==true){
             this.alertClass = "alert alert-success text-center  alert-dismissible";
          this.errorMsg = 'Blacklisted Company Added SuccessFully';
  window.setTimeout(() => {
            this.Router.navigate(['blacklistedcompany']);
          }, 3000);
     
        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = result['Message'];
        }
    
    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      // for (var i = 0; i < files.length; i++) {
      //   formData.append("file", files[i], files[i].name);
      // }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }
}