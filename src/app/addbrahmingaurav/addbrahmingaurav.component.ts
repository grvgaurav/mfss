import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';

@Component({
  selector: 'app-addbrahmingaurav',
  templateUrl: './addbrahmingaurav.component.html',
  styleUrls: ['./addbrahmingaurav.component.css'],
  providers:[datacalls]
})
export class AddbrahmingauravComponent implements OnInit {

  public brahmingauravForm: FormGroup;
  public submitted: boolean;
  public events: any[] = [];
  policiesList: any;
  imagepath: string = "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";

  a: any;
  filesToPost: any;
  file_keys: string[];
  id;

  alertClass: string;
  errorMsg: string;
  resized_images_list: { filename: string, url: string, dimension: string }[];
  isFileLoaded: boolean = false;
  isValidImage: boolean = true;


  constructor(private _fb: FormBuilder, private activatedRoute: ActivatedRoute, private _http: Http, private _datacalls: datacalls, private Router: Router) {
    this.brahmingauravForm = this._fb.group({
      our_toppers_id: [0],
      app_id: [13],
      std: ['1st'],
      student_name: ['', Validators.required],
      rank: ['', Validators.required],
      subject: ['', Validators.required],
      image_path: [''],
      year: ['', Validators.required],
      datetime: [''],
      percentage: ['']


    });
  }
  ngOnInit() {
    window.scrollTo(0,0);
    this.activatedRoute.params.subscribe(
      (param: any) => {

        this.id = param['id'];
        console.log(this)
        if (this.id != undefined) {
          this._datacalls.getBrahmingaurav().subscribe(

            data => {
             // console.log('edit data:', data.Data);
              for (var i = 0; i < data.Data.length; i++) {
                if (this.id == data.Data[i].our_toppers_id) {
                  this.imagepath = data.Data[i].path;
                  console.log('dataaas:',data.Data[i].path)
                  //console.log('edit data descrip:', data.Data[i].subject);
                  this.brahmingauravForm.patchValue({
                    'our_toppers_id': data.Data[i].our_toppers_id,
                    'app_id':13,
                    'std':'1st',
                    'student_name':data.Data[i].student_name,
                    'rank':data.Data[i].rank,
                    'subject': data.Data[i].subject,
                     'image_path':data.Data[i].path,
                     'year': data.Data[i].year,
                     'percentage':data.Data[i].percentage


                  });

                  this.filesToUpload = new Array<File>();
                 
                }
              }
            }
          );
        }
      });


  }

  file: File;
  filesToUpload: Array<File>;

  _url: string = "http://myappcenter.co.in/serverfileuploadapi/api/insertourtoppers";
  onChange(fileInput: any) {
    this.isFileLoaded = true;
    this.filesToUpload = <Array<File>>fileInput.target.files;
    let size = fileInput.target.files[0].size;
    console.log("fileupload: " , this.filesToUpload);
    console.log("Size: " + size);
    if (size >= 500000) {
      this.isValidImage = false;
      console.log("isValidImage: " + this.isValidImage);
    } else {
      this.isValidImage = true;
      console.log("isValidImage: " + this.isValidImage);
    }

  }

  onSubmit() {
    var formData: any = new FormData();
    
    formData.append("our_toppers_id", this.brahmingauravForm.value.our_toppers_id);
    formData.append("student_name", this.brahmingauravForm.value.student_name);
    formData.append("user_std",'1st');
    formData.append("subject", this.brahmingauravForm.value.subject);
    formData.append("percentage",null);
    formData.append("rank",this.brahmingauravForm.value.rank);
    formData.append("year",this.brahmingauravForm.value.year);
    formData.append("image_path", this.brahmingauravForm.value.image_path);
    //formData.append("datetime",'');
    formData.append("app_id",13);
    console.log('formdata'+formData);
    this.makeFileRequest(this._url, formData, this.filesToUpload).then((result) => {
      console.log('result'+result);
      if (result['Success'] == true) {
        this.alertClass = "alert alert-success text-center  alert-dismissible";
        if (this.brahmingauravForm.value.our_toppers_id==0) {
            this.errorMsg = 'Brahmin Gaurav Added SuccessFully';
          } else {
            this.errorMsg = 'Brahmin Gaurav Edited SuccessFully';
          }
        window.setTimeout(() => {
          this.Router.navigate(['brahmingaurav']);
        }, 3000);
      }
      else {
        this.alertClass = "alert alert-danger text-center  alert-dismissible";
        this.errorMsg = result['Message'];
      }

    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }

      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

}