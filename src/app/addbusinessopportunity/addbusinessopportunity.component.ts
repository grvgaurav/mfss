import { Component, OnInit, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-addbusinessopportunity',
  templateUrl: './addbusinessopportunity.component.html',
  styleUrls: ['./addbusinessopportunity.component.css'],
  providers:[datacalls]
})
export class AddbusinessopportunityComponent implements OnInit {

   public businessopportunityForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    bodList:any;
    imagepath:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";

    a:any;
    filesToPost: any;
    file_keys: string[];
    training_id;
    alertClass: string;
    errorMsg: string;
    resized_images_list: { filename: string, url: string, dimension: string }[];
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;
    

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
    this.businessopportunityForm = this._fb.group({
            training_id:[0],
            app_id:[13],
            title:['',Validators.required],
            description:['',Validators.required],
            image_path:[''],
            type:['',Validators.required],
            opportunity_des:['',Validators.required],
            product_range_des:['',Validators.required],
            fees:['',Validators.required],
            investment_required:['',Validators.required],
            duration:['',Validators.required],
            location:['',Validators.required],
            training_type:['',Validators.required],
            remark:['',Validators.required]

        });
    }
    ngOnInit() {
 window.scrollTo(0,0);
//       this._datacalls.getAllCategory().subscribe(
//             data => {
//               if (data.Data.length > 0) {
//                 this.bodList = data.Data[0].data;
//                 console.log(this.bodList);
//               }
//             }
//           );
            
 this.activatedRoute.params.subscribe(
      (param: any) => {

        this.training_id= param['training_id'];
        console.log(this)
        if (this.training_id!= undefined) {

          this._datacalls.getBusinessopportunity().subscribe(

                  data => {
                            for(var i=0;i<data.Data.length;i++){

                              console.log( this.training_id+""+data.Data[i].training_id)

                              if(this.training_id==data.Data[i].training_id){

                               this.imagepath=data.Data[i].image_path;
              console.log(data.Data[i].image_path)
                this.businessopportunityForm.patchValue({
                  'training_id': data.Data[i].training_id,
                  'app_id':13,
                  'title': data.Data[i].title,
                  'image_path':data.Data[i].image_path,
                  'type': data.Data[i].type,
                  'description': data.Data[i].description,
                  'product_range_des': data.Data[i].product_range_des,
                  'fees': data.Data[i].fees,
                  'opportunity_des': data.Data[i].opportunity_des,
                  'investment_required': data.Data[i].investment_required,
                  'duration': data.Data[i].duration,
                  'location': data.Data[i].location,
                  'training_type': data.Data[i].training_type,
                  'remark': data.Data[i].remark
                });
                this.filesToUpload=new Array<File>();
                        
                  }
              }
            }
          );
        }
      });
        

    }
    
    file: File;
    filesToUpload: Array<File>;
  
_url:string ="http://myappcenter.co.in/serverfileuploadapi/api/inserttraining";
  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files; 
    this.isFileLoaded = true;
    let size = fileInput.target.files[0].size;
    console.log("Size: "+ size);
    if(size>=500000) {
      this.isValidImage = false;
      console.log("isValidImage: "+ this.isValidImage);
    }else {
    this.isValidImage = true;
    console.log("isValidImage: "+ this.isValidImage);
  }
  }
  onSubmit() {
   
    var formData: any = new FormData();
    console.log(formData);
     formData.append("training_id",this.businessopportunityForm.value.training_id);
     formData.append("user_std","1st");
     formData.append("category_id",38);
     formData.append("sub_category_id",0);
     formData.append("type",this.businessopportunityForm.value.type);
     formData.append("title",this.businessopportunityForm.value.title);
     formData.append("training_type",this.businessopportunityForm.value.training_type);
      formData.append("location",this.businessopportunityForm.value.location);
       formData.append("fees",this.businessopportunityForm.value.fees);
       formData.append("duration",this.businessopportunityForm.value.duration);
     
     formData.append("last_date",'0001-01-01');
   
     formData.append("image_path",this.businessopportunityForm.value.image_path);
     formData.append("path","NA");
    
     formData.append("description",this.businessopportunityForm.value.description); 
      formData.append("app_id",13);     
     formData.append("product_range_des",this.businessopportunityForm.value.product_range_des);
     formData.append("opportunity_des",this.businessopportunityForm.value.opportunity_des);  
     
     formData.append("investment_required",this.businessopportunityForm.value.investment_required);
      
      
      formData.append("remark",this.businessopportunityForm.value.remark);
      
      
      
      
    
    this.makeFileRequest(this._url,formData, this.filesToUpload).then((result) => {
      console.log(result);
        if(result['Success']==true){
             this.alertClass = "alert alert-success text-center  alert-dismissible";
          if (this.businessopportunityForm.value.training_id==0) {
            this.errorMsg = 'Business Opportunity Added SuccessFully';
          } else {
            this.errorMsg = 'Business Opportunity Edited SuccessFully';
          }
  window.setTimeout(() => {
            this.Router.navigate(['businessopportunity']);
          }, 300);
        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = result['Message'];
        }
    
    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

}
