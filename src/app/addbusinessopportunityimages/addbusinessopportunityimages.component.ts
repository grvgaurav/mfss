import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';

@Component({
  selector: 'app-addbusinessopportunityimages',
  templateUrl: './addbusinessopportunityimages.component.html',
  styleUrls: ['./addbusinessopportunityimages.component.css'],
  providers: [datacalls]
})
export class AddbusinessopportunityimagesComponent implements OnInit {

  public businessopportunityimagesForm: FormGroup;
  public submitted: boolean;
  public events: any[] = [];
  //policiesList:any;
  imagepath: string = "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";

  a: any;
  filesToPost: any;
  file_keys: string[];
  training_id;
  training_images_id;
  alertClass: string;
  errorMsg: string;
  resized_images_list: { filename: string, url: string, dimension: string }[];
  isFileLoaded: boolean = false;
  isValidImage: boolean = true;


  constructor(private _fb: FormBuilder, private activatedRoute: ActivatedRoute, private _http: Http, private _datacalls: datacalls, private Router: Router) {
    this.businessopportunityimagesForm = this._fb.group({
      training_images_id: [0],
      app_id: [13],
      training_id: [this.training_id],
      // teacher_id:[this.id],
      // type:['application/pdf'],
      // title:['',Validators.required],
      //image_path:['',Validators.required],
      image_path: ['', Validators.required]
      // description:['',Validators.required],


    });
  }
  ngOnInit() {
    window.scrollTo(0, 0);
    //       this._datacalls.getAllCategory().subscribe(
    //             data => {
    //               if (data.Data.length > 0) {
    //                 this.policiesList = data.Data[0].data;
    //                 console.log(this.policiesList);
    //               }
    //             }
    //           );

    this.activatedRoute.params.subscribe(
      (param: any) => {

        this.training_id = param['training_id'];
        this.training_images_id = param['training_images_id'];
        // this.pid = param['repositorypdf_id'];
        //this.pid = param['repositorypdf_id'];
        console.log(this)
        if (this.training_id != undefined) {
          if (this.training_images_id != undefined) {

            this._datacalls.getBusinessopportunity().subscribe(

              data => {
                //console.log('edit data:',data.Data.multiple_images);
                for (var j = 0; j < data.Data.length; j++) {
                  for (var i = 0; i < data.Data[j].multiple_images.length; i++) {

                    console.log(this.training_images_id + " " + data.Data[j].multiple_images[i].training_images_id)
                    if (this.training_id == data.Data[j].training_id) {
                      if (this.training_images_id == data.Data[j].multiple_images[i].training_images_id) {

                        this.imagepath = data.Data[j].multiple_images[i].image_path;
                        console.log(data.Data[j].multiple_images[i].image_path)

                        this.businessopportunityimagesForm.patchValue({
                          'training_images_id': data.Data[j].multiple_images[i].training_images_id,
                          'app_id': 13,

                          'training_id': this.training_id,

                          'image_path': data.Data[j].multiple_images[i].image_path,



                        });

                        this.filesToUpload = new Array<File>();

                      }
                    }
                  }
                }
              });
          }
        }
      });


  }

  file: File;
  filesToUpload: Array<File>;

  _url: string = "http://myappcenter.co.in/serverfileuploadapi/api/inserttraining_images";
  onChange(fileInput: any) {
    this.isFileLoaded = true;
    this.filesToUpload = <Array<File>>fileInput.target.files;
    let size = fileInput.target.files[0].size;
    console.log("Size: " + size);
    if (size >= 500000) {
      this.isValidImage = false;
      console.log("isValidImage: " + this.isValidImage);
    } else {
      this.isValidImage = true;
      console.log("isValidImage: " + this.isValidImage);
    }

  }

  onSubmit() {
    var formData: any = new FormData();
    formData.append("training_images_id", this.businessopportunityimagesForm.value.training_images_id);
    formData.append("app_id", 13);

    formData.append("training_id", this.training_id);

    formData.append("file", this.businessopportunityimagesForm.value.image_path);

    console.log(formData)
    this.makeFileRequest(this._url, formData, this.filesToUpload).then((result) => {
      console.log(result);
      if (result['Success'] == true) {
        this.alertClass = "alert alert-success text-center  alert-dismissible";
      if (this.businessopportunityimagesForm.value.training_images_id==0) {
            this.errorMsg = 'Business Opportunity Images Added SuccessFully';
          } else {
            this.errorMsg = 'Business Opportunity Images Edited SuccessFully';
          }
        window.setTimeout(() => {
          this.Router.navigate(['businessopportunityimages/' + this.training_id]);
        }, 3000);
      }
      else {
        this.alertClass = "alert alert-danger text-center  alert-dismissible";
        this.errorMsg = result['Message'];
      }

    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }

      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

}

