import { Component, OnInit, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-addcalender',
  templateUrl: './addcalender.component.html',
  styleUrls: ['./addcalender.component.css'], 
  providers:[datacalls]
})
export class AddcalenderComponent implements OnInit {

  public calenderForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    imagepath:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";

    a:any;
    filesToPost: any;
    file_keys: string[];
    app_calendar_id;
    d;
    currentTime;
    alertClass: string;
    errorMsg: string;
    resized_images_list: { filename: string, url: string, dimension: string }[];
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
    this.calenderForm = this._fb.group({
            app_calendar_id:[0],
            app_id:[17],
            title:['',Validators.required],
            subject:['',Validators.required],
            
            event_date:['',Validators.required],
            description:['',Validators.required],
            venue:['',Validators.required],
             event_time:['',Validators.required],
             file:[''],
            //image_path:['']
        });
    }
   
    ngOnInit() {
      window.scrollTo(0,0);

//       this._datacalls.getAllCategory().subscribe(
//             data => {
//               if (data.Data.length > 0) {
//                 this.aboutusList = data.Data[0].data;
//                 console.log(this.aboutusList);
//               }
//             }
//           );
            
 this.activatedRoute.params.subscribe(
      (param: any) => {

        this.app_calendar_id = param['app_calendar_id'];
        console.log(this)
        if (this.app_calendar_id != undefined) {

          this._datacalls.getCalender().subscribe(
            
            data => {
    console.log(data);
                           for(var i=0;i<data.Data.length;i++){

                        //      console.log( this.id +""+data.Data[i].id)

                              if(this.app_calendar_id ==data.Data[i].app_calendar_id){

                               this.imagepath=data.Data[i].image_path;
              
                 
              // this.imagepath=data.Data[i].path;
              // if (data.Data.length > 0) 
                
                this.calenderForm.patchValue({
                  'app_calendar_id': data.Data[i].app_calendar_id,
                  'app_id': 17,
                  'title':data.Data[i].title,
                  'subject':data.Data[i].subject,
                  'event_date': data.Data[i].event_date.toString("dd-MM-yyyy"),
                  'description': data.Data[i].description,
                  'venue': data.Data[i].venue,
                  'event_time': data.Data[i].event_time,
                  'image_path': data.Data[i].image_path,
                  
                  
                  
                  // 'image_path': data.Data[0].path
                });
                this.filesToUpload=new Array<File>();
              
              
            } 
                           }}
          );
        }
      });
       

    }
   File;
    filesToUpload: Array<File>;
 
_url:string ="http://myappcenter.co.in/serverfileuploadapi/api/insertsmartgaoncalendar";
  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
    let size = fileInput.target.files[0].size;
    console.log("Size: "+ size);
    if(size>=500000) {
      this.isValidImage = false;
      console.log("isValidImage: "+ this.isValidImage);
    }else {
    this.isValidImage = true;
    console.log("isValidImage: "+ this.isValidImage);
  }
    this.isFileLoaded = true;
  }
   
  onSubmit() {
  
       // this.a = [{name :"priya"}];
     var formData: any = new FormData();
     formData.append("app_calendar_id",this.calenderForm.value.app_calendar_id);
     formData.append("app_id", 17);
     formData.append("title",this.calenderForm.value.title); 
     formData.append("subject",this.calenderForm.value.subject);
     formData.append("event_date",this.calenderForm.value.event_date.toString("dd-MM-yyyy"));
     formData.append("description",this.calenderForm.value.description);
     formData.append("venue",this.calenderForm.value.venue);
     formData.append("time",this.calenderForm.value.event_time);
     formData.append("file",this.calenderForm.value.image_path);
     formData.append("created_by",17);
     formData.append("gaon_id",1);
     
     this.makeFileRequest(this._url,formData, this.filesToUpload).then((result) => {
      console.log(result);
      
        if(result['Success']==true){
             this.alertClass = "alert alert-success text-center  alert-dismissible";
          if (this.calenderForm.value.app_calendar_id==0) {
            this.errorMsg = 'Calendar Event Added SuccessFully';
          } else {
            this.errorMsg = 'Calendar Event Edited SuccessFully';
          }
  window.setTimeout(() => {
            this.Router.navigate(['calender']);
          }, 1000);
     
        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = result['Message'];
        }
    
    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }


}

