import { Component, OnInit, ChangeDetectorRef   } from '@angular/core';
import {FormGroup,FormBuilder,FormControl,Validators} from '@angular/forms';
import { ActivatedRoute} from '@angular/router';
import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import { Router } from '@angular/router';

@Component({

  selector: 'app-addcategory',
  templateUrl: './addcategory.component.html',
  styleUrls: ['./addcategory.component.css'],
  providers:[datacalls]
})
export class AddcategoryComponent implements OnInit {
public categoryForm : FormGroup;
public submitted :boolean;
public events:any[]=[];categorylist:any;
public uploader: FileUploader = new FileUploader({ url: '35.154.141.107:4000/api/aryacategory' });
a:any;
isSubcat:boolean;
editSubcat:boolean = false;
editCat:boolean = false;
filesToPost: any;
file_keys: string[];
id;
parent_id;
cat_id;
resized_images_list: { filename: string, url: string, dimension: string }[];
alertClass: string;
errorMsg: string;
isFileLoaded:boolean = false;
imagePath:string;
  constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls, private changeDetectorRef: ChangeDetectorRef,private fb:FormBuilder, private Router: Router) {        
    this.categoryForm=this._fb.group({
      category_id:[''],
      parent_id: ['0'],
      name:[null,[Validators.required,Validators.minLength(5)]],
      file:[''],
      image1:['']
    });

   }

  ngOnInit() {
    window.scrollTo(0,0);
this._datacalls.getCategory().subscribe(
      data => {
        if (data.Data.length> 0) {
          this.categorylist = data.Data;
          console.log(this.categorylist);
        }
      }
    );
  
      this.activatedRoute.params.subscribe(
      (param: any) => {

        this.id = param['id'];
        this.parent_id = param['parent_id'];
        console.log(this)
        if (this.id != undefined) {
          this.cat_id = this.id;
          this._datacalls.getCatdetails(this.id).subscribe(
            
            data => {
              console.log("image src:",data.Data[0].app_image)
              if (data.Data.length > 0) {
                console.log("name:",data.Data)
                this.categoryForm.patchValue({
                  'category_id': data.Data[0].arya_category_id,
                  'name': data.Data[0].category_name,
                  'image1': data.Data[0].app_image
                });
              this.imagePath = this.categoryForm.value.image1;              }
            },
            error => { },
            () => {

            }
          );
        }
      });

      if (this.activatedRoute.routeConfig.path == 'addsubcategory/:parent_id') {
        this.isSubcat = true;
    } else {
        this.isSubcat = false;
    }
    if (this.activatedRoute.routeConfig.path == 'category/addsubcategory/:parent_id/:id') {

        this.editSubcat = true;
    }
    if (this.activatedRoute.routeConfig.path == 'category/addcategory/:id') {

        this.editCat = true;
    } 
        


    }

 


    callHttpPostWithFile(postData: any, files: File[], file_keys: string[]) {

        

        var api = "http://myappcenter.co.in/serverfileuploadapi/api/aryacategory"
        let formData: FormData = new FormData();
        let headers = new Headers();        
        headers.append('enctype', 'multipart/form-data');     
console.log(files.length)
        for (let i = 0; i < files.length; i++) {

            formData.append(file_keys[i], files[i], files[i].name);

        }

        if (postData !== "" && postData !== undefined && postData !== null) {
            for (var property in postData) {
                if (postData.hasOwnProperty(property)) {
                    formData.append(property, postData[property]);
                }
            }
        }

        return this._http.post(api, formData, { headers: headers }).map((response: Response) => {
            var res: any = response;
            var json = JSON.parse(res._body);
            alert(json);
            
        });
    }





    file: File;
  filesToUpload: Array<File>;
 
_url:string ="http://myappcenter.co.in/serverfileuploadapi/api/aryacategory";
  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
    this.isFileLoaded = true;
  }
  onSubmit() {
    
        this.a = [{name :"priya"}];
    var formData: any = new FormData();

    if (this.activatedRoute.routeConfig.path == 'category/addsubcategory/:parent_id') {
        this.isSubcat = true;
        this.categoryForm.value.parent_id = this.parent_id;
    } else {
        this.isSubcat = false;
        this.categoryForm.value.parent_id = '0'
    }

    if (this.activatedRoute.routeConfig.path == 'category/addsubcategory/:parent_id/:id') {

        console.log('category_id: '+ this.cat_id);
        this.categoryForm.value.category_id = this.cat_id;
        this.categoryForm.value.parent_id = this.parent_id;
    }


    if (this.categoryForm.value.category_id==""){
      this.categoryForm.value.category_id=0;
    }
    formData.append("arya_category_id",this.categoryForm.value.category_id); 
    formData.append("app_id",17); 
     formData.append("category_name",this.categoryForm.value.name);
     formData.append("parent_id",this.categoryForm.value.parent_id); 
      formData.append("gaon_id",1);
    console.log("name:"+this.categoryForm.value.name);
    console.log("parent_id:"+this.categoryForm.value.parent_id);
    console.log(this.filesToUpload)
    this.makeFileRequest(this._url,formData, this.filesToUpload).then((result) => {
      console.log(result);
      if(result['Success']==true){
          this.alertClass = "alert alert-success text-center alert-dismissible";
          if (this.categoryForm.value.category_id==0) {
            this.errorMsg = 'Category Added SuccessFully';
          } else {
            this.errorMsg = 'Category Edited SuccessFully';
          }
          window.setTimeout(() => {
                    this.Router.navigate(['category']);
                  }, 2500);
   // this.Router.navigate(['productlist']);
     
        }
        else {
          this.alertClass = "alert alert-danger text-center alert-dismissible";
          this.errorMsg = result['Message'];
        }
    
    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }





}


export interface CategoryData{
  name : string;
} 