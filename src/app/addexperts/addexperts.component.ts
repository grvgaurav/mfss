import { Component, OnInit,ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-addexperts',
  templateUrl: './addexperts.component.html',
  styleUrls: ['./addexperts.component.css'],
  providers: [datacalls]
})
export class AddexpertsComponent implements OnInit {

    public addexpertsForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    expertsList:any;
    imagepath:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";

    a:any;
    filesToPost: any;
    file_keys: string[];
    id;
    alertClass: string;
    errorMsg: string;
    resized_images_list: { filename: string, url: string, dimension: string }[];
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
     let emailRegex = '^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$';
    let contactRegex='^([0-9\(\)\/\+ \-]*)$';
    this.addexpertsForm = this._fb.group({
            id:[0],
            appid:[13],
            name:['',Validators.required],
            mobile:['', [<any>Validators.required,Validators.maxLength(10),Validators.minLength(10),<any>Validators.pattern(contactRegex)]],
            email:['', [<any>Validators.required,  <any>Validators.pattern(emailRegex) ]],
            password:['',Validators.required],
            //file:[''],
            //image_path:['']
        });
    }
    ngOnInit() {
      window.scrollTo(0,0);

//       this._datacalls.getAllCategory().subscribe(
//             data => {
//               if (data.Data.length > 0) {
//                 this.expertsList = data.Data[0].data;
//                 console.log(this.expertsList);
//               }
//             }
//           );
            
 this.activatedRoute.params.subscribe(
      (param: any) => {

        this.id = param['id'];
        console.log(this)
        if (this.id != undefined) {

          this._datacalls.getExperts().subscribe(
            
            data => {
                            for(var i=0;i<data.Data.length;i++){

                        //      console.log( this.id +""+data.Data[i].id)

                              if(this.id ==data.Data[i].id){

                               this.imagepath=data.Data[i].image_path;
              console.log(data.Data[i].image_path)
                 
                this.addexpertsForm.patchValue({
                  'id': data.Data[i].id,
                  'appid':13,
                  'name': data.Data[i].name,
                  'mobile': data.Data[i].mobile_no,
                  'email': data.Data[i].email,
                  'password': data.Data[i].password,
                  'usertype':'expert',
                  //'image_path': data.Data[i].image_path
                });
                this.filesToUpload=new Array<File>();
                              }
              }
            }
          );
        }
      });
        

    }
    
    file: File;
    filesToUpload: Array<File>;
 
_url:string ="http://myappcenter.co.in/serverapi/api/usersignup";
  onChange(fileInput: any) {
    console.log("this is image: "+ this.addexpertsForm.value.image_path);
    this.filesToUpload = <Array<File>>fileInput.target.files;
    this.isFileLoaded = true;
    let size = fileInput.target.files[0].size;
    console.log("Size: "+ size);
    if(size>=500000) {
      this.isValidImage = false;
      console.log("isValidImage: "+ this.isValidImage);
    }else {
    this.isValidImage = true;
    console.log("isValidImage: "+ this.isValidImage);
  }
  }
  onSubmit() {
     
       // this.a = [{name :"priya"}];
       console.log(this.addexpertsForm.value.mobile);
    var formData: any = new FormData();
     formData.append("cust_id",this.addexpertsForm.value.id);
     formData.append("appid",13);
     formData.append("name",this.addexpertsForm.value.name);
     formData.append("mobile",this.addexpertsForm.value.mobile);
     formData.append("email",this.addexpertsForm.value.email);
     formData.append("password",this.addexpertsForm.value.password); 
     formData.append("usertype",'expert');
    //  formData.append("address",'');
    //  formData.append("pincode",'');
    //  formData.append("city",'');
    //  formData.append("company_name",'');
     formData.append("usertype",'expert');
    this.makeFileRequest(this._url,formData, this.filesToUpload).then((result) => {
      console.log(result);
        if(result['Success']==true){ 
          
             this.alertClass = "alert alert-success text-center  alert-dismissible";
          if (this.addexpertsForm.value.id==0) {
            this.errorMsg = 'Expert Added SuccessFully';
          } else {
            this.errorMsg = 'Expert Edited SuccessFully';
          }
  window.setTimeout(() => {
            this.Router.navigate(['experts']);
          }, 3000);
        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = result['Message'];
        }
    
    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      // for (var i = 0; i < files.length; i++) {
      //   formData.append("file", files[i], files[i].name);
      // }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

}
