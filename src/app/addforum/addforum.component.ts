import { Component, OnInit,ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-addforum',
  templateUrl: './addforum.component.html',
  styleUrls: ['./addforum.component.css'],
   providers: [datacalls],
})
export class AddforumComponent implements OnInit {

    public forumForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    forumList:any;
    imagepath:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";

    a:any;
    filesToPost: any;
    file_keys: string[];
    id;
    alertClass: string;
    errorMsg: string;
    posts:Post[];
    resized_images_list: { filename: string, url: string, dimension: string }[];
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;
    selectedlist :any;
    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router,private _elementRef:ElementRef) { 
    this.forumForm = this._fb.group({
            forum_id:[0],
            app_id:[13],
            title:['',Validators.required],
            description:['',Validators.required],
            attachment:[''],
            expert:[],
            //image_path:['']
        });
    }
    ngOnInit() {
      window.scrollTo(0,0);

      this._datacalls.getExperts().subscribe(posts => {
    this.posts=posts.Data;
    //this.dtTrigger.next();
      console.log('a');
        
   // console.log(this.posts);
  });
          
 this.activatedRoute.params.subscribe(
      (param: any) => {

        this.id = param['forum_id'];
        console.log(this)
        if (this.id != undefined) {

          this._datacalls.getForum().subscribe(
            
            data => {
                            for(var i=0;i<data.Data.length;i++){

                        //      console.log( this.id +""+data.Data[i].id)

                              if(this.id ==data.Data[i].forum_id){

                               this.imagepath=data.Data[i].attachment;
              console.log(data.Data)
                 
                this.forumForm.patchValue({
                  'forum_id': data.Data[i].forum_id,
                  'app_id': data.Data[i].app_id,
                  'title': data.Data[i].title,
                  'description': data.Data[i].description,                 
                  'attachment': data.Data[i].attachment,
                   'expert': '',
                });

                 this.selectedlist = [];
               for( var j =0 ; j < data.Data[i].expert.length; j++)
                {
                   this.selectedlist.push( JSON.stringify(data.Data[i].expert[j].expert_id)) ;
                }
                  // if(this.selectedlist.length > 0)
                  //   {
                  //   this.selectedlist =   this.selectedlist.substr(0, this.selectedlist.length -1);
                  //   }
                 
                console.log('expert ' +this.selectedlist );
                this.filesToUpload=new Array<File>();
                              }
              }
            }
          );
        }
      });
        

    }
    
    
    file: File;
    filesToUpload: Array<File>;
 
_url:string ="http://myappcenter.co.in/serverfileuploadapi/api/insertforum";
  
  onChange(fileInput: any) {
    console.log("this is image: "+ this.forumForm.value.image_path);
    this.filesToUpload = <Array<File>>fileInput.target.files;
    this.isFileLoaded = true;
    let size = fileInput.target.files[0].size;
    console.log("Size: "+ size);
    if(size>=500000) {
      this.isValidImage = false;
      console.log("isValidImage: "+ this.isValidImage);
    }else {
    this.isValidImage = true;
    console.log("isValidImage: "+ this.isValidImage);
  }
}

  onSubmit() {

     var expertlist = [];
       // this.a = [{name :"priya"}];
       
       for(var i = 0; i < this.forumForm.value.expert.length ; i++)
       {
         
       var dm = {

         'active_status': 1,
         
         'expert_id': this.forumForm.value.expert[i],
       }
          expertlist.push(dm);
       }
       var jsondm = JSON.stringify(expertlist);

       console.log(jsondm);

    var formData: any = new FormData();
     formData.append("forum_id",this.forumForm.value.forum_id);
     formData.append("app_id",13);
     formData.append("title",this.forumForm.value.title);
     formData.append("description",this.forumForm.value.description); 
     formData.append("attachment",this.forumForm.value.attachment);
     formData.append("active_status",1);
     formData.append("expert",jsondm);
     formData.append("category_id",24);
  
    this.makeFileRequest(this._url,formData, this.filesToUpload).then((result) => {
      console.log(result);
        if(result['Success']==true){
             this.alertClass = "alert alert-success text-center  alert-dismissible";
          if (this.forumForm.value.forum_id==0) {
            this.errorMsg = 'Forum Added SuccessFully';
          } else {
            this.errorMsg = 'Forum Edited SuccessFully';
          }
  window.setTimeout(() => {
            this.Router.navigate(['forum']);
          }, 3000);
        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = result['Message'];
        }
    
    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

}

interface Post{
Message:string;
Status:number;
Success:string;
}