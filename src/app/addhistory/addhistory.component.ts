import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';

@Component({
  selector: 'app-addhistory',
  templateUrl: './addhistory.component.html',
  styleUrls: ['./addhistory.component.css'],
  providers:[datacalls]
})
export class AddhistoryComponent implements OnInit {

  public historyForm: FormGroup;
  public submitted: boolean;
  public events: any[] = [];
  policiesList: any;
  imagepath: string = "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";

  a: any;
  filesToPost: any;
  file_keys: string[];
  id;

  alertClass: string;
  errorMsg: string;
  resized_images_list: { filename: string, url: string, dimension: string }[];
  isFileLoaded: boolean = false;
  isValidImage: boolean = true;


  constructor(private _fb: FormBuilder, private activatedRoute: ActivatedRoute, private _http: Http, private _datacalls: datacalls, private Router: Router) {
    this.historyForm = this._fb.group({
      history_id: [0],
      app_id: [17],
      title: [''],
      image_path: [''],
      description: ['', Validators.required],
      link: ['']


    });
  }
  ngOnInit() {
    window.scrollTo(0,0);
    this.activatedRoute.params.subscribe(
      (param: any) => {

        this.id = param['id'];
        console.log(this)
        if (this.id != undefined) {
          this._datacalls.getHistory().subscribe(

            data => {
             // console.log('edit data:', data.Data);
              for (var i = 0; i < data.Data.length; i++) {
                if (this.id == data.Data[i].history_id) {
                  this.imagepath = data.Data[i].image_path;
                  console.log('dataaas:',data.Data[i].image_path)
                  //console.log('edit data descrip:', data.Data[i].subject);
                  this.historyForm.patchValue({
                    'history_id': data.Data[i].history_id,
                    'app_id':17,
                    'title': data.Data[i].title,
                     'image_path':data.Data[i].image_path,
                     'description': data.Data[i].description,
                     'link':data.Data[i].link


                  });

                  this.filesToUpload = new Array<File>();
                 
                }
              }
            }
          );
        }
      });


  }

  file: File;
  filesToUpload: Array<File>;

  _url: string = " http://myappcenter.co.in/serverfileuploadapi/api/insertsmartgaonhistory";
  onChange(fileInput: any) {
    this.isFileLoaded = true;
    this.filesToUpload = <Array<File>>fileInput.target.files;
    let size = fileInput.target.files[0].size;
    console.log("fileupload: " , this.filesToUpload);
    console.log("Size: " + size);
    if (size >= 500000) {
      this.isValidImage = false;
      console.log("isValidImage: " + this.isValidImage);
    } else {
      this.isValidImage = true;
      console.log("isValidImage: " + this.isValidImage);
    }

  }

  onSubmit() {
    var formData: any = new FormData();
    
    formData.append("history_id", this.historyForm.value.history_id);
    formData.append("title", this.historyForm.value.title);
    formData.append("link", this.historyForm.value.link);
    formData.append("description",this.historyForm.value.description);
    formData.append("file", this.historyForm.value.image_path);
    formData.append("app_id",17);
    formData.append("gaon_id",1);
    console.log('formdata'+formData);
    this.makeFileRequest(this._url, formData, this.filesToUpload).then((result) => {
      console.log('result'+result);
      if (result['Success'] == true) {
        this.alertClass = "alert alert-success text-center  alert-dismissible";
        if (this.historyForm.value.history_id==0) {
            this.errorMsg = 'History Added SuccessFully';
          } else {
            this.errorMsg = 'History Edited SuccessFully';
          }
        window.setTimeout(() => {
          this.Router.navigate(['history']);
        },1000);
      }
      else {
        this.alertClass = "alert alert-danger text-center  alert-dismissible";
        this.errorMsg = result['Message'];
      }

    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }

      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

}