import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';
import { Router } from '@angular/router';
import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';

@Component({
  selector: 'app-addimportantinfo',
  templateUrl: './addimportantinfo.component.html',
  styleUrls: ['./addimportantinfo.component.css'],
  providers:[datacalls]
})
export class AddimportantinfoComponent implements OnInit {

    importantinfoForm: FormGroup;
    submitted: boolean;
    events: any[] = [];
    imagepath:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";


    a:any;
    filesToPost: any;
    file_keys: string[];
    news_info_id;
    alertClass: string;
    errorMsg: string;
    resized_images_list: { filename: string, url: string, dimension: string }[];
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
    this.importantinfoForm = this._fb.group({
           news_info_id:[0],
            app_id:[13],
            cat_name:['',Validators.required],
            title:['',Validators.required],
            description:['',Validators.required],
            file:[''],
            image_path:['']
        });
    }

  ngOnInit() {
    window.scrollTo(0,0);

   this.activatedRoute.params.subscribe(
      (param: any) => {

        this.news_info_id = this.activatedRoute.snapshot.params['news_info_id'];
        console.log(this)
        if (this.news_info_id != undefined) {

          this._datacalls.getimportantinfo().subscribe(
            
            data => {
                                          
               this.imagepath=data.Data[0].image_path;
              console.log(data.Data[0].image_path)
              // if (data.Data.length > 0) {
                 
                this.importantinfoForm = this._fb.group({
                  
                  'news_info_id':data.Data[0].news_info_id,
                  'app_id':data.Data[0].app_id,
                  'cat_name': data.Data[0].cat_name,
                  'title': data.Data[0].title, 
                  'description': data.Data[0].description,
                  'file': data.Data[0].image_path,
                  'image_path': data.Data[0].image_path
                });
                this.filesToUpload=new Array<File>();
                              }
                           
                       
          );
        }
      });
        

    }
    

    file: File;
    filesToUpload: Array<File>;
 
_url:string ="http://35.154.141.107:6002/api/insertnewsinfo";
  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
    this.isFileLoaded = true;
    let size = fileInput.target.files[0].size;
    console.log("Size: "+ size);
    if(size>=500000) {
      this.isValidImage = false;
      console.log("isValidImage: "+ this.isValidImage);
    }else {
    this.isValidImage = true;
    console.log("isValidImage: "+ this.isValidImage);
  }
  }

onSubmit() {
     
      //  this.a = [{name :"priya"}];
     var formData: any = new FormData();
     console.log(formData)
     
     formData.append("news_info_id",this.importantinfoForm.value.news_info_id);
     formData.append("app_id",13);
     formData.append("cat_name",this.importantinfoForm.value.cat_name);
     formData.append("title",this.importantinfoForm.value.title); 
     formData.append("user_std",'5th');
     formData.append("type","");
      formData.append("sub_category_id",60);
    // formData.append("name",this.importantinfoForm.value.book_name); 
      formData.append("description",this.importantinfoForm.value.description);
    // formData.append("rank",this.importantinfoForm.value.price);
     formData.append("file",this.importantinfoForm.value.image_path);
     formData.append("image1",this.importantinfoForm.value.image_path);
     formData.append("category_id",53);
    // formData.append("sub_category_id",60);
     this.makeFileRequest(this._url,formData, this.filesToUpload).then((result) => {
      console.log(result);
        if(result['Success']==true){
             this.alertClass = "alert alert-success text-center  alert-dismissible";
          this.errorMsg = 'Record Added SuccessFully';
  window.setTimeout(() => {
            this.Router.navigate(['importantinfo']);
          }, 3000);
     
        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = result['Message'];
        }
    
    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }
}