import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router';
import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';

@Component({
  selector: 'app-addjobposting',
  templateUrl: './addjobposting.component.html',
  styleUrls: ['./addjobposting.component.css'],
  providers:[datacalls]
})
export class AddjobpostingComponent implements OnInit {

    public jobpostingForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    policiesList:any;
    imagepath:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";

    a:any;
    filesToPost: any;
    file_keys: string[];
    id;
    alertClass: string;
    errorMsg: string;
    resized_images_list: { filename: string, url: string, dimension: string }[];
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
    this.jobpostingForm = this._fb.group({
            id:[0],
            app_id:[13],
            tiltle:[''],
            company_name:['',Validators.required],
            description:['',Validators.required],
            about:['',Validators.required],
            designation:['',Validators.required],
            job_description:['',Validators.required],
            experience:['',Validators.required],
            skill:['',Validators.required],
            job_location:['',Validators.required],
            no_of_vacancies:['',Validators.required],
            type:['application/pdf'],
            job_type:['',Validators.required],
            compensation:['',Validators.required],
            remark:['',Validators.required]

            // file:[''],
            // image_path:['']
        });
    }
    ngOnInit() {
      window.scrollTo(0,0);

//       this._datacalls.getAllCategory().subscribe(
//             data => {
//               if (data.Data.length > 0) {
//                 this.policiesList = data.Data[0].data;
//                 console.log(this.policiesList);
//               }
//             }
//           );
            
 this.activatedRoute.params.subscribe(
      (param: any) => {

        this.id= param['id'];
        console.log(this)
        if (this.id!= undefined) {

          this._datacalls.getJobPosting().subscribe(

            
            data => {
                            for(var i=0;i<data.Data.length;i++){

                              console.log( this.id+" "+data.Data[i].id)

                              if(this.id==data.Data[i].id){

              //                  this.imagepath=data.Data[i].path;
              // console.log(data.Data[i].image_path)
                 
                this.jobpostingForm.patchValue({
                  'id': data.Data[i].id,
                  'app_id':13,
                  'title': data.Data[i].designation,
                  'company_name': data.Data[i].company_name,
                  'description': data.Data[i].description,
                  'about': data.Data[i].about,
                  'designation': data.Data[i].designation,
                  'job_description': data.Data[i].job_description,
                  'experience': data.Data[i].experience,
                  'skill': data.Data[i].skill,
                  'job_location': data.Data[i].job_location,
                  'no_of_vacancies': data.Data[i].no_of_vacancies,
                  'type':'application/pdf',
                  'job_type': data.Data[i].job_type,
                  'compensation': data.Data[i].compensation,
                  'remark': data.Data[i].remark,
                  
                  // 'image1': data.Data[i].image_path
                });
                this.filesToUpload=new Array<File>();
                              }
              }
            }
          );
        }
      });
        

    }
    
    file: File;
    filesToUpload: Array<File>;
 
_url:string ="http://myappcenter.co.in/serverfileuploadapi/api/insertplacement";
  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
    this.isFileLoaded = true;
    let size = fileInput.target.files[0].size;
    console.log("Size: "+ size);
    if(size>=500000) {
      this.isValidImage = false;
      console.log("isValidImage: "+ this.isValidImage);
    }else {
    this.isValidImage = true;
    console.log("isValidImage: "+ this.isValidImage);
  }
  }
  onSubmit() {
     debugger
       // this.a = [{name :"priya"}];
    var formData: any = new FormData();
     formData.append("placement_id",this.jobpostingForm.value.id);
   formData.append("user_std",'1st');
    formData.append("category_id",'8');
    formData.append("sub_category_id",'16');
    
    formData.append("title",this.jobpostingForm.value.designation);
     formData.append("app_id",13);
     formData.append("company_name",this.jobpostingForm.value.company_name);      
     formData.append("description",this.jobpostingForm.value.description); 
     formData.append("about",this.jobpostingForm.value.about);
     formData.append("designation",this.jobpostingForm.value.designation);
     formData.append("job_description",this.jobpostingForm.value.job_description);
     formData.append("experience",this.jobpostingForm.value.experience);
     formData.append("skill",this.jobpostingForm.value.skill);
     formData.append("job_location",this.jobpostingForm.value.job_location);
     formData.append("no_of_vacancies",this.jobpostingForm.value.no_of_vacancies);
     formData.append("type",'application/pdf');
     formData.append("job_type",this.jobpostingForm.value.job_type);
     formData.append("compensation",this.jobpostingForm.value.compensation);
     formData.append("remark",this.jobpostingForm.value.remark);
    //  formData.append("image_path",this.jobpostingForm.value.image_path); 
    //  formData.append("sub_category_id",36);
    this.makeFileRequest(this._url,formData, this.filesToUpload).then((result) => {
      console.log(result);
        if(result['Success']==true){
             this.alertClass = "alert alert-success text-center  alert-dismissible";
         if (this.jobpostingForm.value.id==0) {
            this.errorMsg = 'Job Posting Added SuccessFully';
          } else {
            this.errorMsg = 'Job Posting Edited SuccessFully';
          }
  window.setTimeout(() => {
            this.Router.navigate(['jobposting']);
          }, 300);
        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = result['Message'];
        }
    
    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      // for (var i = 0; i < files.length; i++) {
      //   formData.append("file", files[i], files[i].name);
      // }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

}
