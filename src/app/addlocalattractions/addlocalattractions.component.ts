import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';

@Component({
  selector: 'app-addlocalattractions',
  templateUrl: './addlocalattractions.component.html',
  styleUrls: ['./addlocalattractions.component.css'],
  providers:[datacalls]
})
export class AddlocalattractionsComponent implements OnInit {

  public localattractionsForm: FormGroup;
  public submitted: boolean;
  public events: any[] = [];
  policiesList: any;
  imagepath: string = "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";

  a: any;
  filesToPost: any;
  file_keys: string[];
  id;

  alertClass: string;
  errorMsg: string;
  resized_images_list: { filename: string, url: string, dimension: string }[];
  isFileLoaded: boolean = false;
  isValidImage: boolean = true;


  constructor(private _fb: FormBuilder, private activatedRoute: ActivatedRoute, private _http: Http, private _datacalls: datacalls, private Router: Router) {
    this.localattractionsForm = this._fb.group({
      attraction_id: [0],
      app_id: [17],
      title:['', Validators.required],
      image_path: [''],
      description: ['', Validators.required],
      nearby_places: [''],
      reference: ['']


    });
  }
  ngOnInit() {
    window.scrollTo(0,0);
    this.activatedRoute.params.subscribe(
      (param: any) => {

        this.id = param['id'];
        console.log(this)
        if (this.id != undefined) {
          this._datacalls.getLocalAttractions().subscribe(

            data => {
             // console.log('edit data:', data.Data);
              for (var i = 0; i < data.Data.length; i++) {
                if (this.id == data.Data[i].attraction_id) {
                  this.imagepath = data.Data[i].image_path;
                  console.log('dataaas:',data.Data[i].image_path)
                  //console.log('edit data descrip:', data.Data[i].subject);
                  this.localattractionsForm.patchValue({
                    'attraction_id': data.Data[i].attraction_id,
                    'app_id':17,
                    'title': data.Data[i].title,
                     'image_path':data.Data[i].image_path,
                     'description': data.Data[i].description,
                     //'link':data.Data[i].link,
                     'nearby_places': data.Data[i].nearby_places,
                     'reference':data.Data[i].reference


                  });

                  this.filesToUpload = new Array<File>();
                 
                }
              }
            }
          );
        }
      });


  }

  file: File;
  filesToUpload: Array<File>;

  _url: string = " http://myappcenter.co.in/serverfileuploadapi/api/insertlocalattractions";
  onChange(fileInput: any) {
    this.isFileLoaded = true;
    this.filesToUpload = <Array<File>>fileInput.target.files;
    let size = fileInput.target.files[0].size;
    console.log("fileupload: " , this.filesToUpload);
    console.log("Size: " + size);
    if (size >= 500000) {
      this.isValidImage = false;
      console.log("isValidImage: " + this.isValidImage);
    } else {
      this.isValidImage = true;
      console.log("isValidImage: " + this.isValidImage);
    }

  }

  onSubmit() {
    var formData: any = new FormData();
    
    formData.append("attraction_id", this.localattractionsForm.value.attraction_id);
    formData.append("title", this.localattractionsForm.value.title);
    formData.append("nearby_places", this.localattractionsForm.value.nearby_places);
    formData.append("description",this.localattractionsForm.value.description);
    formData.append("reference",this.localattractionsForm.value.reference);
    formData.append("file", this.localattractionsForm.value.image_path);
    formData.append("app_id",17);
    formData.append("gaon_id",1);
    console.log('formdata'+formData);
    this.makeFileRequest(this._url, formData, this.filesToUpload).then((result) => {
      console.log('result'+result);
      if (result['Success'] == true) {
        this.alertClass = "alert alert-success text-center  alert-dismissible";
        if (this.localattractionsForm.value.attraction_id==0) {
            this.errorMsg = 'Local Attraction Added SuccessFully';
          } else {
            this.errorMsg = 'Local Attraction Edited SuccessFully';
          }
        window.setTimeout(() => {
          this.Router.navigate(['localattractions']);
        },1000);
      }
      else {
        this.alertClass = "alert alert-danger text-center  alert-dismissible";
        this.errorMsg = result['Message'];
      }

    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }

      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

}