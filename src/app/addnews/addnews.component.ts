import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-addnews',
  templateUrl: './addnews.component.html',
  styleUrls: ['./addnews.component.css'],
  providers:[datacalls]
})
export class AddnewsComponent implements OnInit {

 public newsForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
   a:any;
    filesToPost: any;
       alertClass: string;
   errorMsg: string;
    file_keys: string[];
    id; 
    news_id;
    filesToUpload;
    imagepath1:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;
    
    resized_images_list: { filename: string, url: string, dimension: string }[];

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls, private changeDetectorRef: ChangeDetectorRef,private fb:FormBuilder,private Router:Router ) {

        this.newsForm = this.fb.group({
            news_id:[0],
            app_id:[17],
            title: ['', [<any>Validators.required]],
             description: ['', [<any>Validators.required]],
              
            file:[''],
            image_path:[''],
            //type:['', [<any>Validators.required]],

        });
     }


    ngOnInit() {

  console.log('hello')
  
      // this.activatedRoute.params.subscribe(
      //   (param: any) => {
  
      //     this.news_id = param['news_id'];

      this.news_id=this.activatedRoute.snapshot.params['news_id']
          console.log(this.news_id)
          if (this.news_id != undefined) {
  
            this._datacalls.getNews(this.news_id).subscribe(
              
              data => {
                              
                              console.log( 'data',data.Data[0]);
  
                                
                   
                  this.newsForm.patchValue({
                    'news_id': data.Data[0].news_id,
                    'app_id': 17,
                    'title': data.Data[0].title,
                    'description': data.Data[0].description,
                    //'location': data.Data[0].location,
                    'image_path': data.Data[0].image_path,
                   // 'type': data.Data[0].type,
                    });
                  this.filesToUpload=new Array<File>();
                  this.imagepath1= this.newsForm.value.image_path;
                  console.log('path',this.imagepath1); 
                  
                                }
                
              
            );
          }
        // });
          
  
    }
    onChange(fileInput: any) {
      this.filesToUpload = fileInput.target.files['0'];
    //  this.isFileLoaded = true;
   
   }



  onSubmit() {
     
    var formData = new FormData();
   
    formData.append("news_id", this.newsForm.value.news_id);
    formData.append("app_id", this.newsForm.value.app_id);
    formData.append("title", this.newsForm.value.title);
    formData.append("description", this.newsForm.value.description);
    //formData.append("location", this.newsForm.value.location);
    formData.append("file", this.filesToUpload);
    formData.append("type",'image/jpeg');
    //formData.append("active_status", "1");
    formData.append("gaon_id",1);
    
    
 console.log('form',formData)
 
  this._datacalls.addNews(formData).subscribe(posts => {
       
      if(posts['Success']==true){
              this.alertClass = "alert alert-success text-center alert-dismissible";
               if (this.newsForm.value.news_id==0) {
            this.errorMsg = 'News Added SuccessFully';
          } else {
            this.errorMsg = 'News Edited SuccessFully';
          }
               window.setTimeout(() => {
             this.Router.navigate(['news']);
           }, 2000);
           
           } 
           else{
             this.alertClass = "alert alert-danger text-center alert-dismissible";
             this.errorMsg = 'Form not Filled properly';
 
           }
     
       console.log('Run succesfull');
         
   });
 
  }
  }
