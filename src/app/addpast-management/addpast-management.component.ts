import { Component, OnInit,ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-addpast-management',
  templateUrl: './addpast-management.component.html',
  styleUrls: ['./addpast-management.component.css'],
  providers:[datacalls]
})
export class AddpastManagementComponent implements OnInit {


public pastmanagementForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    imagepath:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";


    a:any;
    filesToPost: any;
    file_keys: string[];
    id;
    alertClass: string;
    errorMsg: string;0
    resized_images_list: { filename: string, url: string, dimension: string }[];
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
    this.pastmanagementForm = this._fb.group({
            id:[0],
            app_id:[13],
            cat_name:['',Validators.required],
            description:['',Validators.required],
            job_location:['',Validators.required],
            file:['',Validators.required],
            image_path:[''],
            
        });
    }
    ngOnInit() {
  
    //  this._pastmanagement.getPastmanagement().subscribe(posts => {
    // this.posts=posts.Data;
    //   console.log(posts.Data);
    //      window.setTimeout(() => {
    //       var s = document.createElement("script");
    //       s.text = "TableDatatablesManaged.init();";
    //       this._elementRef.nativeElement.appendChild(s);
           
    //     }, 100);

    //  });
            
 this.activatedRoute.params.subscribe(
      (param: any) => {

        this.id = param['id'];
        console.log(this)
        if (this.id != undefined) {
//remove this.id
          this._datacalls.getPastmanagement().subscribe(
            
            data => {
              for(var i=0;i<data.Data.length;i++){

                              console.log(this.id +""+data.Data[i].id)

                              if(this.id ==data.Data[i].id){

              this.imagepath=data.Data[i].image_path;
              console.log(data.Data[i].image_path)
              if (data.Data.length > 0) {
                 
                this.pastmanagementForm = this._fb.group({
                  'id': data.Data[i].id,
                  'app_id': data.Data[i].app_id,
                  'cat_name': data.Data[i].cat_name,
                  'description': data.Data[i].description,
                  'job_location': data.Data[i].job_location,
                  'file': data.Data[i].path,
                  'image_path': data.Data[i].image_path,
                  
                //0   'file': data.Data[i].image_path,
                  
                });
                this.filesToUpload=new Array<File>();
              }
                              }
              }
                              });
              
        }
      });
        

        }
      
    
    file: File;
    filesToUpload: Array<File>;
 
_url:string ="http://35.154.141.107:6002/api/insertplacement";
  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
    this.isFileLoaded = true;
    let size = fileInput.target.files[0].size;
    console.log("Size: "+ size);
    if(size>=500000) {
      this.isValidImage = false;
      console.log("isValidImage: "+ this.isValidImage);
    }else {
    this.isValidImage = true;
    console.log("isValidImage: "+ this.isValidImage);
  }
  }
  onSubmit() {
     
       // this.a = [{name :"priya"}];
    var formData: any = new FormData();
    console.log(formData)
     formData.append("placement_id",this.pastmanagementForm.value.id);
     formData.append("app_id",13);
     formData.append("category_id",8);
     formData.append("sub_category_id",16);
     formData.append("experience",0);
     formData.append("job_type",'');
     formData.append("skill",'');
     formData.append("path",'');
     formData.append("title",this.pastmanagementForm.value.cat_name); 
     formData.append("description",this.pastmanagementForm.value.description);
     formData.append("job_location",this.pastmanagementForm.value.job_location);
     formData.append("file",this.pastmanagementForm.value.image_path);
     formData.append("image_path",this.pastmanagementForm.value.image_path);
     formData.append("user_std",'5th');
     formData.append("type","");
    // formData.append("category_id",0);
     
    this.makeFileRequest(this._url,formData, this.filesToUpload).then((result) => {
      console.log(result);
        if(result['Success']==true){
             this.alertClass = "alert alert-success text-center  alert-dismissible";
          this.errorMsg = 'Pastmanagement Added SuccessFully';
  window.setTimeout(() => {
            this.Router.navigate(['past-management']);
          }, 3000);
     
        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = result['Message'];
        } 
    
    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

      }
