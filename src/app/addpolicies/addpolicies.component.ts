import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router';
import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';

@Component({
  selector: 'app-addpolicies',
  templateUrl: './addpolicies.component.html',
  styleUrls: ['./addpolicies.component.css'],
  providers:[datacalls]
})
export class AddpoliciesComponent implements OnInit {

    public policiesForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    policiesList:any;
    imagepath:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";

    a:any;
    filesToPost: any;
    file_keys: string[];
    institute_desk_id;
    alertClass: string;
    errorMsg: string;
    resized_images_list: { filename: string, url: string, dimension: string }[];
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
    this.policiesForm = this._fb.group({
            institute_desk_id:[0],
            app_id:[13],
            title:['',Validators.required],
            description:['',Validators.required],
            file:[''],
            image_path:['']
        });
    }
    ngOnInit() {

//       this._datacalls.getAllCategory().subscribe(
//             data => {
//               if (data.Data.length > 0) {
//                 this.policiesList = data.Data[0].data;
//                 console.log(this.policiesList);
//               }
//             }
//           );
            
 this.activatedRoute.params.subscribe(
      (param: any) => {

        this.institute_desk_id = param['institute_desk_id'];
        console.log(this)
        if (this.institute_desk_id != undefined) {

          this._datacalls.getPolicies().subscribe(
            
            data => {
                            for(var i=0;i<data.Data.length;i++){

                              console.log( this.institute_desk_id +""+data.Data[i].institute_desk_id)

                              if(this.institute_desk_id ==data.Data[i].institute_desk_id){

                               this.imagepath=data.Data[i].path;
              console.log(data.Data[i].image_path)
                 
                this.policiesForm.patchValue({
                  'institute_desk_id': data.Data[i].institute_desk_id,
                  'app_id': data.Data[i].app_id,
                  'title': data.Data[i].title,
                  'description': data.Data[i].description,
                  'file': data.Data[i].image_path,
                  'image1': data.Data[i].image_path
                });
                this.filesToUpload=new Array<File>();
                              }
              }
            }
          );
        }
      });
        

    }
    
    file: File;
    filesToUpload: Array<File>;
 
_url:string ="http://myappcenter.co.in/serverfileuploadapi/api/insertinstitutedesk";
  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
    this.isFileLoaded = true;
    let size = fileInput.target.files[0].size;
    console.log("Size: "+ size);
    if(size>=500000) {
      this.isValidImage = false;
      console.log("isValidImage: "+ this.isValidImage);
    }else {
    this.isValidImage = true;
    console.log("isValidImage: "+ this.isValidImage);
  }
  }
  onSubmit() {
     
       // this.a = [{name :"priya"}];
    var formData: any = new FormData();
     formData.append("institute_desk_id",this.policiesForm.value.institute_desk_id);
     formData.append("app_id",this.policiesForm.value.app_id);
     formData.append("title",this.policiesForm.value.title);      
     formData.append("description",this.policiesForm.value.description); 
     formData.append("image_path",this.policiesForm.value.image_path); 
     formData.append("sub_category_id",36);
    this.makeFileRequest(this._url,formData, this.filesToUpload).then((result) => {
      console.log(result);
        if(result['Success']==true){
             this.alertClass = "alert alert-success text-center  alert-dismissible";
          if (this.policiesForm.value.institute_desk_id==0) {
            this.errorMsg = 'Policy Added SuccessFully';
          } else {
            this.errorMsg = 'Policy Edited SuccessFully';
          }
  window.setTimeout(() => {
            this.Router.navigate(['policies']);
          }, 3000);
        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = result['Message'];
        }
    
    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      // for (var i = 0; i < files.length; i++) {
      //   formData.append("file", files[i], files[i].name);
      // }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

}
