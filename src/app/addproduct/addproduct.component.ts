import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router'
@Component({
    selector: 'app-addproduct',
    templateUrl: './addproduct.component.html',
    styleUrls: ['./addproduct.component.css'],
    providers: [datacalls]
})
export class AddproductComponent implements OnInit {
    public productForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    categorylist:any;
   alertClass: string;
   errorMsg: string;
    imagepath1:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
    imagepath2:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
    imagepath3:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
    imagepath4:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
    imagepath5:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
    public uploader: FileUploader = new FileUploader({ url: '35.154.141.107:4000/api/insertproduct' });
     a:any;
    filesToPost: any;
    extra_data;
    file_keys: string[];
    id; 
    filesToUpload;
    filesToUpload1;
    filesToUpload2;
    filesToUpload3;
    filesToUpload4;
    filesToUpload5;

    resized_images_list: { filename: string, url: string, dimension: string }[];
    isFileLoaded:boolean = false;

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls, private changeDetectorRef: ChangeDetectorRef,private fb:FormBuilder,private Router:Router ) {
 let contactRegex='^([0-9\(\)\/\+ \-]*)$';
        this.productForm = this._fb.group({
            product_id:[0],
            app_id:[17],
            name: ['', [Validators.required, Validators.minLength(2)]],
            code: [''],
            category:['',Validators.required],
            description:['',Validators.required],
            size:['',Validators.required],
            price:['',Validators.required],
            stock:['',Validators.required],
            discount_percentage:['',Validators.required],
            contact_now:['', [<any>Validators.required,Validators.maxLength(10),Validators.minLength(10),<any>Validators.pattern(contactRegex)]],
            specific_term:['',Validators.required],
            area:['',Validators.required],
            //file: ['', Validators.required]
            image1:[''],
            image2:[''],
            image3:[''],
            image4:[''],
            company_brief:['',Validators.required],
            web_link:['',Validators.required],
            extra:['',Validators.required],


        });
     }


    ngOnInit() {
    this._datacalls.getAllCategory().subscribe(
      data => {
        if (data.Data.length > 0) {
          this.categorylist = data.Data[0].data;
          console.log("categories:" , this.categorylist);
        }
      }
    );
  
      this.activatedRoute.params.subscribe(
      (param: any) => {

        this.id = param['id'];

        console.log(this)
        if (this.id != undefined) {

          this._datacalls.getProductdetails(this.id).subscribe(
            
            data => {
           
              console.log('data aana:',data.Data[0]);
              this.imagepath1=data.Data[0].image1;
              this.imagepath2=data.Data[0].extra_image.image2;
              this.imagepath3=data.Data[0].extra_image.image3;
              this.imagepath4=data.Data[0].extra_image.image4;
              this.imagepath5=data.Data[0].extra_image.image5;
              
             
              console.log("Image while editing: ", data.Data[0].image1)
              if (data.Data.length > 0) {
                 
                this.productForm.patchValue({
                  'product_id': data.Data[0].product_id,
                  'app_id': data.Data[0].app_id,
                  'name': data.Data[0].product_name,
                  'code': data.Data[0].product_code,
                  'category': data.Data[0].category_id,
                  'description': data.Data[0].description,
                  'size': data.Data[0].size,
                  'price': data.Data[0].price,
                  'image1': data.Data[0].image1,
                  'stock': data.Data[0].stock,
                  'discount_percentage': data.Data[0].discount_percentage,
                  'contact_now': data.Data[0].extra_data.contact_now,
                  'specific_term': data.Data[0].extra_data.specific_term,
                  'area': data.Data[0].extra_data.area,
                  'web_link': data.Data[0].extra_data.weblink,
                  'extra': data.Data[0].extra_data.extra,
                  'company_brief': data.Data[0].extra_data.company_brief,
                

                });
    
              }
            },
            error => { },
            () => {

            }
          );
        }
      });
        


    }

 
    onChange(fileInput: any) {
      this.filesToUpload = fileInput.target.files['0'];
    //  this.isFileLoaded = true;
   
   }
   onChange1(fileInput1: any) {
    this.filesToUpload1= fileInput1.target.files['0'];
  //  this.isFileLoaded = true;
 
 }
 onChange2(fileInput2: any) {
  this.filesToUpload2 = fileInput2.target.files['0'];
//  this.isFileLoaded = true;

}
onChange3(fileInput3: any) {
  this.filesToUpload3 = fileInput3.target.files['0'];
//  this.isFileLoaded = true;

}
onChange4(fileInput4: any) {
  this.filesToUpload4 = fileInput4.target.files['0'];
//  this.isFileLoaded = true;

}



  onSubmit() {
     
       
    var formData: any = new FormData();
    if(this.productForm.value.product_id==""){
      this.productForm.value.product_id=0;
    }

    this.extra_data={
    "contact_now":this.productForm.value.contact_now,
    "specific_term":this.productForm.value.specific_term,
    "area":this.productForm.value.area,
    "weblink":this.productForm.value.web_link,
    "company_brief":this.productForm.value.company_brief,
    "extra":this.productForm.value.extra
       

    }

  console.log('extra data:',this.extra_data)
debugger
    formData.append("product_id",this.productForm.value.product_id); 
    formData.append("app_id",17); 
    formData.append("product_name",this.productForm.value.name); 
    formData.append("product_code",this.productForm.value.code); 
    formData.append("category_id",this.productForm.value.category);
    formData.append("description",this.productForm.value.description); 
    formData.append("size",this.productForm.value.size); 
    formData.append("price",this.productForm.value.price);
    formData.append("image1",this.productForm.value.image1);
    formData.append("stock",this.productForm.value.stock); 
    formData.append("discount_percentage",this.productForm.value.discount_percentage);
    formData.append("extra_data",JSON.stringify(this.extra_data));
    formData.append("active_status",1);
    formData.append("gaon_id",1);
    // formData.append("file1",this.filesToUpload);
    // formData.append("file2",this.filesToUpload1);
    // formData.append("file3",this.filesToUpload2);
    // formData.append("file4",this.filesToUpload3);
    // formData.append("file5",this.filesToUpload4);
    

    if(this.imagepath1 != undefined){
      formData.append("imagepath1",this.imagepath1);
      formData.append("file1",this.filesToUpload);
    }

    if (this.imagepath1 == undefined){
      formData.append("file1",this.filesToUpload);
    }

      if(this.imagepath2 != undefined){
      formData.append("imagepath2",this.imagepath2);
      formData.append("file2",this.filesToUpload1);
    }

    if(this.imagepath2 == undefined)
      {
        formData.append("file2",this.filesToUpload1);
      }
       if(this.imagepath3 != undefined){
      formData.append("imagepath3",this.imagepath3);
      formData.append("file3",this.filesToUpload2);
    }

    if(this.imagepath3 == undefined){
      formData.append("file3",this.filesToUpload2);
    }
       if(this.imagepath4 != undefined){
      formData.append("imagepath4",this.imagepath4);
      formData.append("file4",this.filesToUpload3);
      
    }

    if(this.imagepath4 == undefined){
      formData.append("file4",this.filesToUpload3);
      
    }

    
     if(this.imagepath5 != undefined){
      formData.append("imagepath5",this.imagepath5);
      formData.append("file5",this.filesToUpload4);
    }

    if(this.imagepath5 == undefined){
      formData.append("file5",this.filesToUpload4);
    }

    else{
      console.log("no image found");
    }
  
    console.log("formdata: " , this.filesToUpload)

    this._datacalls.addProduct(formData).subscribe(posts => {
      if (posts['Success'] == true) {
        this.alertClass = "alert alert-success text-center  alert-dismissible";
        this.errorMsg = 'Product Added Successfully!';
        window.setTimeout(() => {
          this.Router.navigate(['productlist']);
        }, 3000);
      }
      else {
        this.alertClass = "alert alert-danger text-center  alert-dismissible";
        this.errorMsg = posts['Message'];
      }
    });

  
  }

 

}

export interface CategoryData {
    name: string,
    code:string,
    category:string,
    description:string,
    price:number,
    size:string
} 