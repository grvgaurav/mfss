import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-addquery',
  templateUrl: './addquery.component.html',
  styleUrls: ['./addquery.component.css'],
  providers:[datacalls]
})
export class AddqueryComponent implements OnInit {

  
    public queryForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
   a:any;
    filesToPost: any;
       alertClass: string;
   errorMsg: string;
    file_keys: string[];
    id; 
    query_id;
    filesToUpload;
    imagepath1:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;
    
    resized_images_list: { filename: string, url: string, dimension: string }[];

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls, private changeDetectorRef: ChangeDetectorRef,private fb:FormBuilder,private Router:Router ) {

        this.queryForm = this.fb.group({
            query_id:[0],
            app_id:[17],
            title: ['', [<any>Validators.required]],
             description: ['', [<any>Validators.required]],
              location: ['', [<any>Validators.required]],
            file:[''],
            attachment:[''],
            query_status:['', [<any>Validators.required]],

        });
     }


    ngOnInit() {

  console.log('hello')
  
      // this.activatedRoute.params.subscribe(
      //   (param: any) => {
  
      //     this.query_id = param['query_id'];

      this.query_id=this.activatedRoute.snapshot.params['query_id']
          console.log(this.query_id)
          if (this.query_id != undefined) {
  
            this._datacalls.getQuery(this.query_id).subscribe(
              
              data => {
                              
                              console.log( 'data',data.Data[0]);
  
                                
                   
                  this.queryForm.patchValue({
                    'query_id': data.Data[0].query_id,
                    'app_id': 17,
                    'title': data.Data[0].title,
                    'description': data.Data[0].description,
                    'location': data.Data[0].location,
                    'attachment': data.Data[0].attachment,
                    'query_status': data.Data[0].query_status,
                    });
                  this.filesToUpload=new Array<File>();
                  this.imagepath1= this.queryForm.value.attachment;
                  console.log('path',this.imagepath1); 
                  
                                }
                
              
            );
          }
        // });
          
  
    }
    onChange(fileInput: any) {
      this.filesToUpload = fileInput.target.files['0'];
    //  this.isFileLoaded = true;
   
   }



  onSubmit() {
     
    var formData = new FormData();
   
    formData.append("query_id", this.queryForm.value.query_id);
    formData.append("app_id", this.queryForm.value.app_id);
    formData.append("title", this.queryForm.value.title);
    formData.append("description", this.queryForm.value.description);
    formData.append("location", this.queryForm.value.location);
    formData.append("file", this.filesToUpload);
    formData.append("query_status", this.queryForm.value.query_status);
    //formData.append("type","image/jpeg");
    formData.append("active_status", "1");
    formData.append("gaon_id",1);
    formData.append("post_status",1);
    
    
 console.log('form',formData)
 
  this._datacalls.addQuery(formData).subscribe(posts => {
       
      if(posts['Success']==true){
              this.alertClass = "alert alert-success text-center alert-dismissible";
               if (this.queryForm.value.query_id==0) {
            this.errorMsg = 'Query Added SuccessFully';
          } else {
            this.errorMsg = 'Query Edited SuccessFully';
          }
               window.setTimeout(() => {
             this.Router.navigate(['query']);
           }, 2000);
           
           } 
           else{
             this.alertClass = "alert alert-danger text-center alert-dismissible";
             this.errorMsg = 'Form not Filled properly';
 
           }
     
       console.log('Run succesfull');
         
   });
 
  }
  }