import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router';
import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';

@Component({
  selector: 'app-addrepository',
  templateUrl: './addrepository.component.html',
  styleUrls: ['./addrepository.component.css'],
  providers:[datacalls]
})
export class AddrepositoryComponent implements OnInit {

  public repositoryForm: FormGroup;
    public submitted: boolean;
    a:any;
    id;
    
    //type;
    //types;
    alertClass: string;
    errorMsg: string;


    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
    this.repositoryForm = this._fb.group({
            id:[0],
            //brand_id:[23],
            app_id:[17],
           name:['',Validators.required],
           active_status:[''],
           user_std:['1st'],
           Subjects:['']
        });
    }
    ngOnInit() {
      window.scrollTo(0,0);
this.activatedRoute.params.subscribe(
      (param: any) => {
        this.id = param['id'];
       console.log(this.id);
        if (this.id != undefined) {
          this._datacalls.getRepository().subscribe(
            
            data => {
              for(var i=0; i<data.Data.Teachers.length; i++){

                              console.log(this.id +" "+data.Data.Teachers[i].id) 

                              if(this.id==data.Data.Teachers[i].id){
              if (data.Data.Teachers.length > 0) {                 
                this.repositoryForm.patchValue({
                  id:data.Data.Teachers[i].id,
                  app_id:17,
                  name:data.Data.Teachers[i].name,
                  active_status:data.Data.Teachers[i].active_status,
                  user_std:'1st',
                  Subjects:data.Data.Teachers[i].Subjects

                  
                 
                });
              }
            }
            }
          }
            );
        }
        
      });




}
    
onSubmit() {
  debugger;

/*let formData={
                'teacher_id': this.repositoryForm.value.teacher_id,
                'brand_id': this.repositoryForm.value.brand_id,
                'app_id': this.repositoryForm.value.app_id,
                'model_name': this.repositoryForm.value.model_name
                
}*/
 var formData: any = new FormData();

 console.log(formData);
 formData.append("id",this.repositoryForm.value.id);
     //formData.append("brand_id",this.repositoryForm.value.brand_id);
    formData.append("app_id",17);
     formData.append("name",this.repositoryForm.value.name );
     formData.append("active_status",1);
     formData.append("user_std",'1st' );
     formData.append("Subjects",''); 
     formData.append("parent_id",0);
console.log('name data:'+this.repositoryForm.value.name);

 this._datacalls.addRepository(formData).subscribe(
      data => {
        if (data) {
          console.log("Inserted value");

          // this.Router.navigate(['events']);
        } else {
          console.log("not");
        }
        
         if(data['Success']==true){
          this.alertClass = "alert alert-success text-center  alert-dismissible";
          if (this.repositoryForm.value.id==0) {
            this.errorMsg = 'Repository Added SuccessFully';
          } else {
            this.errorMsg = 'Repository Edited SuccessFully';
          }
          window.setTimeout(() => {
              this.Router.navigate(['repository']);
            //this.Router.navigate(['attributeType/'+ this.type + '/attributes']);
          }, 250);     
        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = data['Message'];
        }
      }
      
    );
    
}

}
