import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router';
import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';

@Component({
  selector: 'app-addrepositorypdf',
  templateUrl: './addrepositorypdf.component.html',
  styleUrls: ['./addrepositorypdf.component.css'],
  providers:[datacalls]
})
export class AddrepositorypdfComponent implements OnInit {
 public repositorypdfForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    policiesList:any;
    imagepath:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";

    a:any;
    filesToPost: any;
    file_keys: string[];
    id;
    sid;
    alertClass: string;
    errorMsg: string;
    resized_images_list: { filename: string, url: string, dimension: string }[];
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;
    

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
    this.repositorypdfForm = this._fb.group({
            my_notes_id:[0],
            app_id:[17],
            user_std:['1st'],
            teacher_id:[this.id],
            type:['application/pdf'],
            title:['',Validators.required],
            //image_path:['',Validators.required],
            path:[''],
            description:['',Validators.required],
            
            
        });
    }
    ngOnInit() {
window.scrollTo(0,0);
//       this._datacalls.getAllCategory().subscribe(
//             data => {
//               if (data.Data.length > 0) {
//                 this.policiesList = data.Data[0].data;
//                 console.log(this.policiesList);
//               }
//             }
//           );
            
 this.activatedRoute.params.subscribe(
      (param: any) => {

        this.id = param['id'];
        this.sid = param['sid'];
       // this.pid = param['repositorypdf_id'];
        //this.pid = param['repositorypdf_id'];
        console.log(this)
        if (this.id != undefined) {
        if (this.sid != undefined) {

          this._datacalls.getRepositorypdf(this.id).subscribe(
            
            data => {
              console.log('edit data:',data.Data);
                            for(var i=0;i<data.Data.length;i++){

                              console.log(this.sid +" "+data.Data[i].my_notes_id)
if(this.id==data.Data[i].teacher_id){
                              if(this.sid==data.Data[i].my_notes_id){

                               this.imagepath=data.Data[i].path;
              console.log(data.Data[i].path)
                  console.log('edit data descrip:',data.Data[i].description);
                this.repositorypdfForm .patchValue({
                  'my_notes_id':data.Data[i].my_notes_id,
                  'app_id':17,
                  'user_std':'1st',
                  'teacher_id':this.id,
                  'type':'application/pdf',
                  'title':data.Data[i].title,
                  //'image_path':data.Data[i].image_path,
                  'description':data.Data[i].description,
                  // 'path':data.Data[i].path,
                  'division':'all',
                  
                  
                });
                
                this.filesToUpload=new Array<File>();
                
                              }
              }
            }
        } );
        }
      }});
        

    }
    
    file: File;
    filesToUpload: Array<File>;
 
_url:string ="http://myappcenter.co.in/serverfileuploadapi/api/insertmynotes";
    onChange(fileInput: any) {
     this.isFileLoaded=true;
    this.filesToUpload = <Array<File>>fileInput.target.files;
    let size = fileInput.target.files[0].size;
    console.log("Size: "+ size);
    if(size>=500000) {
      this.isValidImage = false;
      console.log("isValidImage: "+ this.isValidImage);
    }else {
    this.isValidImage = true;
    console.log("isValidImage: "+ this.isValidImage);
  }

}

  onSubmit() {
    var formData: any = new FormData();
     formData.append("my_notes_id",this.repositorypdfForm.value.my_notes_id);
     formData.append("app_id",17);
    formData.append("user_std",'1st');
     formData.append("teacher_id",this.id);
     formData.append("type",'application/pdf');
     formData.append("title",this.repositorypdfForm.value.title); 
     //formData.append("image_path",this.repositorypdfForm.value.image_path);
     formData.append("path",this.repositorypdfForm.value.path);     
     formData.append("description",this.repositorypdfForm.value.description); 
     formData.append("subject_id",null);
     formData.append("division",'all');
     console.log(formData)
    this.makeFileRequest(this._url,formData, this.filesToUpload).then((result) => {
      console.log(result);
        if(result['Success']==true){
             this.alertClass = "alert alert-success text-center  alert-dismissible";
          if (this.repositorypdfForm.value.my_notes_id==0) {
            this.errorMsg = 'Repository PDF Added SuccessFully';
          } else {
            this.errorMsg = 'Repository PDF Edited SuccessFully';
          }
  window.setTimeout(() => {
            this.Router.navigate(['repositorypdf/'+this.id]);
          }, 3000);
        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = result['Message'];
        }
    
    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

}
