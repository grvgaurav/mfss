import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';


@Component({
  selector: 'app-addschemas',
  templateUrl: './addschemas.component.html',
  styleUrls: ['./addschemas.component.css'],
  providers:[datacalls]
})
export class AddschemasComponent implements OnInit {

    public schemasForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
   a:any;
    filesToPost: any;
       alertClass: string;
   errorMsg: string;
    file_keys: string[];
    id; 
    scheme_id;
    filesToUpload;
    imagepath1:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;
    
    resized_images_list: { filename: string, url: string, dimension: string }[];

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls, private changeDetectorRef: ChangeDetectorRef,private fb:FormBuilder,private Router:Router ) {

        this.schemasForm = this.fb.group({
            scheme_id:[0],
            app_id:[17],
            title: ['', [<any>Validators.required]],
             description: ['', [<any>Validators.required]],
              start_date: ['', [<any>Validators.required]],
              launched_date: ['', [<any>Validators.required]],
               
            file:[''],
            gaon_id:[''],
            guest:['', [<any>Validators.required]],

        });
     }


    ngOnInit() {

      this.scheme_id=this.activatedRoute.snapshot.params['scheme_id']
          console.log(this.scheme_id)
          if (this.scheme_id != undefined) {
  
            this._datacalls.getSchemes(this.scheme_id).subscribe(
              
              data => {
                              
              console.log( 'data',data.Data[0]);
              
                   
                  this.schemasForm.patchValue({
                    'scheme_id': data.Data[0].scheme_id,
                    'app_id': 17,
                    'title': data.Data[0].title,
                    'description': data.Data[0].description,
                    'file': data.Data[0].file,
                    'start_date': data.Data[0].start_date,
                    'launched_date': data.Data[0].launched_date,
                    'guest': data.Data[0].guest,
                    });
                  this.filesToUpload=new Array<File>();
                  this.imagepath1= this.schemasForm.value.file;
                  console.log('path',this.imagepath1); 
                  
                                }
                
              
            );
          }
        // });
          
  
    }
    onChange(fileInput: any) {
      this.filesToUpload = fileInput.target.files['0'];
    //  this.isFileLoaded = true;
   
   }



  onSubmit() {
     
    var formData = new FormData();
   
    formData.append("scheme_id", this.schemasForm.value.scheme_id);
    formData.append("app_id", this.schemasForm.value.app_id);
    formData.append("title", this.schemasForm.value.title);
    formData.append("description", this.schemasForm.value.description.replace(/'/g,"''"));
    
    if(this.imagepath1 != undefined){
    formData.append("file", this.filesToUpload);
    }

    if(this.imagepath1 == undefined ){
      formData.append("file", this.imagepath1);
    }

    formData.append("start_date", this.schemasForm.value.start_date);
    formData.append("launched_date", this.schemasForm.value.launched_date);
    formData.append("guest", this.schemasForm.value.guest);
     formData.append("location",'NA');
    formData.append("gaon_id",1);

    // console.log(encodeURI(this.schemasForm.value.description));
    //  console.log("Encoded URI Component-->"+encodeURIComponent(this.schemasForm.value.description));
    
    
 console.log('form',formData)
 
  this._datacalls.addSchemes(formData).subscribe(posts => {
       
      if(posts['Success']==true){
              this.alertClass = "alert alert-success text-center alert-dismissible";
               if (this.schemasForm.value.scheme_id==0) {
            this.errorMsg = 'Scheme Added SuccessFully';
          } else {
            this.errorMsg = 'Scheme Edited SuccessFully';
          }
               window.setTimeout(() => {
             this.Router.navigate(['schemas']);
           }, 2000);
           
           } 
           else{
             this.alertClass = "alert alert-danger text-center alert-dismissible";
             this.errorMsg = 'Form not Filled properly';
 
           }
     
       console.log('Run succesfull');
         
   });
 
  }
  }