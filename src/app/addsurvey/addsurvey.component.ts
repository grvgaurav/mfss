import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router';
import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';

@Component({
  selector: 'app-addsurvey',
  templateUrl: './addsurvey.component.html',
  styleUrls: ['./addsurvey.component.css'],
  providers:[datacalls]
})
export class addsurveycomponent implements OnInit {


    public surveyForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    surveyList:any;
    imagepath:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";

    a:any;
    filesToPost: any;
    file_keys: string[];
    id;
    alertClass: string;
    errorMsg: string;
    resized_images_list: { filename: string, url: string, dimension: string }[];
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
    this.surveyForm = this._fb.group({
            id:[0],
            app_id:[13],
            title:['',Validators.required],
            // Questions:['', Validators.required]
            
            image_path:['',Validators.required],
            // image_path:['']
        });
    }
    ngOnInit() {
window.scrollTo(0,0);
//       this._datacalls.getAllCategory().subscribe(
//             data => {
//               if (data.Data.length > 0) {
//                 this.policiesList = data.Data[0].data;
//                 console.log(this.policiesList);
//               }
//             }
//           );
            
 this.activatedRoute.params.subscribe(
      (param: any) => {

        this.id = param['id'];
        console.log(this)
        if (this.id != undefined) {

          this._datacalls.getSurvey().subscribe(
            
            data => {
                            for(var i=0;i<data.Data.Survey.length;i++){

                              console.log( this.id +""+data.Data.Survey.id)

                              if(this.id ==data.Data.Survey[i].id){

              //                  this.imagepath=data.Data[i].path;
              // console.log(data.Data[i].image_path)
                 if (data.Data.Survey.length > 0) {

                this.surveyForm.patchValue({
                  'id': data.Data.Survey[i].id,
                  'app_id': data.Data.Survey[i].app_id,
                  'title': data.Data.Survey[i].title,
                  // 'Questions': data.Data[i].Questions,
                  'file': data.Data.Survey[i].image_path,
                  // 'image1': data.Data[i].image_path
                });
                this.filesToUpload=new Array<File>();
                              }
              }
            }
            
          }
      
          );
        }
      });
        

    }
    
    file: File;
    filesToUpload: Array<File>;
 
_url:string ="http://myappcenter.co.in/serverfileuploadapi/api/insertsurvey";
  onChange(fileInput: any) {
    
     console.log("this is image: "+ this.surveyForm.value.image_path);
    this.filesToUpload = <Array<File>>fileInput.target.files;
    this.isFileLoaded = true;
    let size = fileInput.target.files[0].size;
    console.log("Size: "+ size);
    if(size>=500000) {
      this.isValidImage = false;
      console.log("isValidImage: "+ this.isValidImage);
    }else {
    this.isValidImage = true;
    console.log("isValidImage: "+ this.isValidImage);
    }
  }
  onSubmit() {
     
       // this.a = [{name :"priya"}];
    var formData: any = new FormData();
    console.log("id",this.surveyForm.value.id);
    console.log(this.surveyForm.value.datetime);
    
     formData.append("id",this.surveyForm.value.id);
     formData.append("app_id",13);
     formData.append("user_std",'1st');
     formData.append("category_id",5);
     formData.append("title",this.surveyForm.value.title); 
     //formData.append("image_path",'http://35.154.141.107:3001/urja/data_icon/survey_icon/survey.png');
     formData.append("description",'test');  
     formData.append("datetime",'2017-08-05');  
     formData.append("division",'all');  
      
     formData.append("role",'test');  
//   formData.append("category_id",5);  
       
       
    //   console.log("title",this.surveyForm.value.title);    
    // //  formData.append("description",this.surveyForm.value.description); 
    //  console.log("file",this.surveyForm.value.image_path);
     
    //  formData.append("sub_category_id",36);
    console.log('allformdata:',formData);
    this.makeFileRequest(this._url,formData, this.filesToUpload).then((result) => {
      console.log(result);
        if(result['Success']==true){
             this.alertClass = "alert alert-success text-center  alert-dismissible";
          if (this.surveyForm.value.id==0) {
            this.errorMsg = 'Survey Added SuccessFully';
          } else {
            this.errorMsg = 'Survey Edited SuccessFully';
          }
  window.setTimeout(() => {
            this.Router.navigate(['survey']);
          }, 3000);
        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = result['Message'];
        }
    
    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      // for (var i = 0; i < files.length; i++) {
      //   formData.append("file", files[i], files[i].name);
      // }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

}
