import { Component, OnInit, ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';



@Component({
  selector: 'app-addsurveyquestions',
  templateUrl: './addsurveyquestions.component.html',
  styleUrls: ['./addsurveyquestions.component.css'],
  providers:[datacalls]
})
export class AddsurveyquestionsComponent implements OnInit {

   public surveyquestionsForm: FormGroup;
    public submitted: boolean;
    a:any;
    id:any;
    sid:any;
    
    //type;
    //types;
    alertClass: string;
    errorMsg: string;


    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
    this.surveyquestionsForm = this._fb.group({
            id:[0],
            //brand_id:[23],
            app_id:[13],
            survey_id: [0],
           question:['',Validators.required],
           a:['',Validators.required],
           b:['',Validators.required],
           c:['',Validators.required],
           d:['',Validators.required]
           
        });
    }
    ngOnInit() {
window.scrollTo(0,0);
//this._datacalls.getBrandModel(null).subscribe(
           // data => {
             // if (data.Data.length > 0) {
              //  this.types = data.Data;
              //  console.log("Types: ",data.Data);
             // }
           // }
         // );
            
this.activatedRoute.params.subscribe(
      (param: any) => {
        this.id = param['id'];
        this.sid = param['sid'];
        //this.return_policy_id = param['m,'];
       // this.type = param['type_id'];

        //console.log("Type: "+ this.type);
        if (this.id != undefined || this.sid != undefined) {
          this._datacalls.getSurvey().subscribe(
            
            data => {
              for(var i=0;i<data.Data.Survey.length;i++){
                for(var j=0;j<data.Data.Survey[i].Questions.length;j++){

                              console.log(this.id +" "+data.Data.Survey[i].id)

                              console.log(this.sid +" "+data.Data.Survey[i].Questions[j].id) 
                if(this.id ==data.Data.Survey[i].id){
                              if(this.sid ==data.Data.Survey[i].Questions[j].id){
              if (data.Data.Survey[i].Questions.length > 0) {                 
                this.surveyquestionsForm.patchValue({
                  id:data.Data.Survey[i].Questions[j].id,
                  app_id: 13,
                  survey_id:data.Data.Survey[i].Questions[j].survey_id,
                  question:data.Data.Survey[i].Questions[j].question,
                  a:data.Data.Survey[i].Questions[j].a,
                  b:data.Data.Survey[i].Questions[j].b,
                  c:data.Data.Survey[i].Questions[j].c,
                  d:data.Data.Survey[i].Questions[j].d
                  
                 
                });
              }
            }
            }
          }}}
            );
        }
        
      });




}
    
onSubmit() {

/*let formData={
                'return_policy_id': this.surveyquestionsForm.value.return_policy_id,
                'brand_id': this.surveyquestionsForm.value.brand_id,
                'app_id': this.surveyquestionsForm.value.app_id,
                'model_name': this.surveyquestionsForm.value.model_name
                
}*/
 var formData: any = new FormData();
 formData.append("id",this.surveyquestionsForm.value.id);
 formData.append("survey_id",this.id);
     //formData.append("brand_id",this.surveyquestionsForm.value.brand_id);
     formData.append("app_id",13);
     formData.append("question",this.surveyquestionsForm.value.question); 
     formData.append("a",this.surveyquestionsForm.value.a); 
     formData.append("b",this.surveyquestionsForm.value.b); 
     formData.append("c",this.surveyquestionsForm.value.c); 
     formData.append("d",this.surveyquestionsForm.value.d); 


 this._datacalls.addsurveyquestion(formData).subscribe(
      data => {
        if (data) {
          console.log("Inserted value");

          // this.Router.navigate(['events']);
        } else {
          console.log("not");
        }
        
         if(data['Success']==true){
          this.alertClass = "alert alert-success text-center  alert-dismissible";
          if (this.surveyquestionsForm.value.id==0) {
            this.errorMsg = 'Survey Question & Answer Added SuccessFully';
          } else {
            this.errorMsg = 'Survey Question & Answer Edited SuccessFully';
          }
          window.setTimeout(() => {
              this.Router.navigate(['surveyquestions/'+this.id]);
            //this.Router.navigate(['attributeType/'+ this.type + '/attributes']);
          }, 250);     
        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = data['Message'];
        }
      }
      
    );
    
}


  
}

 