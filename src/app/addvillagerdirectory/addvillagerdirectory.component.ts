import { Component, OnInit, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-addvillagerdirectory',
  templateUrl: './addvillagerdirectory.component.html',
  styleUrls: ['./addvillagerdirectory.component.css'],
  providers:[datacalls]
})
export class AddvillagerdirectoryComponent implements OnInit {

  public directoryForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    imagepath:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";


    a:any;
    filesToPost: any;
    file_keys: string[];
    id;
    alertClass: string;
    errorMsg: string;
    resized_images_list: { filename: string, url: string, dimension: string }[];
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
    let emailRegex = '^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$';
    let contactRegex='^([0-9\(\)\/\+ \-]*)$';
    let pincodeRegex='([0-9]*)$';
    this.directoryForm = this._fb.group({
           id:[0],
            app_id:[17],
            gender:['',Validators.required],
            city:[''],
            address:[''],
            name:['',Validators.required],
            mobile_no:['', [<any>Validators.required,Validators.maxLength(10),Validators.minLength(10),<any>Validators.pattern(contactRegex)]],
            email:['',[ <any>Validators.pattern(emailRegex)]],
           pincode:['', [Validators.maxLength(6),Validators.minLength(6),<any>Validators.pattern(pincodeRegex)]],
           company_name:[''],
           profile_pic:[''],
           age:[''],
           marital_status:[''],
           profession:[''],
          blood_group:[''],
          education:[''],
          aadhar_no:[''],
          pan_no:[''],
          registration_id:['']
        });
    }
    ngOnInit() {
      window.scrollTo(0,0);            
 this.activatedRoute.params.subscribe(
      (param: any) => {

        this.id = param['id'];
        console.log(this)
        if (this.id != undefined) {

          this._datacalls.getVillagerDirectory().subscribe(
            
            data => {
             
              //console.log(data.Data[0].image1)
                            for(var i=0;i<data.Data.length;i++){

                              console.log( this.id +""+data.Data[i].id)

                              if(this.id ==data.Data[i].id){
              if (data.Data.length > 0) {
                console.log(data.Data[i].profile_pic)
                 this.imagepath=data.Data[i].profile_pic;
                this.directoryForm.patchValue({
                 'id':data.Data[i].id,
                  'app_id':17,
                  'city':data.Data[i].city,
                  'address':data.Data[i].address,
                  'name':data.Data[i].name,
                  'mobile_no':data.Data[i].mobile_no,
                  'gender':data.Data[i].gender,
                  'email':data.Data[i].email,
                  'pan_no':data.Data[i].pan_no,
                  'pincode':data.Data[i].pincode,
                  'company_name':data.Data[i].company_name,
                  'profile_pic':data.Data[i].profile_pic,
                   'age':data.Data[i].age,
                  'marital_status':data.Data[i].marital_status,
                  'profession':data.Data[i].profession,
                  'blood_group':data.Data[i].blood_group,
                  'education':data.Data[i].education,
                  'aadhar_no':data.Data[i].aadhar_no,
                  'registration_id':data.Data[i].registration_id,
                });
                this.filesToUpload=new Array<File>();
              }
            }}}
          )
        }
      });
        

    }
    
    file: File;
    filesToUpload: Array<File>;
 
_url:string ="http://myappcenter.co.in/serverfileuploadapi/api/insertsmartgaonprofile";
  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
    this.isFileLoaded = true;
    let size = fileInput.target.files[0].size;
    console.log("Size: ", this.filesToUpload);
    if(size>=500000) {
      this.isValidImage = false;
      console.log("isValidImage: "+ this.isValidImage);
    }else {
    this.isValidImage = true;
    console.log("isValidImage: "+ this.isValidImage);
  }
 }
 onSubmit() {
    var formData: any = new FormData();
      formData.append("id",this.directoryForm.value.id);
     formData.append("app_id",17);
     formData.append("user_type","villager");
     formData.append("gender",this.directoryForm.value.gender);
     formData.append("city",this.directoryForm.value.city);
     formData.append("address",this.directoryForm.value.address);
     formData.append("name",this.directoryForm.value.name); 
     formData.append("mobile_no",this.directoryForm.value.mobile_no);
     formData.append("email",this.directoryForm.value.email);
     formData.append("education",this.directoryForm.value.education);
     formData.append("age",this.directoryForm.value.age);
     formData.append("marital_status",this.directoryForm.value.marital_status);
     formData.append("profession",this.directoryForm.value.profession); 
     formData.append("blood_group",this.directoryForm.value.blood_group);
     formData.append("aadhar_no",this.directoryForm.value.aadhar_no);
     formData.append("pan_no",this.directoryForm.value.pan_no);
    formData.append("file",this.directoryForm.value.profile_pic);
     formData.append("pincode",this.directoryForm.value.pincode);
     formData.append("password",123456);
     formData.append("company_name",this.directoryForm.value.company_name);
     formData.append("active",1);
     formData.append("gaon_id",1);
     if (this.directoryForm.value.id==0) {
           formData.append("registration_id",0);
          } else {
formData.append("registration_id",this.directoryForm.value.registration_id);
          }
     
     formData.append("extra_data",'NA');


     //formData.append("sub_category_id",47);
    this.makeFileRequest(this._url,formData, this.filesToUpload).then((result) => {
      console.log(result);
        if(result['Success']==true && result['Data']['Id'] != -1){
             this.alertClass = "alert alert-success text-center  alert-dismissible";
          if (this.directoryForm.value.id==0) {
            this.errorMsg = 'Villager Directory Added SuccessFully';
          } else {
            this.errorMsg = 'Villager Directory Edited SuccessFully';
          }
  window.setTimeout(() => {
            this.Router.navigate(['villagerdirectory']);
          }, 1000);
        }
        else if(result['Success']==true && result['Data']['Id'] == -1){
this.alertClass = "alert alert-success text-center  alert-dismissible";
this.errorMsg = 'This Mobile number already Exists';
        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = result['Message'];
        }
    
    }, (error) => {
      console.error(error);
    });
  }
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

}
