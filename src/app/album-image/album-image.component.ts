import { Component, OnInit, NgModule, ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import { Subject } from 'rxjs/Rx';
import { ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-album-image',
  templateUrl: './album-image.component.html',
  styleUrls: ['./album-image.component.css'], 
  providers:[datacalls]
})
export class AlbumImageComponent implements OnInit {
  
	posts:Post[];
  album_category_id;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  a: any;
  constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef, private activatedRoute:ActivatedRoute) {
  }

  ngOnInit() {
    window.scrollTo(0,0);
   
 this.activatedRoute.params.subscribe(
      (param: any) => {
        this.album_category_id = param['album_category_id'];
        
       

        console.log("album_category_id"+ this.album_category_id);
        if (this.album_category_id != undefined) {
     this._DatacallsService.getalbumimage(this.album_category_id).subscribe(posts => {
    this.posts=posts.Data;
    this.dtTrigger.next();
      console.log(posts.Data);
         window.setTimeout(() => {
          var s = document.createElement("script");
          s.text = "TableDatatablesManaged.init();";
          this._elementRef.nativeElement.appendChild(s);
           
        }, 100);
   // console.log(this.posts);
  });
 

//  this._DatacallsService.getAllCategory().subscribe(
//       data => {
//         if (data.Data.length > 0) {
//           this.categorylist = data.Data[0].data;
//           console.log("categories:" , this.categorylist);
//         }
//       }
//     );
 }})}

}
interface Post{
Message:string;
Status:number;
Success:string;
}
 

