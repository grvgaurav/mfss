import { Component, OnInit,ElementRef } from '@angular/core';
import{datacalls} from '../datacalls.service';
import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'app-albumlist',
  templateUrl: './albumlist.component.html',
  styleUrls: ['./albumlist.component.css'],
  providers:[datacalls]
})
export class AlbumlistComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  posts:post[];
  deleteid;
  jsonPara2={};

  constructor(private _album:datacalls,private _elementRef:ElementRef) { }

  deletephotogallery(id)
  { 
    console.log("delete_id:",id);
      
      this.deleteid=id;
  }
  ondelete(deleteid) {
    console.log('you have clicked submit: ', deleteid);
    this.jsonPara2={"app_id": 17,"album_category_id": deleteid}
    this._album.deletePhoto(this.jsonPara2).subscribe(posts => {
    location.reload();
});
} 



  ngOnInit() {
    window.scrollTo(0,0);
  
     this._album.getAlbum().subscribe(posts => {
       
    this.posts=posts.Data;
    this.dtTrigger.next();
      console.log('a:',posts.Data);
       
   // console.log(this.posts);
  });
  }

}
 
 
interface post{
Message:string;
Status:number;
Success:string;
}
  


