import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { FileSelectDirective, FileDropDirective } from 'ng2-file-upload';

import { HashLocationStrategy, Location, LocationStrategy } from '@angular/common';
//import {DataTableModule} from 'angular2-datatable'
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent} from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { PublicComponent} from './public/public.component';
import {SecureComponent} from './secure/secure.component';
import {  routing, appRoutingProviders } from './app.routing';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { AboutUsComponent } from './about-us/about-us.component';
import { AddAboutUsComponent } from './add-about-us/add-about-us.component';
import { WorkshopComponent } from './workshop/workshop.component';
import { AddWorkshopComponent } from './add-workshop/add-workshop.component';
import { BooksComponent } from './books/books.component';
import { AddBooksComponent } from './add-books/add-books.component';
import { VideosComponent } from './videos/videos.component';
import { AddVideosComponent } from './add-videos/add-videos.component';
import { ImageSliderComponent } from './image-slider/image-slider.component';
import { AddImageSliderComponent } from './add-image-slider/add-image-slider.component';
import { PersonalCoachingComponent } from './personal-coaching/personal-coaching.component';
import { AddPersonalCoachingComponent } from './add-personal-coaching/add-personal-coaching.component';
import { TruncatePipe } from 'angular2-truncate';
import { ActivityCategoryComponent } from './activity-category/activity-category.component';
import { StocklistComponent } from './stocklist/stocklist.component';
import { BodComponent } from './bod/bod.component';
import { AddbodComponent } from './addbod/addbod.component';
import { PoliciesComponent } from './policies/policies.component';
import { AddpoliciesComponent } from './addpolicies/addpolicies.component';
import { ContactusComponent } from './contactus/contactus.component';
import { AddcontactusComponent } from './addcontactus/addcontactus.component';
import { AlbumlistComponent } from './albumlist/albumlist.component';
import { AddalbumComponent } from './addalbum/addalbum.component';
import { BlacklistedcompanyComponent } from './blacklistedcompany/blacklistedcompany.component';
import { AddblacklistcompanyComponent } from './addblacklistcompany/addblacklistcompany.component';
import { importantinfo } from './importantinfo/importantinfo.component';
import { AddimportantinfoComponent } from './addimportantinfo/addimportantinfo.component';
import { AlbumImageComponent } from './album-image/album-image.component';
import { addalbumimagelistComponent } from './addalbumimagelist/addalbumimagelist.component';
import {ObituaryComponent} from './obituary/obituary.component';
import {AddObituaryComponent} from './add-obituary/add-obituary.component';
import {PastManagementComponent} from './past-management/past-management.component';
import {AddpastManagementComponent} from './addpast-management/addpast-management.component';
import {DirectoryComponent} from './directory/directory.component';
import {AddDirectoryComponent} from './add-directory/add-directory.component';
import { NewsletterComponent } from './newsletter/newsletter.component';
import { AddNewsletterComponent } from './add-newsletter/add-newsletter.component';
import { NewsletterSubcategoryComponent } from './newsletter-subcategory/newsletter-subcategory.component';
import { AddNewsletterSubcategoryComponent } from './add-newsletter-subcategory/add-newsletter-subcategory.component';
import { NewsletterFileComponent } from './newsletter-file/newsletter-file.component';
import { AddActivityCategoryComponent } from './add-activity-category/add-activity-category.component';
import { ActivitySubcategoryComponent } from './activity-subcategory/activity-subcategory.component';
import { AddActivitySubcategoryComponent } from './add-activity-subcategory/add-activity-subcategory.component';
import { ActivityAlbumComponent } from './activity-album/activity-album.component';
import { AddActivityAlbumComponent } from './add-activity-album/add-activity-album.component';
//import { AddNewsletterListComponent } from './add-newsletter-list/add-newsletter-list.component';
import { DataTablesModule } from 'angular-datatables';
import { FeedbackComponent } from './feedback/feedback.component';

import { JobpostingComponent } from './jobposting/jobposting.component';
import { AddjobpostingComponent } from './addjobposting/addjobposting.component';
import { SurveyComponent } from './survey/survey.component';
import{addsurveycomponent} from './addsurvey/addsurvey.component';
import { SurveyquestionsComponent } from './surveyquestions/surveyquestions.component';
import { AddsurveyquestionsComponent } from './addsurveyquestions/addsurveyquestions.component';
import { ForumComponent } from './forum/forum.component';
import { AddforumComponent } from './addforum/addforum.component';
import { ExpertsComponent } from './experts/experts.component';
import { AddexpertsComponent } from './addexperts/addexperts.component';
import { AgentComponent } from './agent/agent.component';
import { AddagentComponent } from './addagent/addagent.component';
import { RegisterationComponent } from './registeration/registeration.component';
import { BannersComponent } from './banners/banners.component';
import { AddbannersComponent } from './addbanners/addbanners.component';

import { CategoryComponent } from './category/category.component';
import { AddcategoryComponent } from './addcategory/addcategory.component';
import { AddproductComponent } from './addproduct/addproduct.component';
import { ProductComponent } from './product/product.component';
import { OrderComponent} from './order/order.component';
import{InnovationComponent} from './innovation/innovation.component';
import{ SurveysubmitComponent} from './surveysubmit/surveysubmit.component';
import{BusinessopportunityComponent} from './businessopportunity/businessopportunity.component';
import{AddbusinessopportunityComponent}from './addbusinessopportunity/addbusinessopportunity.component';
import{CalenderComponent} from './calender/calender.component';
import{AddcalenderComponent} from './addcalender/addcalender.component';
import{CatinterestComponent} from './catinterest/catinterest.component';
import { RepositoryComponent } from './repository/repository.component';
import { RepositorypdfComponent } from './repositorypdf/repositorypdf.component';
import { AddrepositoryComponent } from './addrepository/addrepository.component';
import { AddrepositorypdfComponent } from './addrepositorypdf/addrepositorypdf.component';
import { SpeaktoexpertComponent } from './speaktoexpert/speaktoexpert.component';
import { ExpertanswerComponent } from './expertanswer/expertanswer.component';
import { BrahmingauravComponent } from './brahmingaurav/brahmingaurav.component';
import { AddbrahmingauravComponent } from './addbrahmingaurav/addbrahmingaurav.component';
import { MessagesComponent } from './message/message.component';

import{AuthguardGuard} from './authguard.guard';
import { CatinterestsubComponent } from './catinterestsub/catinterestsub.component';
import { AddbusinessopportunityimagesComponent } from './addbusinessopportunityimages/addbusinessopportunityimages.component';
import { BusinessopportunityimagesComponent } from './businessopportunityimages/businessopportunityimages.component';
import { MoreproductimagesComponent } from './moreproductimages/moreproductimages.component';
import { SchemasComponent } from './schemas/schemas.component';
import { QueryComponent } from './query/query.component';
import { AddqueryComponent } from './addquery/addquery.component';
import { AddschemasComponent } from './addschemas/addschemas.component';
import { NewsComponent } from './news/news.component';
import { AddnewsComponent } from './addnews/addnews.component';
import { PanchayatdirectoryComponent } from './panchayatdirectory/panchayatdirectory.component';
import { AddpanchayatdirectoryComponent } from './addpanchayatdirectory/addpanchayatdirectory.component';
import { VillagerdirectoryComponent } from './villagerdirectory/villagerdirectory.component';
import { AddvillagerdirectoryComponent } from './addvillagerdirectory/addvillagerdirectory.component';
import { HistoryComponent } from './history/history.component';
import { AddhistoryComponent } from './addhistory/addhistory.component';
import { FeaturesComponent } from './features/features.component';
import { AddfeaturesComponent } from './addfeatures/addfeatures.component';
import { LocalattractionsComponent } from './localattractions/localattractions.component';
import { AddlocalattractionsComponent } from './addlocalattractions/addlocalattractions.component';
//import { AdvetisementComponent } from './advetisement/advetisement.component';
import { AdvertisementComponent } from './advertisement/advertisement.component';
import { AddadvertisementComponent } from './addadvertisement/addadvertisement.component';
import { SchemesappliedComponent } from './schemesapplied/schemesapplied.component';





@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    DashboardComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    PublicComponent,
    SecureComponent,
    SidebarComponent,
    FileSelectDirective,
    MenuComponent,
    AboutUsComponent,
    AddAboutUsComponent,
    WorkshopComponent,
    AddWorkshopComponent,
    BooksComponent,
    AddBooksComponent,
    VideosComponent,
    AddVideosComponent,
    ImageSliderComponent,
    AddImageSliderComponent,
    PersonalCoachingComponent,
    AddPersonalCoachingComponent,
    TruncatePipe,
    ActivityCategoryComponent,
    // AlbumComponent,
    AlbumImageComponent,
    StocklistComponent,
    BodComponent,
    AddbodComponent,
    PoliciesComponent,
    AddpoliciesComponent,
    ContactusComponent,
    AddcontactusComponent,
    AlbumlistComponent,
    AddalbumComponent,
    BlacklistedcompanyComponent,
    AddblacklistcompanyComponent,
    importantinfo,
    AddimportantinfoComponent,
    AlbumImageComponent,
    addalbumimagelistComponent,
    ObituaryComponent,
    AddObituaryComponent,
    PastManagementComponent,
    AddpastManagementComponent,
    DirectoryComponent,
    AddDirectoryComponent,
    NewsletterComponent,
    AddNewsletterComponent,
    NewsletterSubcategoryComponent,
    AddNewsletterSubcategoryComponent,
    NewsletterFileComponent,
    ActivityCategoryComponent,
    AddActivityCategoryComponent,
    ActivitySubcategoryComponent,
    AddActivitySubcategoryComponent,
    ActivityAlbumComponent,
    AddActivityAlbumComponent,
    //AddNewsletterListCompone`nt,
    FeedbackComponent,   
    JobpostingComponent,   
    AddjobpostingComponent,   
    SurveyComponent,   
    addsurveycomponent,   
    SurveyquestionsComponent,   
    AddsurveyquestionsComponent,   
    ForumComponent,   
    AddforumComponent,   
    ExpertsComponent,   
    AddexpertsComponent,   
    AgentComponent,   
    AddagentComponent,   
    RegisterationComponent,   
    BannersComponent,   
    AddbannersComponent,
    CategoryComponent,
    AddcategoryComponent,
    AddproductComponent,
    ProductComponent,
    OrderComponent,
    InnovationComponent,
    SurveyquestionsComponent,
    AddsurveyquestionsComponent,
    SurveysubmitComponent,
    BusinessopportunityComponent,
    AddbusinessopportunityComponent,
    CalenderComponent,
    AddcalenderComponent,
    CatinterestComponent,
    RepositoryComponent,
    RepositorypdfComponent,
    AddrepositorypdfComponent,   
        AddrepositoryComponent, SpeaktoexpertComponent, ExpertanswerComponent,
        BrahmingauravComponent,
   
    AddbrahmingauravComponent,
   
    CatinterestsubComponent,
   
    AddbusinessopportunityimagesComponent,
   
    BusinessopportunityimagesComponent,
   
    MoreproductimagesComponent,
   
    SchemasComponent,
   
    QueryComponent,
   
    AddqueryComponent,
   
    AddschemasComponent,
   
    NewsComponent,
   
    AddnewsComponent,
   
    PanchayatdirectoryComponent,
   
    AddpanchayatdirectoryComponent,
   
    VillagerdirectoryComponent,
   
    AddvillagerdirectoryComponent,
   
    HistoryComponent,
   
    AddhistoryComponent,
   
    FeaturesComponent,
   
    AddfeaturesComponent,
   
    LocalattractionsComponent,
   
    AddlocalattractionsComponent,
   
    //AdvetisementComponent,
   
    AdvertisementComponent,
   
    AddadvertisementComponent,
   
    SchemesappliedComponent,
    
    MessagesComponent,

   
  ],
  imports: [
    BrowserModule,
    //DataTableModule,
    FormsModule,
    HttpModule,
    routing,
    ReactiveFormsModule,
    DataTablesModule
   
  ],

  providers: [appRoutingProviders, CookieService,AuthguardGuard,  Location, { provide: LocationStrategy, useClass: HashLocationStrategy }],
  // providers: [appRoutingProviders, CookieService,  Location, { provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
