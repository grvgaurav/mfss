import { Component, OnInit, ElementRef } from '@angular/core';
import{datacalls} from '../datacalls.service';
import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'app-blacklistedcompany',
  templateUrl: './blacklistedcompany.component.html',
  styleUrls: ['./blacklistedcompany.component.css'],
  providers:[datacalls]
})
export class BlacklistedcompanyComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  	posts:post[];
    a:any;

  constructor (private _blacklist:datacalls,private _elementRef:ElementRef){

  }

  ngOnInit() { 
    this._blacklist.getBlacklistcompany().subscribe(posts => {
    this.posts=posts.Data;
    this.dtTrigger.next();
     // console.log('a');
         window.setTimeout(() => {
          var s = document.createElement("script");
          s.text = "TableDatatablesManaged.init();";
          this._elementRef.nativeElement.appendChild(s);
           
        }, 100);
   // console.log(this.posts);
  });
  }

}
 
 
interface post{
Message:string;
Status:number;
Success:string;
}