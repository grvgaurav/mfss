import { Component, OnInit , ElementRef} from '@angular/core';
import {datacalls} from '../datacalls.service';
import { ActivatedRoute, Router} from '@angular/router';
import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'app-brahmingaurav',
  templateUrl: './brahmingaurav.component.html',
  styleUrls: ['./brahmingaurav.component.css'],
  providers:[datacalls]
})
export class BrahmingauravComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
 posts:Post[];
  a: any;
  constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef) {
  }

  ngOnInit() {

    window.scrollTo(0,0);
    this.dtOptions = {
      pagingType: 'full_numbers'
    };
     this._DatacallsService.getBrahmingaurav().subscribe(posts => {
       
         
    this.posts=posts.Data;
    console.log('post', posts.Data);
  
    this.dtTrigger.next();
      
       
        
   // console.log(this.posts);
  });
 

//  this._DatacallsService.getAllCategory().subscribe(
//       data => {
//         if (data.Data.length > 0) {
//           this.categorylist = data.Data[0].data;
//           console.log("categories:" , this.categorylist);
//         }
//       }
//     );
  }

}
interface Post{
Message:string;
Status:number;
Success:string;
}