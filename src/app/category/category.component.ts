import { Component, OnInit, NgModule,ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import { ActivatedRoute} from '@angular/router';
import { Subject } from 'rxjs/Rx';
import * as $ from 'jquery';


@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],
   providers:[datacalls],
})
export class CategoryComponent implements OnInit {

    dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
 tableWidget: any = {};
 posts:Post[];
  a: any;

  parentId;
  isSubcat: boolean;
  constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef,private activatedRoute:ActivatedRoute,) {
  
   // console.log('b');
   
  }
ngAfterViewInit(){
  this.initDatatable();
}

private initDatatable(): void {
    // debugger
    // let exampleId: any = $('#example');
    // this.tableWidget = exampleId.DataTable({
    //   select: true
    // });
 
  }

  ngOnInit() {
window.scrollTo(0,0);
this.activatedRoute.params.subscribe(
      (param: any) => {

        this.parentId = param['id'];

        console.log(this.activatedRoute.routeConfig.path);
        console.log(this);
        if (this.parentId != undefined) {

         this._DatacallsService.getSubcat(this.parentId).subscribe(posts => {
          this.posts=posts.Data;
          console.log(posts.Data);
          
      
  });
        }else {
     this._DatacallsService.getCategory().subscribe(posts => {
    this.posts=posts.Data;
      console.log(posts.Data);
      this.dtTrigger.next();
        //  window.setTimeout(() => {
        //   var s = document.createElement("script");
        //   s.text = "TableDatatablesManaged.init();";
        //   this._elementRef.nativeElement.appendChild(s);  
        // }, 100);
  });
        }
      });


  


   
 
    if(this.activatedRoute.routeConfig.path == 'category/subcat/:id') {
        this.isSubcat = true;
        console.log("Subcat:" + this.isSubcat);
    } else {
      this.isSubcat = false;
        console.log("Subcat:" + this.isSubcat);
    }
  

  }

  url="";
  http="";
  
  downloadexcel(){
    
      this._DatacallsService.downloadcategoryexcel().subscribe(blob => {
      console.log(blob);
  
       var link=document.createElement('a');
                  link.href=window.URL.createObjectURL(blob);
                  link.download="SmartgaonCategory.xlsx";
                  link.click();
                  
    });
  
  }


}
interface Post{
Message:string;
Status:number;
Success:string;
}