import { Component, OnInit, ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers:[datacalls]
})
export class DashboardComponent implements OnInit {

	posts:Post[];
  a: any;
  order;
  product;
  register;
  query;
  schemes;
  images;
  testimonials;
  constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef) {
  }



  ngOnInit() {
     this._DatacallsService.getDashboardCounts().subscribe(posts => { 
    this.posts=posts.Data[0];
      console.log('a');
   console.log("Dashboard: ",posts.Data[0]['ordercount']);
   this.order = posts.Data[0]['ordercount'];
   this.product = posts.Data[0]['productcount'];
   this.register = posts.Data[0]['registrationcount'];
   this.query = posts.Data[0]['querycount'];
   this.schemes = posts.Data[0]['schemescount'];
   this.images = posts.Data[0]['imagescount'];
  });

  }

}
interface Post{
Message:string;
Status:number;
Success:string;
}