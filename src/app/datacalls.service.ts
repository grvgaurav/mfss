import { Injectable,Inject } from '@angular/core';
import { Http,Headers,Response, RequestOptions , ResponseContentType } from '@angular/http';
import {Observable} from 'rxjs';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class datacalls {

  ip: string='http://myappcenter.co.in/serverapi';
  app_id = 17;
 constructor(private http:Http) { }


  // getLogin(username,password){
  //    return this.http.get(this.ip+'/api/superlogin/'+username+'/'+password)
  //   .map(res=>res.json());
  // }

  
  getLogin(json){
    return this.http.post(this.ip+'/api/memberlogin/', json)
   .map(res=>res.json());
 }

  getAllWorkshops(id){
    return this.http.get(this.ip+'/api/viewour_toppers/7/'+id)
    .map(res=>res.json());
  }

  getAllTipsAndTools(id){
    return this.http.get(this.ip+'/api/viewfacility_gallery/7/'+id)
    .map(res=>res.json());
  }

  getAboutUs(){
    return this.http.get(this.ip+'/api/viewaboutus/' + this.app_id +'/1/null')
    .map(res=>res.json());
  }

  getBod(){
    return this.http.get(this.ip+'/api/SubCatDetail/0/' + this.app_id +'/1st/12/35/1/null')
    .map(res=>res.json());
  }

  getPolicies(){
     return this.http.get(this.ip+'/api/SubCatDetail/0/' + this.app_id +'/1st/12/36/1/null')
    .map(res=>res.json());
  }

  getContactus(){
     return this.http.get(this.ip+'/api/SubCatDetail/0/' + this.app_id +'/1st/12/37/1/null')
    .map(res=>res.json());
  }

  getObituary(id){
     return this.http.get(this.ip+'/api/viewobituary/' + this.app_id +'/' + id)
    .map(res=>res.json());
  }

  getPastmanagement(){
     return this.http.get(this.ip+'/api/SubCatDetail/0/12/5th/8/0/1/null')
    .map(res=>res.json());
  }

  getDirectory(){
     return this.http.get(this.ip+'/api/getsmartgaonusers/'+this.app_id+'/null/null/null/null/null')
    .map(res=>res.json());
  }

  getimportantinfo(){
    return this.http.get(this.ip+'/api/SubCatDetail/0/12/5th/44/0/1/null')
    .map(res=>res.json());
  }

  getalbumimage(album_category_id){
     return this.http.get(this.ip+'/api/viewalbumimages/' + this.app_id +'/'+ album_category_id)
    .map(res=>res.json());
  }

  getAlbum(){
    return this.http.get(this.ip+'/api/viewalbum/' + this.app_id+'/1' )
    .map(res=>res.json());
  }

  getBlacklistcompany(){
     return this.http.get(this.ip+'/api/viewblacklistedcompanies/12')
    .map(res=>res.json());
  }

  getNewsletter(){
   return this.http.get(this.ip+'/api/SubCatDetail/0/12/5th/2/916/1/null')
    .map(res=>res.json());
  }

  getNewsletterList(){
   return this.http.get(this.ip+'/api/viewsubjectmaster/916')
    .map(res=>res.json());
  }
 
  getViewSubNewsletter(subject_id,teacher_id){
   return this.http.get(this.ip+'/api/Notes/0/12/5th/'+teacher_id+'/'+subject_id+'/1/null')
    .map(res=>res.json());
  }

  getAllProducts(){
     return this.http.get(this.ip+'/api/viewtraining/7')
    .map(res=>res.json());
  }

  getAllTestimonials(id){
     return this.http.get(this.ip+'/api/viewsuccess_story/7/'+id)
    .map(res=>res.json());
  }

  getAllPersonalCoaching(id){
    return this.http.get(this.ip+'/api/viewplacement/7/'+id)
    .map(res=>res.json());
  }

  getAllBooks(id){
    return this.http.get(this.ip+'/api/viewaward/7/'+id)
    .map(res=>res.json());
  }

  getAllVideos(id){
    return this.http.get(this.ip+'/api/SubCatDetail/0/' + this.app_id +'/1st/19/0/null/null')
    .map(res=>res.json());
  }



  addWorkshop(json){
    return this.http.post(this.ip+'/api/insertourtoppers', json)
    .map(res=>res.json());
  }

 addJobPosting(json){
    return this.http.post(this.ip+'/api/insertplacement', json)
    .map(res=>res.json());
  }




addEvent(json){
    return this.http.post(this.ip+'/api/insertappcalendar', json)
    .map(res=>res.json());
  }


 getEvents(id){
     return this.http.get(this.ip+'/api/viewapp_calendar/7/'+id)
    .map(res=>res.json());
  }


getImageSliders(id){
    return this.http.get(this.ip+'/api/homescreen/0/' + this.app_id +'/1/20')
    .map(res=>res.json());
  }


getCatinterestcategory(){
    return this.http.get(this.ip+'/api/homescreen/0/' + this.app_id +'/1/20')
    .map(res=>res.json());
  }
getAllEnquiries(){
    return this.http.get(this.ip+'/api/getEnquiries/7')
    .map(res=>res.json());
  }

getDashboardCounts(){
    return this.http.get(this.ip+'/api/dashboardsmartgaoncount/' + this.app_id +'/1')
    .map(res=>res.json());
  }

getActivity(){
   return this.http.get(this.ip+'/api/viewactivity/12')
    .map(res=>res.json());
}


getJobPosting(){
   return this.http.get(this.ip+'/api/SubCatDetail/0/' + this.app_id +'/1st/8/0/1/20')
    .map(res=>res.json());
}

getfeedback(){
   return this.http.get(this.ip+'/api/viewappfeedback/' + this.app_id +'/null/null')
    .map(res=>res.json());
}

getCatinterest(cat_id){
   return this.http.get(this.ip+'/api/viewcatinterest/' + this.app_id +'/null/null/'+cat_id+'/null')
    .map(res=>res.json());
}

getCalender(){
   return this.http.get(this.ip+'/api/viewappcalendar/' + this.app_id +'/null/1')
    .map(res=>res.json());
}



getSurvey(){
   return this.http.get(this.ip+'/api/SubCatDetail/0/' + this.app_id +'/1st/5/0/1/20')
    .map(res=>res.json());
}

getForum(){
   return this.http.get(this.ip+'/api/viewforum/' + this.app_id +'/null/null/1/1/student/null')
    .map(res=>res.json());
}

getExperts(){
   return this.http.get(this.ip+'/api/viewuser/' + this.app_id +'/1/expert/null/null')
    .map(res=>res.json());
}

getAgents(){
   return this.http.get(this.ip+'/api/viewuser/' + this.app_id +'/1/agent/null/null')
    .map(res=>res.json());
}



getBanners(id){
  return this.http.get(this.ip+'/api/viewforumbanner/17/'+id+'/null')
   .map(res=>res.json());
}

//Start market place
getCatdetails(id){

return this.http.get(this.ip+ '/api/viewcat/'+id)
.map(res=>res.json());

}

getCategory(){

return this.http.get(this.ip+ '/api/viewaryacategorydetails/' + this.app_id +'/null/null/1')
.map(res=>res.json());
}

getOrder(){
return this.http.get(this.ip+ '/api/vieworderdetails/' + this.app_id )
.map(res=>res.json());
}

getAllCategory(){

return this.http.get(this.ip+ '/api/viewallcategories/' + this.app_id +'')
.map(res=>res.json());
}

getSurveysubmit(){
   return this.http.get(this.ip+'/api/viewsurveysubmit/' + this.app_id +'/null/null')
    .map(res=>res.json());
}

getBusinessopportunity(){
   return this.http.get(this.ip+'/api/SubCatDetail/5925/' + this.app_id +'/1st/38/0/1/20')
    .map(res=>res.json());
}

getInnovation(){
   return this.http.get(this.ip+'/api/getbrahmininnovation/' + this.app_id +'/null/null/null/1')
    .map(res=>res.json());
}

addsurveyquestion(json){
    return this.http.post(this.ip+'/api/insertsurveyquestion', json)
    .map(res=>res.json());
}


getSubcat(id){

return this.http.get(this.ip+ '/api/viewaryacategorydetails/' + this.app_id +'/'+id)
.map(res=>res.json());


}

 getProduct(json){
       

      console.log(json);
     var api = this.ip+ '/api/aaryaproduct';
    return this.http.post(api, json)
    .map(res =>res.json());
    

  }

downloadproductexcel(): Observable<File>{
         
  let headers = new Headers();
headers.append('Content-Type', 'application/json');
headers.append('responseType', 'arrayBuffer');
let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });

  return this.http.get(this.ip+'/api/productexceldownload/17', options)
    .map(res => res.json());    
  
  //     var blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-16le' });
  //     var fileName = headers('content-disposition');
  //     FileSaver.saveAs(blob, fileName);
  // }).error(function (data, status, headers, config) {
  //   console.log('Unable to download the file')
  // });
 }


 getProductdetails(id){
       

      console.log(id);
     var api = this.ip+ '/api/aaryaproduct';
    return this.http.post(api, {"app_id":17,"cat_id":null,"customerid":0,"deviceid":null,"page":null,"pagesize":null,"price_id":null,"product_id":id,"search":"","status":1})
    .map(res =>res.json());
    

  }
//End market place



  getRepository(){
    return this.http.get(this.ip+'/api/SubCatDetail/0/17/1st/2/0/1/20')
     .map(res=>res.json());
 }

   addRepository(json){
     return this.http.post(this.ip+'/api/insertmynotescategory', json)
     .map(res=>res.json());
 
 }
   
   
getRepositorypdf(id){
  return this.http.get(this.ip+'/api/notes/5925/17/1st/0/'+id+'/1/20')
   .map(res=>res.json());
}

 getSpeaktoexpert(expert_id,forum_question_id){
    return this.http.get(this.ip+'/api/viewforumquestion/17/null/null/'+expert_id+'/'+forum_question_id)
     .map(res=>res.json());
 }

 addexpertanswer(json){
    return this.http.post(this.ip+'/api/insertforumanswer', json)
    .map(res=>res.json());
  }

getBrahmingaurav(){
   return this.http.get(this.ip+'/api/SubCatDetail/5925/17/1st/16/0/1/20')
    .map(res=>res.json());
}

addProduct(json){
  return this.http.post('http://13.126.122.152:4002/api/insertaryaproduct',json)
   .map(res=>res.json());
}

getQuery(query_id){
   return this.http.get(this.ip+'/api/viewsmartgaonquery/'+this.app_id+'/null/null/null/'+query_id)
    .map(res=>res.json());
}

addQuery(json){
  return this.http.post('http://myappcenter.co.in/serverfileuploadapi/api/insertsmartgaonquery',json)
   .map(res=>res.json());
}

getSchemes(scheme_id){
   return this.http.get(this.ip+'/api/viewschemes/'+this.app_id+'/null/'+scheme_id)
    .map(res=>res.json());
}

addSchemes(json){
  return this.http.post(' http://myappcenter.co.in/serverfileuploadapi/api/insertschemes',json)
   .map(res=>res.json());
}

getNews(news_id){
   return this.http.get(this.ip+'/api/viewsmartgaonnews/'+this.app_id+'/null/'+news_id)
    .map(res=>res.json());
}

addNews(json){
  return this.http.post(' http://myappcenter.co.in/serverfileuploadapi/api/insertsmartgaonnews',json)
   .map(res=>res.json());
}
getRegistration(){
   return this.http.get(this.ip+'/api/getsmartgaonusers/' + this.app_id +'/null/1/null/null/1/null/null')
    .map(res=>res.json());
}

 getPanchayatDirectory(){
     return this.http.get(this.ip+'/api/getsmartgaonusers/'+this.app_id+'/null/null/panchayat/null/1/null/null')
    .map(res=>res.json());
  }

  getVillagerDirectory(){
     return this.http.get(this.ip+'/api/getsmartgaonusers/'+this.app_id+'/null/null/villager/null/1/null/null')
    .map(res=>res.json());
  }

  
changesmartgaonpoststatus(query_id,post_status){
    return this.http.get(this.ip+'/api/changesmartgaonpoststatus/'+this.app_id+'/'+query_id+'/'+post_status)
    .map(res=>res.json());
}

getLocalAttractions(){
     return this.http.get(this.ip+'/api/viewlocalattractions/'+this.app_id+'/1/null')
    .map(res=>res.json());
  }

  getHistory(){
     return this.http.get(this.ip+'/api/viewsmartgaonhistory/'+this.app_id+'/1/null')
    .map(res=>res.json());
  }

  getFeatures(){
     return this.http.get(this.ip+'/api/viewsmartgaonfeature/'+this.app_id+'/1/1/null')
    .map(res=>res.json());
  }

    
changesmartgaonquerystatus(query_status,query_id){
    return this.http.get(this.ip+'/api/changesmartgaonquerystatus/'+this.app_id+'/'+query_id+'/'+query_status)
    .map(res=>res.json());
}

  getAdvertisement(){
     return this.http.get(this.ip+'/api/viewimageslider/'+this.app_id+'/null/1')
    .map(res=>res.json());
  }

  getSchemesApplied(item_id){
     return this.http.get(this.ip+'/api/viewapply/'+this.app_id+'/null/'+ item_id +'/null/null')
    .map(res=>res.json());
  }

   downloaddirectoryexcel(): Observable<File>{
         
  let headers = new Headers();
headers.append('Content-Type', 'application/json');
headers.append('responseType', 'arrayBuffer');
let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });

  return this.http.get(this.ip+'/api/smartgaondirectoryexceldownload/'+this.app_id+'/null/null/villager/null/1/null/null', options)
    .map(res => res.json());    
  
  //     var blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-16le' });
  //     var fileName = headers('content-disposition');
  //     FileSaver.saveAs(blob, fileName);
  // }).error(function (data, status, headers, config) {
  //   console.log('Unable to download the file')
  // });
 }
changeuserpriority(user_id,priority){
    return this.http.get(this.ip+'/api/changeuserpriority/'+this.app_id+'/'+user_id+'/'+priority)
    .map(res=>res.json());
}

deletePhoto(json){
  return this.http.post(this.ip+'/api/deletephoto',json)
   .map(res=>res.json());
}


deleteProduct(json){
  return this.http.post(this.ip+'/api/deletesitaraproduct',json)
   .map(res=>res.json());
}

downloadregistereduserexcel(): Observable<File>{
  
let headers = new Headers();
headers.append('Content-Type', 'application/json');
headers.append('responseType', 'arrayBuffer');
let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });

return this.http.get(this.ip+'/api/smartgaondirectoryexceldownload/'+this.app_id+'/null/null/null/null/1/null/null', options)
.map(res => res.json());    

//     var blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-16le' });
//     var fileName = headers('content-disposition');
//     FileSaver.saveAs(blob, fileName);
// }).error(function (data, status, headers, config) {
//   console.log('Unable to download the file')
// });
}

downloadfeedbackexcel(): Observable<File>{
  
let headers = new Headers();
headers.append('Content-Type', 'application/json');
headers.append('responseType', 'arrayBuffer');
let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });

return this.http.get(this.ip+'/api/feedbackexceldownload/17', options)
.map(res => res.json());    

//     var blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-16le' });
//     var fileName = headers('content-disposition');
//     FileSaver.saveAs(blob, fileName);
// }).error(function (data, status, headers, config) {
//   console.log('Unable to download the file')
// });
}
downloadcategoryexcel(): Observable<File>{
  
let headers = new Headers();
headers.append('Content-Type', 'application/json');
headers.append('responseType', 'arrayBuffer');
let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });

return this.http.get(this.ip+'/api/categoryexceldownload/17/null/null/null', options)
.map(res => res.json());    

//     var blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-16le' });
//     var fileName = headers('content-disposition');
//     FileSaver.saveAs(blob, fileName);
// }).error(function (data, status, headers, config) {
//   console.log('Unable to download the file')
// });
}

}





