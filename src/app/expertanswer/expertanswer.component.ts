import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router';
import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';

@Component({
  selector: 'app-expertanswer',
  templateUrl: './expertanswer.component.html',
  styleUrls: ['./expertanswer.component.css'],
  providers:[datacalls]
})
export class ExpertanswerComponent implements OnInit {
   public expertanswerForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
  a: any;
  id=0;
  sid;
  alertClass: string;
    errorMsg: string;
     resized_images_list: { filename: string, url: string, dimension: string }[];

  constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
  this.expertanswerForm = this._fb.group({
            forum_question_id:[0],
            forum_answer_id:[0],
            forum_id:[0],
            app_id:[13],
            expert_id:[this.id],
            subject:[''],
            message:[''], 
            answer:['',Validators.required], 
            file:['']   
        });
    }

  ngOnInit() {
    window.scrollTo(0,0);
    this.activatedRoute.params.subscribe(
      (param: any) => {
        this.id = param['expert_id'];
        this.sid = param['forum_question_id'];

        console.log('id'+this.id+" "+'sid'+this.sid)
        if (this.id != undefined) {
          if (this.sid != undefined) {
        
     this._datacalls.getSpeaktoexpert(this.id,this.sid).subscribe(
            data => {
              console.log('edit data:',data);
                            for(var i=0;i<data.Data.length;i++){
                              console.log(this.sid +" "+data.Data[i].forum_question_id)
                           
                this.expertanswerForm.patchValue({
                  'forum_question_id':data.Data[i].forum_question_id,
                  'app_id':13,
                  'forum_id':data.Data[i].forum_id,
                  'expert_id':this.id,
                  'subject':data.Data[i].subject,
                  'message':data.Data[i].message, 
                  'file':data.Data[i].file,
                });
                            
                              }}
                            
                              );
                                }  } }
  )}
// file: File;
//     filesToUpload: Array<File>;
 
// _url:string ="http://myappcenter.co.in/serverapi/api/insertforumanswer";
 onSubmit() {
     
       // this.a = [{name :"priya"}];
    var formData: any = new FormData();
     formData.append("forum_question_id",this.sid);
     formData.append("app_id",13);
     formData.append("forum_answer_id",this.expertanswerForm.value.forum_answer_id);      
     formData.append("forum_id",this.expertanswerForm.value.forum_id); 
     formData.append("answer",this.expertanswerForm.value.answer); 
debugger
     this._datacalls.addexpertanswer(formData).subscribe(res =>{
       debugger
  if(res['Success']==true){
             this.alertClass = "alert alert-success text-center  alert-dismissible";
          this.errorMsg = 'Reply given SuccessFully';
  window.setTimeout(() => {
            this.Router.navigate(['speaktoexpert/'+this.id]);
          }, 3000);
     
        }
        else {
          this.alertClass = "alert alert-danger text-center  alert-dismissible";
          this.errorMsg = res['Message'];
        }

     });
    //  formData.append("sub_category_id",36);
//     this.makeFileRequest(this._url,formData, this.filesToUpload).then((result) => {
//       console.log(result);
//         if(result['Success']==true){
//              this.alertClass = "alert alert-success text-center  alert-dismissible";
//           this.errorMsg = 'Policies Added Successfully!';
//   window.setTimeout(() => {
//             this.Router.navigate(['policies']);
//           }, 3000);
//         }
//         else {
//           this.alertClass = "alert alert-danger text-center  alert-dismissible";
//           this.errorMsg = result['Message'];
//         }
    
//     }, (error) => {
//       console.error(error);
//     });
// }
//  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
//     return new Promise((resolve, reject) => {
//       var xhr = new XMLHttpRequest();
//       // for (var i = 0; i < files.length; i++) {
//       //   formData.append("file", files[i], files[i].name);
//       // }
    
//       xhr.onreadystatechange = function () { 
//         if (xhr.readyState == 4) {
//           if (xhr.status == 200) {
//             resolve(JSON.parse(xhr.response));
//           } else {
//             reject(xhr.response);
//           }
//         }
//       }
//       xhr.open("POST", url, true);
//       xhr.send(formData);
//     });
//   }
}}