import { Component, OnInit,ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import { Subject } from 'rxjs/Rx';


@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css'],
  providers:[datacalls]
})
export class FeedbackComponent implements OnInit {



  dtTrigger: Subject<any> = new Subject();
  posts:Post[];
  a: any;
  constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef) {
  }

  ngOnInit() {
    window.scrollTo(0,0);
     this._DatacallsService.getfeedback().subscribe(posts => {
    this.posts=posts.Data;
    this.dtTrigger.next();
      console.log('a');
        
   // console.log(this.posts);
  });
 

//  this._DatacallsService.getAllCategory().subscribe(
//       data => {
//         if (data.Data.length > 0) {
//           this.categorylist = data.Data[0].data;
//           console.log("categories:" , this.categorylist);
//         }
//       }
//     );
  }
  url="";
  http="";
  
  downloadexcel(){
    
      this._DatacallsService.downloadfeedbackexcel().subscribe(blob => {
      console.log(blob);
  
       var link=document.createElement('a');
                  link.href=window.URL.createObjectURL(blob);
                  link.download="SmartgaonComplaints.xlsx";
                  link.click();
                  
    });
  
  }

}
interface Post{
Message:string;
Status:number;
Success:string;
}