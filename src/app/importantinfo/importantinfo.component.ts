import { Component, OnInit, ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'app-importantinfo',
  templateUrl: './importantinfo.component.html',
  styleUrls: ['./importantinfo.component.css'],
  providers:[datacalls]
})

export class importantinfo implements OnInit {

	posts:Post[];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  a: any;
  constructor(private _importantinfo:datacalls,private _elementRef:ElementRef) {
  }

  ngOnInit() {
    window.scrollTo(0,0);
     this._importantinfo.getimportantinfo().subscribe(posts => {
    this.posts=posts.Data;
    this.dtTrigger.next();
      console.log('a');
         window.setTimeout(() => {
          var s = document.createElement("script");
          s.text = "TableDatatablesManaged.init();";
          this._elementRef.nativeElement.appendChild(s);
           
        }, 100);
   // console.log(this.posts);
  });
 

//  this._DatacallsService.getAllCategory().subscribe(
//       data => {
//         if (data.Data.length > 0) {
//           this.categorylist = data.Data[0].data;
//           console.log("categories:" , this.categorylist);
//         }
//       }
//     );
  }

}
interface Post{
Message:string;
Status:number;
Success:string;
}