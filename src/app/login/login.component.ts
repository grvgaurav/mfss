import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,Validators} from '@angular/forms';
import {datacalls} from '../datacalls.service';
import { Router} from '@angular/router';
import {User} from '../_models/User'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
    providers:[datacalls],
})
export class LoginComponent implements OnInit {

  alertClass;
  errorMsg;
  user:User;

 public loginForm:FormGroup;
constructor(private fb:FormBuilder,public _datacalls:datacalls,private Router:Router){

this.loginForm = this.fb.group({
  username: ['', Validators.required],
  password: ['', Validators.required],
});
}

  ngOnInit() {
 $('body').addClass('login');

    
  }


doLogin(event){
   this.Router.navigate(['dashboard']);
  //console.log(event);
 // console.log(this.loginForm.value);
 // console.log( );
 let formData={
  'app_id':13,
  'mobile_no':this.loginForm.value.username,
'password':this.loginForm.value.password ,
};

console.log(formData);
this._datacalls.getLogin(formData).subscribe(data=>{


      console.log('data:',data)
      
     if(data['Status']==200){
       
         this.user = { "mobile" :data.Data[0].mobile_no  ,"user_type" : data.Data[0].usertype, "user_id" : data.Data[0].user_id, "name": data.Data[0].name};
       
      //  this.getLoggedInName.emit(true);
        
            sessionStorage.setItem('currentuser',JSON.stringify(this.user));
       //   this.Router.navigate([this.returnUrl]);
            console.log('current:',this.user.user_type)
          if(this.user.user_type=='admin'){

            this.Router.navigate(['dashboard'])

          }

          else if(this.user.user_type=='agent'){

            this.Router.navigate(['dashboard'])

          }
          else {
        this.alertClass = "alert alert-danger text-center alert-dismissible";
        this.errorMsg = 'Something went wrong';
       
      }
        
      }
    else if(data['Status']==204){
       this.alertClass = "alert alert-danger text-center alert-dismissible";
        this.errorMsg = 'Wrong username and password';
    }
      else{
       this.alertClass = "alert alert-danger text-center alert-dismissible";
        this.errorMsg = 'Something went wrong';
      }

});



}
}
