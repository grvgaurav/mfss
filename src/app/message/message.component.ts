import { Component, OnInit, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
//import {GooglePlaceModule} from 'ng2-google-place-autocomplete';
import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css'],
  providers: [datacalls]
})
export class MessagesComponent implements OnInit {

  a: any;
  filesToPost: any;
  file_keys: string[];
  MessageForm;
  local;
  alertClass: string;
  errorMsg: string;
  resized_images_list: { filename: string, url: string, dimension: string }[];
  isFileLoaded: boolean = false;
  isValidImage: boolean = true;
  autocomplete: any;

  constructor(private _fb: FormBuilder, private activatedRoute: ActivatedRoute, private _http: Http, private _datacalls: datacalls, private Router: Router) {
    this.MessageForm = this._fb.group({
      msg_id: [0],
      app_id: [17],
      title: ['', Validators.required],
      msg: ['', Validators.required],
      file: [''],
      //url: ['', Validators.required],
      usertype: ['', Validators.required],
      
    });
    
  }
  ngOnInit() {
    window.scrollTo(0,0); 
  }
  getcity(a){
    console.log('City:', a);
    this.autocomplete = a;
  }

  file: File;
  filesToUpload: Array<File>;


  _url: string = "http://myappcenter.co.in/serverfileuploadapi/api/insertmessage";
  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
    this.isFileLoaded = true;
    let size = fileInput.target.files[0].size;
    console.log("Size: " + size);
    if (size >= 500000) {
      this.isValidImage = false;
      console.log("isValidImage: " + this.isValidImage);
    } else {
      this.isValidImage = true;
      console.log("isValidImage: " + this.isValidImage);
    }
  }
  onSubmit() {
    this.local=JSON.parse(sessionStorage.getItem('currentuser'));
    console.log(this.local);
    console.log('title value:', this.MessageForm.value.title);
    var formData: any = new FormData();
    formData.append("msg_id", this.MessageForm.value.msg_id);
    formData.append("app_id", 17);
    formData.append("title", this.MessageForm.value.title);
    formData.append("msg", this.MessageForm.value.msg);
    //formData.append("url", '');
    formData.append("category",'');
    formData.append("geography",'');
    formData.append("isattached", '1');
    formData.append("attached_type", '');
    formData.append("sender_type",this.local.name);
    formData.append("receiver_type",this.MessageForm.value.usertype);
    formData.append("sender_id", this.local.user_id);
    formData.append("receiver_id",-1);
    formData.append("user_list",JSON.stringify({}));
    this.makeFileRequest(this._url, formData, this.filesToUpload).then((result) => {
      console.log(result);
      if (result['Success'] == true) {
        this.alertClass = "alert alert-success text-center  alert-dismissible";
        this.errorMsg = 'Messages Sent Successfully!';
        
        window.setTimeout(() => {
          location.reload();
        }, 3000);
      }
      else {
        this.alertClass = "alert alert-danger text-center  alert-dismissible";
        this.errorMsg = result['Message'];
      }

    }, (error) => {
      console.error(error);
    });
  }
  
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      if (files != undefined ){
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }
      }
      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

}




