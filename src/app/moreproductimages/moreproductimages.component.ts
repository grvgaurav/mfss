import { Component, OnInit, NgModule, ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import { Subject } from 'rxjs/Rx';
import { ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-moreproductimages',
  templateUrl: './moreproductimages.component.html',
  styleUrls: ['./moreproductimages.component.css'],
  providers:[datacalls]
})
export class MoreproductimagesComponent implements OnInit {

 posts:Post[];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  a: any;
  product_id: any;
  
  value;
jsonPara: any;
  constructor(private _DatacallsService:datacalls, private Router: Router,private _elementRef:ElementRef, private activatedRoute:ActivatedRoute) {
  }


  ngOnInit() {
    

window.scrollTo(0,0);
 this.activatedRoute.params.subscribe(
      (param: any) => {
        this.product_id = param['product_id'];

       

        console.log("product_id"+ this.product_id);
        if (this.product_id != undefined) {
this.jsonPara = {"app_id":17,"cat_id":null,"product_id":this.product_id,"status":1,"gaon_id":1};

     this._DatacallsService.getProduct(this.jsonPara).subscribe(posts => {
       console.log('hi data:',posts.Data);
   
    this.posts=posts.Data;
    console.log('hi required data:',posts.Data)
    this.dtTrigger.next();
      console.log('a');
      //this.value=this.posts.length;
        
   // console.log(this.posts);
     });
        }
        });
        

  }


}
interface Post{
Message:string;
Status:number;
Success:string;
}
