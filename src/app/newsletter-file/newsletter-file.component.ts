import { Component, OnInit,ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';
import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'app-newsletter-file',
  templateUrl: './newsletter-file.component.html',
  styleUrls: ['./newsletter-file.component.css'],
  providers: [datacalls]
})
export class NewsletterFileComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  posts:Post[];
  a: any;
  teacher_id;
  subject_id;
  constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef,private activatedRoute:ActivatedRoute) {
  }

  ngOnInit() {
     this.teacher_id=this.activatedRoute.snapshot.params['teacher_id'];
     console.log(this.teacher_id);
     this.subject_id=this.activatedRoute.snapshot.params['subject_id'];
     this._DatacallsService.getViewSubNewsletter(this.subject_id,this.teacher_id).subscribe(posts => {
     this.posts=posts.Data;
     this.dtTrigger.next();
    
      console.log('a',posts.Data);
      //console.log('a1',posts.Data[0].title);
         window.setTimeout(() => {
          var s = document.createElement("script");
          s.text = "TableDatatablesManaged.init();";
          this._elementRef.nativeElement.appendChild(s);
           
        }, 100);
   // console.log(this.posts);
  });
 

//  this._DatacallsService.getAllCategory().subscribe(
//       data => {
//         if (data.Data.length > 0) {
//           this.categorylist = data.Data[0].data;
//           console.log("categories:" , this.categorylist);
//         }
//       }
//     );
  }

}
interface Post{
Message:string;
Status:number;
Success:string;
}