import { Component, OnInit, NgModule,ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import { ActivatedRoute, Router} from '@angular/router';
import { Subject } from 'rxjs/Rx';
//import * as $ from 'jquery';

@Component({
  selector: 'app-newsletter-subcategory',
  templateUrl: './newsletter-subcategory.component.html',
  styleUrls: ['./newsletter-subcategory.component.css'],
  providers:[datacalls]
})
export class NewsletterSubcategoryComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  posts:Post[];
  a: any;
  id:any;
   constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef,private activatedRoute:ActivatedRoute) {
  }

  ngOnInit() {



    this.activatedRoute.params.subscribe(
      (param: any) => {
        this.id = param['id'];
        this.dtTrigger.next();
       // this.type = param['type_id'];

        //console.log("Type: "+ this.type);
        if (this.id != undefined) {
       
     this._DatacallsService.getNewsletterList().subscribe(posts => {
         
         
    this.posts=posts.Data;
     console.log(posts.Data);
      window.setTimeout(() => {
          var s = document.createElement("script");
          s.text = "TableDatatablesManaged.init();";
          this._elementRef.nativeElement.appendChild(s);
           
        }, 100);
    
       
        
   // console.log(this.posts);
  });
        }
        
      });

 

//  this._DatacallsService.getAllCategory().subscribe(
//       data => {
//         if (data.Data.length > 0) {
//           this.categorylist = data.Data[0].data;
//           console.log("categories:" , this.categorylist);
//         }
//       }
//     );
  }

}
interface Post{
Message:string;
Status:number;
Success:string;
}