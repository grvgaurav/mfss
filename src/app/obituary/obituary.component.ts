import { Component, OnInit ,ElementRef  } from '@angular/core';
import {datacalls} from '../datacalls.service';
import { Subject } from 'rxjs/Rx';
@Component({
  selector: 'app-obituary',
  templateUrl: './obituary.component.html',
  styleUrls: ['./obituary.component.css'],
  providers:[datacalls],
})
export class ObituaryComponent implements OnInit {
  
dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
 posts:Post[];
  
  constructor(private _Obituary_datacallserv:datacalls,private _elementRef:ElementRef) {
  }

  ngOnInit() {
    window.scrollTo(0,0);
     this._Obituary_datacallserv.getObituary(null).subscribe(posts => {
    this.posts=posts.Data;
    this.dtTrigger.next();
      console.log('a');
         
   // console.log(this.posts);
  });
 

//  this._DatacallsService.getAllCategory().subscribe(
//       data => {
//         if (data.Data.length > 0) {
//           this.categorylist = data.Data[0].data;
//           console.log("categories:" , this.categorylist);
//         }
//       }
//     );
  }

}
interface Post{
Message:string;
Status:number;
Success:string;
}
