import { Component, OnInit, NgModule,ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import * as $ from 'jquery';

import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css'],
   providers:[datacalls],
})
export class OrderComponent implements OnInit {

  tableWidget: any = {};
 posts:Post[];
  a: any;

   dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef) {
  
   // console.log('b');
  
    
   
  }
// ngAfterViewInit(){
//   this.initDatatable();
// }

// private initDatatable(): void {
    
//   }

  ngOnInit() {
    window.scrollTo(0,0);    
   // console.log('a');

    
     this._DatacallsService.getOrder().subscribe(posts => {
   //console.log(posts.Data[0].data);
    this.posts=posts.Order;
    this.dtTrigger.next();

       console.log('a',posts.Order);
        
   // console.log(this.posts);
  });
 
  }
}
interface Post{
Message:string;
Status:number;
Success:string;
}