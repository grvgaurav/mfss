import { Component, OnInit, ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'app-panchayatdirectory',
  templateUrl: './panchayatdirectory.component.html',
  styleUrls: ['./panchayatdirectory.component.css'],
  providers:[datacalls]
})
export class PanchayatdirectoryComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  posts:Post[];
  a: any;
  errorMsg;
  constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef) {
  }


run(priority,user_id){
    this._DatacallsService.changeuserpriority(user_id,priority).subscribe(posts => {

        location.reload();
  if(posts['Success']==true){
    
this.errorMsg='Work Progress updated Successfully';
  }
else{
  console.log('Error in Updating');
}
});
  
  
  }
  ngOnInit() {
     this.dtOptions = {
      pagingType: 'full_numbers'
    };
    window.scrollTo(0,0);
     this._DatacallsService.getPanchayatDirectory().subscribe(posts => {
    this.posts=posts.Data;
     this.dtTrigger.next();
      console.log('a');
         
   // console.log(this.posts);
  });
 
  }

}
interface Post{
Message:string;
Status:number;
Success:string;
}