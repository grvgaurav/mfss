import { Component, OnInit,ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'app-past-management',
  templateUrl: './past-management.component.html',
  styleUrls: ['./past-management.component.css'],
providers:[datacalls],
})
export class PastManagementComponent implements OnInit {
 dtOptions: DataTables.Settings = {};
 dtTrigger: Subject<any> = new Subject();
 posts:Post[];
  
  constructor(private _Pastmanagement_datacallserv:datacalls,private _elementRef:ElementRef) {
  }

  ngOnInit() {
     this._Pastmanagement_datacallserv.getPastmanagement().subscribe(posts => {
    this.posts=posts.Data;
    this.dtTrigger.next();
      console.log(this.posts=posts.Data);
         window.setTimeout(() => {
          var s = document.createElement("script");
          s.text = "TableDatatablesManaged.init();";
          this._elementRef.nativeElement.appendChild(s);
           
        }, 100);
   // console.log(this.posts);
  });
 

//  this._DatacallsService.getAllCategory().subscribe(
//       data => {
//         if (data.Data.length > 0) {
//           this.categorylist = data.Data[0].data;
//           console.log("categories:" , this.categorylist);
//         }
//       }
//     );
  }

}
interface Post{
Message:string;
Status:number;
Success:string;
}
