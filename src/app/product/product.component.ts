import { Component, OnInit, NgModule, ElementRef } from '@angular/core';
import { datacalls } from '../datacalls.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import * as $ from 'jquery';
import * as FileSaver from 'file-saver';
import { Router } from '@angular/router'

import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/Rx';

import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  providers: [datacalls],
})
export class ProductComponent implements OnInit {
  public productForm: FormGroup;
  tableWidget: any = {};
  posts: Post[];
  a: any;
  deleteid;
  jsonPara2={};
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  jsonPara = { "app_id": 17, "cat_id": null, "customerid": null, "deviceid": null, "page": null, "pagesize": null, "price_id": null, "product_id": 0, "search": "", "status": 1, "gaon_id": 1 };

  constructor(private _DatacallsService: datacalls, private _elementRef: ElementRef, private fb: FormBuilder, private Router: Router) {

    // console.log('b');
    this.productForm = this.fb.group({

      Excelfile: ['', Validators.required]

    });


  }

  ngAfterViewInit() {
    this.initDatatable();
  }

  private initDatatable(): void {
    // debugger
    // let exampleId: any = $('#example');
    // this.tableWidget = exampleId.DataTable({
    //   select: true
    // });

  }

  deleteproduct(id)
  { 
    console.log("delete_id:",id);
      
      this.deleteid=id;
  }
  ondelete(deleteid) {
    console.log('you have clicked submit: ', deleteid);
    this.jsonPara2={"app_id": 17,"product_id": deleteid}
    this._DatacallsService.deleteProduct(this.jsonPara2).subscribe(posts => {
    location.reload();
});
} 


  ngOnInit() {
    window.scrollTo(0, 0);
    
    this._DatacallsService.getProduct(this.jsonPara).subscribe(posts => {
      console.log(posts.Data);
      this.posts = posts.Data;

      this.dtTrigger.next();
      console.log('a');

      // console.log(this.posts);
    });

  }

 
  url = "";
  http = "";

  

  downloadexcel() {

    this._DatacallsService.downloadproductexcel().subscribe(blob => {
      console.log(blob);

      var link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      link.download = "SmartgaonProduct.xlsx";
      link.click();

    });

  }
  file: File;
  filesToUpload: Array<File>;

  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
  }


  UploadExcel() {
    console.log(this.filesToUpload)
    var formData: any = new FormData();
    formData.append("app_id", 1);

    this.makeFileRequest("http://35.154.141.107:4000/api/uploadaryaexcel", formData, this.filesToUpload).then((result) => {
      console.log(result);
      if (result['Success'] == true) {
        alert('excel updated successfully');
        // window.setTimeout(() => {
        //           this.Router.navigate(['productlist']);
        //         }, 200);
        // this.Router.navigate(['productlist']);

      }
      else {

      }
    }, (error) => {
      console.error(error);
    });

  }

  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }

      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

}
interface Post {
  Message: string;
  Status: number;
  Success: string;
}
