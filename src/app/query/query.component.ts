import { Component, OnInit, NgModule,ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import { ActivatedRoute, Router} from '@angular/router';
import * as $ from 'jquery';
import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'app-query',
  templateUrl: './query.component.html',
  styleUrls: ['./query.component.css'],
  providers:[datacalls]
})
export class QueryComponent implements OnInit {

   posts:Post[];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  a: any;
  errorMsg: string;

constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef,private activatedRoute:ActivatedRoute, private Router:Router) {  }

  run(query_status,query_id){
    this._DatacallsService.changesmartgaonquerystatus(query_status,query_id).subscribe(posts => {
    location.reload();
  if(posts['Success']==true){
    
this.errorMsg='Work Progress updated Successfully';
  }
else{
  console.log('Error in Updating');
}
});
       
        

   // console.log(this.posts);
 
  }
// ngAfterViewInit(){
//   this.initDatatable();
// }

// private initDatatable(): void {
//     // debugger
//     // let exampleId: any = $('#example');
//     // this.tableWidget = exampleId.DataTable({
//     //   select: true
//     // });
 
//   }
  change(query_id,post_status){
    this._DatacallsService.changesmartgaonpoststatus(query_id,post_status).subscribe(posts => {
        //  window.setTimeout(() => {
        //     this.Router.navigate(['category']);
        //     //  location.reload();
        //   });
        
        location.reload();
        

   // console.log(this.posts);
  });

  
  
  }
  ngOnInit() { 
    window.scrollTo(0,0);
     this._DatacallsService.getQuery(null).subscribe(posts => {
    this.posts=posts.Data;
   this.dtTrigger.next();

      console.log(this.posts=posts.Data);
  });

 
  }

}
interface Post{
Message:string;
Status:number;
Success:string;
}