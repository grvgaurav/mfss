import { Component, OnInit,ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'app-registeration',
  templateUrl: './registeration.component.html',
  styleUrls: ['./registeration.component.css'],
  providers: [datacalls],
})
export class RegisterationComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  posts:Post[];
  a: any;
  constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef) {
  }

  ngOnInit() {
    window.scrollTo(0,0);
     this._DatacallsService.getRegistration().subscribe(posts => {
    this.posts=posts.Data;
    this.dtTrigger.next();
      console.log('a');
   // console.log(this.posts);
  });
  }

 url="";
  http="";
  
  downloadexcel(){
    
      this._DatacallsService.downloadregistereduserexcel().subscribe(blob => {
      console.log(blob);
  
       var link=document.createElement('a');
                  link.href=window.URL.createObjectURL(blob);
                  link.download="Registeredusers.xlsx";
                  link.click();
                  
    });
  
  }
}
interface Post{
Message:string;
Status:number;
Success:string;
}
