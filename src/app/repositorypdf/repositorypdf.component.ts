import { Component, OnInit, NgModule, ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import { Subject } from 'rxjs/Rx';
import { ActivatedRoute, Router} from '@angular/router';


@Component({
  selector: 'app-repositorypdf',
  templateUrl: './repositorypdf.component.html',
  styleUrls: ['./repositorypdf.component.css'],
  providers:[datacalls]
})
export class RepositorypdfComponent implements OnInit {

  posts:Post[];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  a: any;
  id: any;
  sid:any;
  constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef, private activatedRoute:ActivatedRoute) {
  }

  ngOnInit() {

window.scrollTo(0,0);
 this.activatedRoute.params.subscribe(
      (param: any) => {
        this.id = param['id'];
        
       

        console.log("id"+ this.id);
        if (this.id != undefined) {


     this._DatacallsService.getRepositorypdf(this.id).subscribe(posts => {
   
    this.posts=posts.Data;
    console.log(this.posts)
    this.dtTrigger.next();
      console.log('a');
        //  window.setTimeout(() => {
        //   var s = document.createElement("script");
        //   s.text = "TableDatatablesManaged.init();";
        //   this._elementRef.nativeElement.appendChild(s);
           
        // }, 100);
   // console.log(this.posts);
  });
        }
        });
//  this._DatacallsService.getAllCategory().subscribe(
//       data => {
//         if (data.Data.length > 0) {
//           this.categorylist = data.Data[0].data;
//           console.log("categories:" , this.categorylist);
//         }
//       }
//     );
  }

}
interface Post{
Message:string;
Status:number;
Success:string;
}
