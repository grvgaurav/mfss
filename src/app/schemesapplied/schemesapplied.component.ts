import { Component, OnInit, ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import { Subject } from 'rxjs/Rx';
import{ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-schemesapplied',
  templateUrl: './schemesapplied.component.html',
  styleUrls: ['./schemesapplied.component.css'],
    providers:[datacalls]
})
export class SchemesappliedComponent implements OnInit {
 
  posts:Post[];
  id:any;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  a: any;
  item_id;
  constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef, private activatedRoute:ActivatedRoute) {
  }

  ngOnInit() {
    window.scrollTo(0,0);
     this.activatedRoute.params.subscribe(
      (param: any) => {
        this.item_id = param['item_id'];
       // this.type = param['type_id'];

        //console.log("Type: "+ this.type);
        if (this.item_id != undefined) {

     this._DatacallsService.getSchemesApplied(this.item_id).subscribe(posts => {
        // for (var i=0; i<posts.Data.Survey.length;i++){
         
        // console.log("survey_id"+posts.Data.Survey[i].id+" "+this.id);
        //  if(this.id==posts.Data.Survey[i].id){ 
    this.posts=posts.Data
    this.dtTrigger.next();

      console.log('a');
         window.setTimeout(() => {
          // var s = document.createElement("script");
          // s.text = "TableDatatablesManaged.init();";
          // this._elementRef.nativeElement.appendChild(s);
           
        }, 100);
   // console.log(this.posts);
         
  });
        }});

//  this._DatacallsService.getAllCategory().subscribe(
//       data => {
//         if (data.Data.length > 0) {
//           this.categorylist = data.Data[0].data;
//           console.log("categories:" , this.categorylist);
//         }
//       }
//     );
  }

}
interface Post{
Message:string;
Status:number;
Success:string;
}
