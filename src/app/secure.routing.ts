import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { AddAboutUsComponent } from './add-about-us/add-about-us.component';
import { WorkshopComponent } from './workshop/workshop.component';
import { AddWorkshopComponent } from './add-workshop/add-workshop.component';
import { BooksComponent } from './books/books.component';
import { AddBooksComponent } from './add-books/add-books.component';
import { VideosComponent } from './videos/videos.component';
import { AddVideosComponent } from './add-videos/add-videos.component';
import { ImageSliderComponent } from './image-slider/image-slider.component';
import { AddImageSliderComponent } from './add-image-slider/add-image-slider.component';
import { PersonalCoachingComponent } from './personal-coaching/personal-coaching.component';
import { AddPersonalCoachingComponent } from './add-personal-coaching/add-personal-coaching.component';
import { ActivityCategoryComponent } from './activity-category/activity-category.component';
import { StocklistComponent } from './stocklist/stocklist.component';
import { BodComponent } from './bod/bod.component';
import { AddbodComponent } from './addbod/addbod.component';
import { PoliciesComponent } from './policies/policies.component';
import { AddpoliciesComponent } from './addpolicies/addpolicies.component';
import { ContactusComponent } from './contactus/contactus.component';
import { AddcontactusComponent } from './addcontactus/addcontactus.component';
import { AlbumlistComponent } from './albumlist/albumlist.component';
import { AddalbumComponent } from './addalbum/addalbum.component';
import { BlacklistedcompanyComponent } from './blacklistedcompany/blacklistedcompany.component';
import { AddblacklistcompanyComponent } from './addblacklistcompany/addblacklistcompany.component';
import { importantinfo } from './importantinfo/importantinfo.component';
import { AddimportantinfoComponent } from './addimportantinfo/addimportantinfo.component';
import { AlbumImageComponent } from './album-image/album-image.component';
import { addalbumimagelistComponent } from './addalbumimagelist/addalbumimagelist.component';
import { ObituaryComponent } from './obituary/obituary.component';
import { AddObituaryComponent } from './add-obituary/add-obituary.component';
import { PastManagementComponent } from './past-management/past-management.component';
import { AddpastManagementComponent } from './addpast-management/addpast-management.component';
import { DirectoryComponent } from './directory/directory.component';
import { AddDirectoryComponent } from './add-directory/add-directory.component';
import { NewsletterComponent } from './newsletter/newsletter.component';
import { AddNewsletterComponent } from './add-newsletter/add-newsletter.component';
import { NewsletterSubcategoryComponent } from './newsletter-subcategory/newsletter-subcategory.component';
import { AddNewsletterSubcategoryComponent } from './add-newsletter-subcategory/add-newsletter-subcategory.component';
//import { AddNewsletterListComponent } from './add-newsletter-list/add-newsletter-list.component';
import { NewsletterFileComponent } from './newsletter-file/newsletter-file.component';
import { AddActivityCategoryComponent } from './add-activity-category/add-activity-category.component';
import { ActivitySubcategoryComponent } from './activity-subcategory/activity-subcategory.component';
import { AddActivitySubcategoryComponent } from './add-activity-subcategory/add-activity-subcategory.component';
import { ActivityAlbumComponent } from './activity-album/activity-album.component';
import { AddActivityAlbumComponent } from './add-activity-album/add-activity-album.component';
import {JobpostingComponent} from './jobposting/jobposting.component';
import{AddjobpostingComponent} from './addjobposting/addjobposting.component';
import{FeedbackComponent} from './feedback/feedback.component';
import { SurveyComponent } from './survey/survey.component';
import{ addsurveycomponent} from './addsurvey/addsurvey.component';
import{ SurveyquestionsComponent } from './surveyquestions/surveyquestions.component';
import{ AddsurveyquestionsComponent} from './addsurveyquestions/addsurveyquestions.component';
import { ForumComponent } from './forum/forum.component';
import { AddforumComponent } from './addforum/addforum.component';
import { ExpertsComponent } from './experts/experts.component';
import { AddexpertsComponent } from './addexperts/addexperts.component';
import { AgentComponent } from './agent/agent.component';
import { AddagentComponent } from './addagent/addagent.component';
import { RegisterationComponent } from './registeration/registeration.component';
import { BannersComponent } from './banners/banners.component';
import { AddbannersComponent } from './addbanners/addbanners.component';
import { CategoryComponent } from './category/category.component';
import { AddcategoryComponent } from './addcategory/addcategory.component';
import { AddproductComponent } from './addproduct/addproduct.component';
import { ProductComponent } from './product/product.component';
import{MoreproductimagesComponent} from './moreproductimages/moreproductimages.component';
import {OrderComponent} from './order/order.component';
import{InnovationComponent} from './innovation/innovation.component';
import{ SurveysubmitComponent} from './surveysubmit/surveysubmit.component';
import{BusinessopportunityComponent} from './businessopportunity/businessopportunity.component';
import{AddbusinessopportunityComponent}from './addbusinessopportunity/addbusinessopportunity.component';

import{BusinessopportunityimagesComponent} from './businessopportunityimages/businessopportunityimages.component';
import{AddbusinessopportunityimagesComponent}from './addbusinessopportunityimages/addbusinessopportunityimages.component';

import{CalenderComponent} from './calender/calender.component';
import{AddcalenderComponent} from './addcalender/addcalender.component';
import{CatinterestComponent} from './catinterest/catinterest.component';
import{CatinterestsubComponent} from './catinterestsub/catinterestsub.component';
import{ RepositoryComponent} from './repository/repository.component';
import{AddrepositoryComponent} from './addrepository/addrepository.component';
import{RepositorypdfComponent} from './repositorypdf/repositorypdf.component';
import{AddrepositorypdfComponent} from './addrepositorypdf/addrepositorypdf.component';
import{SpeaktoexpertComponent} from './speaktoexpert/speaktoexpert.component';
import{ExpertanswerComponent} from './expertanswer/expertanswer.component';
import{BrahmingauravComponent} from './brahmingaurav/brahmingaurav.component';
import{AddbrahmingauravComponent} from './addbrahmingaurav/addbrahmingaurav.component';
import{QueryComponent} from './query/query.component';
import{AddqueryComponent} from './addquery/addquery.component';
import{SchemasComponent} from './schemas/schemas.component';
import{AddschemasComponent} from './addschemas/addschemas.component';

import{NewsComponent} from './news/news.component';
import{AddnewsComponent} from './addnews/addnews.component';
import{PanchayatdirectoryComponent} from './panchayatdirectory/panchayatdirectory.component';
import{AddpanchayatdirectoryComponent} from './addpanchayatdirectory/addpanchayatdirectory.component';

import{VillagerdirectoryComponent} from './villagerdirectory/villagerdirectory.component';
import{AddvillagerdirectoryComponent} from './addvillagerdirectory/addvillagerdirectory.component';

import{LocalattractionsComponent} from './localattractions/localattractions.component';
import{AddlocalattractionsComponent} from './addlocalattractions/addlocalattractions.component';

import{HistoryComponent} from './history/history.component';
import{AddhistoryComponent} from './addhistory/addhistory.component';

import{FeaturesComponent} from './features/features.component';
import{AddfeaturesComponent} from './addfeatures/addfeatures.component';
import{AdvertisementComponent} from './advertisement/advertisement.component';
import{AddadvertisementComponent} from './addadvertisement/addadvertisement.component';
import{SchemesappliedComponent} from './schemesapplied/schemesapplied.component';
import { MessagesComponent } from './message/message.component';

import{AuthguardGuard} from './authguard.guard';


export const SECURE_ROUTES: Routes = [
    { path: 'dashboard', component: DashboardComponent},
    { path: 'aboutUs', component: AboutUsComponent },
    { path: 'addAboutUs', component: AddAboutUsComponent },
    { path: 'editAboutUs/:id', component: AddAboutUsComponent },
    { path: 'workshops', component: WorkshopComponent },
    { path: 'addWorkshops', component: AddWorkshopComponent },
    { path: 'editWorkshops/:id', component: AddWorkshopComponent },
    { path: 'books', component: BooksComponent },
    { path: 'addBooks', component: AddBooksComponent },
    { path: 'editBooks/:id', component: AddBooksComponent },
    { path: 'videos', component: VideosComponent },
    { path: 'addVideos', component: AddVideosComponent },
    { path: 'editVideos/:id', component: AddVideosComponent },
    { path: 'imageSlider', component: ImageSliderComponent },
    { path: 'addImageSlider', component: AddImageSliderComponent },
    { path: 'editImageSlider/:id', component: AddImageSliderComponent },
    { path: 'personalCoaching', component: PersonalCoachingComponent },
    { path: 'addPersonalCoaching', component: AddPersonalCoachingComponent },
    { path: 'editPersonalCoaching/:id', component: AddPersonalCoachingComponent },
    { path: "activity-category", component: ActivityCategoryComponent },
    { path: "album-image", component: AlbumImageComponent },
    { path: "stocklist", component: StocklistComponent },
    { path: "bod", component: BodComponent },
    { path: "addbod", component: AddbodComponent },
    { path: "policies", component: PoliciesComponent },
    { path: "addpolicies", component: AddpoliciesComponent },
    { path: "contactus", component: ContactusComponent },
    { path: "addcontactus", component: AddcontactusComponent },
    { path: "addalbum", component: AddalbumComponent},
    { path: "albumimage", component: AlbumImageComponent },
    { path: "blacklistedcompany", component: BlacklistedcompanyComponent },
    { path: "addblacklistcompany", component: AddblacklistcompanyComponent },
    { path: "albumimage", component: AlbumImageComponent },
    { path: "addalbumimagelist/:album_category_id", component: addalbumimagelistComponent },
    { path: "importantinfo", component: importantinfo },
    { path: "addimportantinfo", component: AddimportantinfoComponent },
    { path: "albumlist", component: AlbumlistComponent },
    { path: 'editbod/:institute_desk_id', component: AddbodComponent },
    { path: 'editpolicies/:institute_desk_id', component: AddpoliciesComponent },
    { path: 'editcontactus/:institute_desk_id', component: AddcontactusComponent },
    { path: "obituary", component: ObituaryComponent },
    { path: "addobituary", component: AddObituaryComponent },
    { path: "past-management", component: PastManagementComponent },
    { path: "addpast-management", component: AddpastManagementComponent },
    
    { path: "editalbumimage/:album_category_id/:album_images_id", component: addalbumimagelistComponent },
    { path: "editimportantinfo/:news_info_id", component: AddimportantinfoComponent },
    { path: "editBlacklist/:blacklist_id", component: AddblacklistcompanyComponent },
    { path: "viewalbum/:album_category_id", component: AlbumImageComponent },
    { path: "editalbumlist/:album_category_id", component: AddalbumComponent },
    { path: "editObituary/:obituary_id", component: AddObituaryComponent },
    { path: "editpastmanagement/:id/:sub_cat_id", component: AddpastManagementComponent },
    { path: "newsletter-subcategory/:id", component: NewsletterSubcategoryComponent },
    { path: "newsletter", component: NewsletterComponent },
    { path: "add-newsletter", component: AddNewsletterComponent },
    { path: "editnewsletter/:teacher_id", component: AddNewsletterComponent },
    { path: "add-newsletter-subcategory/:teacher_id", component: AddNewsletterSubcategoryComponent },
    { path: "editnewsletter-subcategory/:subject_id/:teacher_id", component: AddNewsletterSubcategoryComponent },
    //{ path: "addnewsletterlist/:subject_id/:teacher_id", component: AddNewsletterListComponent },
    { path: "newsletterfile/:subject_id/:teacher_id", component: NewsletterFileComponent },
    //{ path: "editnewsletterfile/:my_notes_id/:subject_id/:teacher_id", component: AddNewsletterListComponent },
    

   
   { path: "feedback", component: FeedbackComponent },
   

    {path: "jobposting", component: JobpostingComponent},
    { path: 'editjobposting/:id', component: AddjobpostingComponent },
    {path: "addjobposting", component: AddjobpostingComponent}, 



{ path: "directory", component: DirectoryComponent },
    { path: "addDirectory", component: AddDirectoryComponent },
    { path: 'editDirectory/:id', component: AddDirectoryComponent },
    
    


    { path: "activity-category", component: ActivityCategoryComponent },
    { path: "add-activity-category", component: AddActivityCategoryComponent },
    { path: "editactivity-category/:id", component: AddActivityCategoryComponent },

    { path: "activity-subcategory/:id", component: ActivitySubcategoryComponent },
    { path: "add-activity-subcategory/:id", component: AddActivitySubcategoryComponent },
    { path: "editactivity-subcategory/:id/:sid", component: AddActivitySubcategoryComponent },

    { path: "activity-album/:id/:sid", component: ActivityAlbumComponent },
    { path: "add-activity-album/:id/:sid", component: AddActivityAlbumComponent },
    { path: "editactivity-album/:id/:sid/:aid", component: AddActivityAlbumComponent },
    { path: "activity-album", component: ActivityAlbumComponent},

    {path: "survey", component: SurveyComponent},
   {path: "addsurvey", component: addsurveycomponent},
   { path: 'editsurvey/:id', component: addsurveycomponent},

    {path: "surveyquestions/:id", component: SurveyquestionsComponent},
    {path: "addsurveyquestions/:id", component: AddsurveyquestionsComponent},
    {path: "editsurveyquestions/:id/:sid", component: AddsurveyquestionsComponent},

    {path: "forum", component: ForumComponent},
    {path: "addforum",component: AddforumComponent},
    {path: "editforum/:forum_id",component: AddforumComponent},

    {path: "experts",component: ExpertsComponent},
    {path: "addexperts",component: AddexpertsComponent },
    {path: "editexperts/:id",component: AddexpertsComponent},

    {path: "agent",component: AgentComponent},
    {path: "addagent",component: AddagentComponent},
    {path: "editagent/:id",component: AddagentComponent},

    {path: "registeration",component: RegisterationComponent},

    {path: "viewbanners/:forum_id",component: BannersComponent},
    {path: "addbanners/:forum_id",component: AddbannersComponent},
    {path: "editbanners/:forum_id/:forum_banner_id",component: AddbannersComponent},


{path: "repository", component: RepositoryComponent},
   {path: "addrepository", component: AddrepositoryComponent},
    {path: 'editrepository/:id', component: AddrepositoryComponent},


    {path: "repositorypdf/:id", component: RepositorypdfComponent},
    {path: "addrepositorypdf/:id", component:AddrepositorypdfComponent },
    {path: "editrepositorypdf/:id/:sid", component: AddrepositorypdfComponent},

    {path:'category',component:CategoryComponent},
    {path:'addproduct',component:AddproductComponent},
    {path:'addcategory',component:AddcategoryComponent},
    {path:'productlist',component:ProductComponent},
    {path:'moreproductimages/:product_id',component:MoreproductimagesComponent},
    {path:'category/addsubcategory/:parent_id', component: AddcategoryComponent },
    {path:'category/addsubcategory/:parent_id/:id', component: AddcategoryComponent },
    {path:'addcategory/:id', component: AddcategoryComponent },
    {path:'category/subcat/:id',component:CategoryComponent},
    {path:'productlist/addproduct/:id',component:AddproductComponent},
    {path:'order',component:OrderComponent},
    {path: "innovation", component: InnovationComponent},

    {path: "surveyquestions/:id", component: SurveyquestionsComponent},
    {path: "addsurveyquestions/:id", component: AddsurveyquestionsComponent},
    {path: "editsurveyquestions/:id/:sid", component: AddsurveyquestionsComponent},

    {path: "surveysubmit", component: SurveysubmitComponent},

    
    {path: "businessopportunity", component: BusinessopportunityComponent},
    {path: "addbusinessopportunity", component: AddbusinessopportunityComponent},
    {path: "editbusinessopportunity/:training_id", component: AddbusinessopportunityComponent},

    {path: "businessopportunityimages/:training_id", component: BusinessopportunityimagesComponent},
    {path: "addbusinessopportunityimages/:training_id", component:AddbusinessopportunityimagesComponent },
    {path: "editbusinessopportunityimages/:training_id/:training_images_id", component: AddbusinessopportunityimagesComponent},

     {path: "calender", component: CalenderComponent},
    {path: "addcalender", component: AddcalenderComponent},
    {path: 'editcalender/:app_calendar_id', component: AddcalenderComponent},
     { path: "catinterest", component: CatinterestComponent},
     { path: "catinterestsub/:cat_id", component: CatinterestsubComponent},
     {path:"speaktoexpert/:expert_id",component: SpeaktoexpertComponent},
{path:"expertanswer/:expert_id/:forum_question_id",component:ExpertanswerComponent},

{path:"brahmingaurav", component:BrahmingauravComponent},
{path:"addbrahmingaurav", component:AddbrahmingauravComponent},
{path:"editbrahmingaurav/:id", component:AddbrahmingauravComponent},

{path:"query", component:QueryComponent},
{path:"addquery", component:AddqueryComponent},
{path:"editquery/:query_id", component:AddqueryComponent},

{path:"schemas", component:SchemasComponent},
{path:"addschemas", component:AddschemasComponent},
{path:"editschemas/:scheme_id", component:AddschemasComponent},

{path:"news", component:NewsComponent},
{path:"addnews", component:AddnewsComponent},
{path:"editnews/:news_id", component:AddnewsComponent},


{ path: "panchayatdirectory", component: PanchayatdirectoryComponent },
    { path: "addpanchayatdirectory", component: AddpanchayatdirectoryComponent },
    { path: 'editpanchayatdirectory/:id', component: AddpanchayatdirectoryComponent },

    { path: "villagerdirectory", component: VillagerdirectoryComponent },
    { path: "addvillagerdirectory", component: AddvillagerdirectoryComponent },
    { path: 'editvillagerdirectory/:id', component: AddvillagerdirectoryComponent },

     { path: "localattractions", component: LocalattractionsComponent },
    { path: "addlocalattractions", component: AddlocalattractionsComponent },
    { path: 'editlocalattractions/:id', component: AddlocalattractionsComponent },


    { path: "history", component: HistoryComponent},
    { path: "addhistory", component: AddhistoryComponent },
    { path: 'edithistory/:id', component: AddhistoryComponent},

 { path: "features", component:FeaturesComponent},
    { path: "addfeatures", component: AddfeaturesComponent },
    { path: 'editfeatures/:id', component: AddfeaturesComponent },

{ path: "advertisement", component:AdvertisementComponent},
    { path: "addadvertisement", component: AddadvertisementComponent },
    { path: 'editadvertisement/:id', component: AddadvertisementComponent },

{ path: 'schemesapplied/:item_id', component:SchemesappliedComponent},
// { path: 'dashboard', component: DashboardComponent ,canActivate : [AuthguardGuard]}
{path:'message', component:MessagesComponent},









];