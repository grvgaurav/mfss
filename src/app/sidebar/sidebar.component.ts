import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  user :any= {};
  sidebarMenu=[];
  marketplace=[];

  constructor(private router: Router, private activatedRoute:ActivatedRoute) {
      router.events.subscribe((val) => {
        
        for(var i=0; i<this.sidebarMenu.length; i++){
          if (this.sidebarMenu[i].path == this.router.routerState.snapshot.url) {
            this.sidebarMenu[i].class ="active";
          } else {
            this.sidebarMenu[i].class ="";
          } 
        }
    });
   }

  onClick(name){
    for(var i=0; i<this.sidebarMenu.length; i++){
      if (this.sidebarMenu[i].name == name) {
        this.sidebarMenu[i].class ="active";
      } else {
        this.sidebarMenu[i].class ="";
      } 
    }
  }
  onClick1(name){
    for(var i=0; i<this.sidebarMenu.length; i++){
      if (this.sidebarMenu[i].name == name) {
        this.sidebarMenu[i].class ="active";
      } else {
        this.sidebarMenu[i].class ="";
      } 
    }
  }

  ngOnInit() {

        
 this.user= JSON.parse( sessionStorage.getItem('currentuser'));
 
 if(this.user.user_type=='admin')
   {
 this.sidebarMenu = [
  {
    id:1,
    name: "Dashboard",
    path: "/dashboard",
    icon: "fa fa-home",
    class: ""
  },
  {
    id:2,
    name: "About Us",
    path: "/aboutUs",
    icon: "fa fa-user",
    class: "",
    submenu : [
      {
        id:3,
        name: "History",
        path: "/history",
        icon: "fa fa-user",
        class: ""
      },
      {
        id:4,
        name: "Features",
        path: "/features",
        icon: "fa fa-user",
        class: ""
      },
      {
        id:5,
        name: "Local Attractions",
        path: "/localattractions",
        icon: "fa fa-user",
        class: ""
      },
    ]
  },
   {
   id:12,
   name: "Directory",
   path: "/dashboard",
   icon: "fa fa-book",
  class: "",
  icon2:"fa fa-chevron-down",
    submenu : [
      {
        name: "Panchayat Directory",
        path: "/panchayatdirectory",
        icon: "fa fa-comments",
        class: ""
      },
      {
        name: "Villager Directory",
        path: "/villagerdirectory",
        icon: "fa fa-comments",
        class: ""
      }
    ] },
  
  // {
  //   id:6,
  //   name: "Forum",
  //   path: "/forum",
  //   icon: "fa fa-forumbee",
  //   class: ""
  // },
  // {
  //   id:7,
  //   name: "Experts",
  //   path: "/experts",
  //   icon: "fa fa-street-view",
  //   class: ""
  // },
  // {
  //   id:8,
  //   name: "Agents",
  //   path: "/agent",
  //   icon: "fa fa-user-secret",
  //   class: ""
  // },
  {
    id:9,
    name: "Registered Users",
    path: "/registeration",
    icon: "fa fa-registered",
    class: ""
  },
  {
    id:10,
    name: "Photos",
    path: "/albumlist",
    icon: "fa fa-camera-retro",
    class: ""
  },

  {
    id:10,
    name: "SmartGaon Queries",
    path: "/query",
    icon: "fa fa-pencil-square-o",
    class: ""
  },
  {
    id:10,
    name: "Advertisements",
    path: "/advertisement",
    icon: "fa fa-image",
    class: ""
  },

  {
    id:10,
    name: "News",
    path: "/news",
    icon: "fa fa-newspaper-o",
    class: ""
  },
  // {
  //   id:11,
  //   name: "Obituary",
  //   path: "/obituary",
  //   icon: "fa fa-inbox",
  //   class: ""
  // },





  // {
  //   id:13,
  //   name: "Videos",
  //   path: "/videos",
  //   icon: "fa fa-caret-square-o-right",
  //   class: ""
  // },
  // {
  //   id:14,
  //   name: "Image Slider",
  //   path: "/imageSlider",
  //   icon: "fa fa-image",
  //   class: ""
  // },
  {
    id:15,
    name: "Calender",
    path: "/calender",
    icon: "fa fa-calendar",
    class: ""
  },
   {
    id:17,
    name: "Government Schemes",
    path: "/schemas",
    icon: "fa fa-university",
    class: ""
  },
  // {
  //   id:17,
  //   name: "Complaints",
  //   path: "",
  //   icon: "fa fa-commenting-o",
  //   class: ""                        
  // },
  // {
  //   id:16,
  //   name: "Job Posting",
  //   path: "/jobposting",
  //   icon: "fa fa-users",
  //   class: ""
  // },

  {
    id:17,
    name: "Complaints",
    path: "/feedback",
    icon: "fa fa-file-text",
    class: ""
  },
  {
    id:25,
    name: "Messages",
    path: "/message",
    icon: "fa fa-envelope",
    class: ""
  },

//  {
//     id:18,
//     name: "Survey",
//     path: "/survey",
//     icon: "fa fa-file-text",
//     class: "",
//     submenu : [
//       {
//         id:19,
//         name: "Survey Submit",
//         path: "/surveysubmit",
//         icon: "fa fa-file-text",
//         class: ""
//       }
//     ]
//   },

  // {
  //   id:19,
  //   name: "Brahmin Gaurav",
  //   path: "/brahmingaurav",
  //   icon: "fa fa-star",
  //   class: ""
  // },

{
    id:20,
    name: "Business Opportunity",
    path: "/businessopportunity",
    icon: "fa fa-empire",
    class: ""
  },

//   {
//     id:21,
//     name: "Community Funding",
//     path: "/innovation",
//     icon: "fa fa-google-wallet",
//     class: ""
//   },
  // {
  //   id:22,
  //   name: "Interested People",
  //   path: "/catinterest",
  //   icon: "fa fa-thumbs-o-up",
  //   class: ""
  // },
  // {
  //   id:23,
  //   name: "Repository",
  //   path: "/repository",
  //   icon: "fa fa-newspaper-o",
  //   class: ""
  // },
  {
    id:24,
    name: "MarketPlace",
    path: "/dashboard",
    icon: "fa fa-diamond",
    class: "",
    icon2:"fa fa-chevron-down",
    submenu : [
      {
        name: "Category",
        path: "/category",
        icon: "fa fa-comments",
        class: ""
      },
      {
        name: "Product",
        path: "/productlist",
        icon: "fa fa-comments",
        class: ""
      },
      {
        name: "Order",
        path: "/order",
        icon: "fa fa-comments",
        class: ""
      }
    ]
  },
]

// this.marketplace=[
//   {
//     name: "Category",
//     path: "/category",
//     icon: "fa fa-comments",
//     class: ""
//   },
//   {
//     name: "Product",
//     path: "/productlist",
//     icon: "fa fa-comments",
//     class: ""
//   },
//   {
//     name: "Order",
//     path: "/order",
//     icon: "fa fa-comments",
//     class: ""
//   }
// ]

}
else if(this.user.user_type=='agent'){
this.sidebarMenu=[
  {
    name: "Dashboard",
    path: "/dashboard",
    icon: "fa fa-home",
    class: "active"
  },

 
];
}

  }
//   dispnone(object){
//     console.log('object:',object);
//     if(object != undefined){
//   $('#show').slideToggle();
//   console.log('jquery started');
//     }
    
    
// }

}