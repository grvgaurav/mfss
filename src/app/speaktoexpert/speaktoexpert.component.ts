import { Component, OnInit,ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import { Subject } from 'rxjs/Rx';

import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';


import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-speaktoexpert',
  templateUrl: './speaktoexpert.component.html',
  styleUrls: ['./speaktoexpert.component.css'],
  providers:[datacalls]
})
export class SpeaktoexpertComponent implements OnInit {

   dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  posts:Post[];
  a: any;
  id;
  constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router) { 
  }

  ngOnInit() {
window.scrollTo(0,0);
    this.activatedRoute.params.subscribe(
      (param: any) => {

        this.id = param['expert_id'];
        if (this.id != undefined) {
     this._datacalls.getSpeaktoexpert(this.id,null).subscribe(posts => {
    this.posts=posts.Data;
    this.dtTrigger.next();
      console.log('a');
   // console.log(this.posts);
  });
 

//  this._DatacallsService.getAllCategory().subscribe(
//       data => {
//         if (data.Data.length > 0) {
//           this.categorylist = data.Data[0].data;
//           console.log("categories:" , this.categorylist);
//         }
//       }
//     );
      }}
  )}
    }
interface Post{
Message:string;
Status:number;
Success:string;
}