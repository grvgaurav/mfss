import { Component, OnInit, ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import { Subject } from 'rxjs/Rx';
import{ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-surveysubmit',
  templateUrl: './surveysubmit.component.html',
  styleUrls: ['./surveysubmit.component.css'],
  providers:[datacalls]
})
export class SurveysubmitComponent implements OnInit {

 posts:Post[];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  a: any;
  constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef,private activatedRoute:ActivatedRoute) {
  }

  ngOnInit() {
    window.scrollTo(0,0);
     this._DatacallsService.getSurveysubmit().subscribe(posts => {
       
    this.posts=posts.Data;
    this.dtTrigger.next();

      console.log('a');
         
   // console.log(this.posts);
  });
 

//  this._DatacallsService.getAllCategory().subscribe(
//       data => {
//         if (data.Data.length > 0) {
//           this.categorylist = data.Data[0].data;
//           console.log("categories:" , this.categorylist);
//         }
//       }
//     );
  }

}
interface Post{
Message:string;
Status:number;
Success:string;
}