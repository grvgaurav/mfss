import { Component, OnInit, NgModule,ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import * as $ from 'jquery';
import * as FileSaver from 'file-saver';
import {Router} from '@angular/router';

// import { FileUploader } from   'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/Rx';
import { Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-villagerdirectory',
  templateUrl: './villagerdirectory.component.html',
  styleUrls: ['./villagerdirectory.component.css'],
  providers:[datacalls]
})
export class VillagerdirectoryComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any>= new Subject();
public villagerForm: FormGroup;
   tableWidget: any = {};
  posts:Post[];
  a: any;
   constructor(private _datacalls:datacalls,private _elementRef:ElementRef, private fb:FormBuilder,private Router:Router) {
  
   // console.log('b');
      this.villagerForm = this.fb.group({

            Excelfile: ['', Validators.required]

        });
    
   
  }

  ngOnInit() {
    window.scrollTo(0,0);
     this._datacalls.getVillagerDirectory().subscribe(posts => {
    this.posts=posts.Data;
    this.dtTrigger.next();
      console.log('a');
         
   // console.log(this.posts);
  });
 
  }
  url="";
http="";

downloadexcel(){
  
    this._datacalls.downloaddirectoryexcel().subscribe(blob => {
    console.log(blob);

     var link=document.createElement('a');
                link.href=window.URL.createObjectURL(blob);
                link.download="VillagerDirectory.xlsx";
                link.click();
                
  });

}
file: File;
  filesToUpload: Array<File>;
 
onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
  }


UploadExcel(){
 console.log(this.filesToUpload)
     var formData: any = new FormData();
   formData.append("app_id",17); 
   formData.append("gaon_id",1); 


   
 this.makeFileRequest("http://myappcenter.co.in/serverfileuploadapi/api/uploadsmartgaonndirectoryexcel",formData, this.filesToUpload).then((result) => {
      console.log(result);
       if(result['Success']==true){
          alert('excel updated successfully');
  // 
        }
        else {
     
        }
    }, (error) => {
      console.error(error);
    });

}

makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }


}
interface Post{
Message:string;
Status:number;
Success:string;
}