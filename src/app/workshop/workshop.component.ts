import { Component, OnInit, ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';

@Component({
  selector: 'app-workshop',
  templateUrl: './workshop.component.html',
  styleUrls: ['./workshop.component.css'],
  providers:[datacalls]
})
export class WorkshopComponent implements OnInit {

	posts:Post[];
  a: any;
  constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef) {
  }

  ngOnInit() {
     this._DatacallsService.getAllWorkshops(null).subscribe(posts => {
    this.posts=posts.Data;
      console.log('a');
         window.setTimeout(() => {
          var s = document.createElement("script");
          s.text = "TableDatatablesManaged.init();";
          this._elementRef.nativeElement.appendChild(s);
           
        }, 100);
   // console.log(this.posts);
  });
 

//  this._DatacallsService.getAllCategory().subscribe(
//       data => {
//         if (data.Data.length > 0) {
//           this.categorylist = data.Data[0].data;
//           console.log("categories:" , this.categorylist);
//         }
//       }
//     );
  }

}
interface Post{
Message:string;
Status:number;
Success:string;
}